﻿//-----------------------------------------------------------------------
// <copyright file="WorkoutPage.xaml.cs" company="EvanDolan">
//     Copyright (c) Evan Dolan. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace KeepFit
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Windows.Foundation;
    using Windows.Foundation.Collections;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;
    using Windows.UI.Xaml.Controls.Primitives;
    using Windows.UI.Xaml.Data;
    using Windows.UI.Xaml.Input;
    using Windows.UI.Xaml.Media;
    using Windows.UI.Xaml.Navigation;

    using JsonClasses;
    using KeeptFitAPI;

    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class WorkoutPage : KeepFit.Common.LayoutAwarePage
    {
        /// <summary>
        /// An APICaller object is supplied when the App navigates to this page.
        /// </summary>
        APICaller ac;

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkoutPage"/> class.
        /// </summary>
        public WorkoutPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // An instance of an APICaller object should be passed when the App
            // navigates to this page.
            this.ac = e.Parameter as APICaller;

            // Display users fullname on top right of screen
            this.tbUsersFullname.Text = this.ac.Fullname;

            // Update the ListView of Workouts.
            this.UpdateWorkoutListView();
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
        }

        /// <summary>
        /// This method is invoked when the user selects an item from the "lvWorkouts" ListView.
        /// This method will update the data in the "gvSelectedWorkouts" GridView based on which
        /// item in the ListView was selected.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void lvWorkouts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // The ListView's default selection item is null. If the selected item is null
            // then return and don't update the "gvSelectedWorkouts" GridView which shows
            // information on the selected Workout
            if (this.lvWorkouts.SelectedItem == null)
            {
                this.tbGridViewError.Visibility = Windows.UI.Xaml.Visibility.Visible;
                this.tbGridViewError.Text = "No Exercise selected.";
                return;
            }

            // Clean up the elements for the "NewWorkout", "RecordWorkout", "WorkoutInfo", and "DeleteWorkout" areas
            this.CleanUpNewWorkout();
            this.CleanUpRecordWorkout();
            this.CleanUpWorkoutInfo();
            this.CleanUpDeleteWorkout();

            // Try to populate the GridView 
            try
            {
                // Get a list of the workouts (and the data contained in them) in the database that have the
                // same name as the workout selected in the ListView and set it to the ItemSource of the GridView.
                List<JsonWorkout.WorkoutDetails> workoutDetails = this.ac.GetWorkoutsWithoutSets(this.lvWorkouts.SelectedItem.ToString());

                // Reverse the order of the List so that the newest workouts appear first
                workoutDetails.Reverse(0, workoutDetails.Count);

                // Hide the error TextBlock
                this.tbGridViewError.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

                // Set the "tbSelectedWorkoutHeader" TextBlock to be the name of the selected workout
                this.tbSelectedWorkoutHeader.Text = string.Empty + this.lvWorkouts.SelectedItem.ToString();

                // Check if the List returned is empty (user has not recorded any of these Workouts)
                if (workoutDetails.Count == 0)
                {
                    // Hide the GridView and show the error TextBlock
                    this.gvSelectedWorkouts.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    this.tbGridViewError.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    this.tbGridViewError.Text = "You have not recorded anything for this Exercise";
                }
                else
                {
                    // At least one Workout found, populate the GridView and set it to visible
                    this.gvSelectedWorkouts.ItemsSource = workoutDetails;
                    this.gvSelectedWorkouts.Visibility = Windows.UI.Xaml.Visibility.Visible;

                    // Hide the first ToolTip, show the second
                    this.ttExerciseListList.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    this.ttExerciseGridView.Visibility = Windows.UI.Xaml.Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                // This is called to get read of the "Variable unused" warning
                ex.ToString();
                // If the list of workouts is null or empty, the user has not made any entries for this workout
                // An ArgumentOutOfRangeException, ArgumentNullException, or ArgumentException can be thrown here
                // Inform the user that there is no data about the workouts for the selected workout
                this.tbSelectedWorkoutHeader.Text = string.Empty;

                // Hide the GridView and show the error TextBlock
                this.gvSelectedWorkouts.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                this.tbGridViewError.Visibility = Windows.UI.Xaml.Visibility.Visible;
                this.tbGridViewError.Text = "You have not recorded anything for this Exercise";
            }
        }

        /// <summary>
        /// This method is invoked when the user selects an item from the "gvSelectedWorkouts"
        /// GridView. This method will update the "gridWorkoutInfo" Grid which is located
        /// to the right of this GridView. It will set the visibility of the Grid to Visible
        /// and put some information in the UI elements inside of the Grid.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void gvSelectedWorkouts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Check if the selected item has changed to null
            if (this.gvSelectedWorkouts.SelectedItem == null)
            {
                // Selected item is null, clean the page
                this.CleanUpWorkoutInfo();
                this.CleanUpRecordWorkout();
                this.CleanUpNewWorkout();
                this.CleanUpDeleteWorkout();

                // Informing the user will be handled by the "Selection Changed" event for the ListView
                return;
            }

            // Show the "WorkoutInfo" elements
            this.ShowWorkoutInfoElements();

            // Hide the ToolTip
            this.ttExerciseGridView.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

            // Get the selected item as a JsonWorkout.WorkoutDetails object
            JsonWorkout.WorkoutDetails selectedItem = this.gvSelectedWorkouts.SelectedItem as JsonWorkout.WorkoutDetails;

            // Set text of the subheader to the timestamp and number of sets for the selected item
            this.tbWorkoutInfoSubheader.Text = string.Empty + selectedItem.timestamp_formatted + "\tSets: " + selectedItem.num_sets;
            
            // Get the Workout objects List of Sets
            selectedItem.sets = this.ac.GetSetsByTimestamp(selectedItem.timestamp, selectedItem.name);

            // Set the text of the notes details TextBox to the value for "notes" for the selected Workout
            this.tbWorkoutInfoNotesDetails.Text = string.Empty + selectedItem.note;

            // Set the text of the sets details TextBox to the value for "sets" for the selected Workout
            this.tbWorkoutInfoSetsDetails.Text = string.Empty + selectedItem.SetsToLongString();
        }

        #region CreateNewWorkout Button Methods

        /// <summary>
        /// This method is invoked when the "btnCreateNewWorkout" Button is clicked. This method
        /// will show the UI elements used for creating a new Workout and hide any other elements.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void btnCreateNewWorkout_Click(object sender, RoutedEventArgs e)
        {
            // Hide the GridView elements and add the correct header to this section of the page
            this.CollapseGridViewElements();
            this.tbSelectedWorkoutHeader.Text = "Create An Exercise";

            // Clean up the "RecordWorkout", "WorkoutInfo", and "DeleteWorkout" areas
            this.CleanUpRecordWorkout();
            this.CleanUpWorkoutInfo();
            this.CleanUpDeleteWorkout();

            // Show the "CreateNewWorkout" elements/controls
            this.ShowNewWorkoutElements();
        }

        /// <summary>
        /// This method is invoked when the "btnAddNewWorkout" Button is clicked. This method will
        /// attempt to add a new workout
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void btnAddNewWorkout_Click(object sender, RoutedEventArgs e)
        {
            // Set the border thickness of the TextBox fields to the default of 0
            this.tbxNewWorkoutName.BorderThickness = new Thickness(0);
            this.tbxNewWorkoutNote.BorderThickness = new Thickness(0);

            if (this.CheckNewWorkoutTextBoxes())
            {
                JsonReply jr = ac.PostNewWorkout(this.tbxNewWorkoutName.Text, this.tbxNewWorkoutNote.Text);
                
                // Check if the Workout was added successfully
                if (jr.errorCode.Equals(APIErrorCodes.Success))
                {
                    // Set the UI elements used to create the new workout to default and hide them
                    this.ClearNewWorkoutElements();

                    // Hide the UI elements used for creating a new Workout
                    this.CollapseNewWorkoutElements();

                    // Inform the user that the Workout was created successfully
                    this.tbSelectedWorkoutHeader.Text = string.Empty;
                    this.tbGridViewError.Text = "Exercise added successfully";
                    this.tbGridViewError.Visibility = Windows.UI.Xaml.Visibility.Visible;

                    // Update the Workout ListView to show the new workout (which shouldn't have any entries)
                    this.UpdateWorkoutListView();

                    // Set the selected item in the ListView to null
                    this.lvWorkouts.SelectedItem = null;
                }
                else if (jr.errorCode.Equals(APIErrorCodes.WorkoutAlreadyExists))
                {
                    // The Workout name has already been used
                    this.tbNewWorkoutError.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    this.tbxNewWorkoutName.BorderThickness = new Thickness(2);
                    this.tbNewWorkoutError.Text = "This name is being used by another one of yours Exercises.";
                }
                else if (jr.errorCode.Equals(APIErrorCodes.ServerOffline))
                {
                    // The server is offline
                    this.tbNewWorkoutError.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    this.tbNewWorkoutError.Text = "The server is offline. Please try again in a moment.";
                }
                else
                {
                    // The Workout could not be added to the database
                    this.tbNewWorkoutError.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    this.tbNewWorkoutError.Text = "The Exercise could not be added. Please try using new details.";
                    this.tbNewWorkoutError.Text += "\n" + jr.errorCode + "\n"+ jr.status;
                }
            }
            else
            {
                // The name or note textBox contains invalid data.
                this.tbNewWorkoutError.Visibility = Windows.UI.Xaml.Visibility.Visible;
                this.tbNewWorkoutError.Text = "The information you have entered is not valid. Please supply ";
                this.tbNewWorkoutError.Text += "a non-empty value for the name and note of this new Exercise.";
            }
        }

        #endregion CreateNewWorkout Button Methods

        #region RecordWorkout Button Methods

        /// <summary>
        /// This method is invoked when the "btnRecordWorkout" Button is clicked. This method
        /// will show the UI elements used for recording a Workout and hide any other elements.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void btnRecordWorkout_Click(object sender, RoutedEventArgs e)
        {
            // Hide the GridView elements and add the correct header to this section of the page
            this.CollapseGridViewElements();
            this.tbSelectedWorkoutHeader.Text = "Record An Exercise";

            // Clean up the "CreateNewWorkout", "WorkoutInfo", and "DeleteWorkout" areas
            this.CleanUpNewWorkout();
            this.CleanUpWorkoutInfo();
            this.CleanUpDeleteWorkout();

            // Check if the user has selected a Workout from the ListView on the right
            if (this.lvWorkouts.SelectedItem==null)
            {
                // If no Workout is select, inform the user and exit this method.
                this.tbGridViewError.Text = "Please select an Exercise from the List on the left";
                this.tbGridViewError.Visibility = Windows.UI.Xaml.Visibility.Visible;
                return;
            }

            // Show the "RecordAWorkout" elements/controls
            this.ShowRecordWorkoutElements();

            // Add Workout name to header
            this.tbSelectedWorkoutHeader.Text += " for: " + this.lvWorkouts.SelectedItem.ToString();
        }

        /// <summary>
        /// This method is invoked when the "btnSumbitRecordWorkout" Button is clicked. This method
        /// will attempt to submit a record of a workout based on the data entered by the user.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void btnSubmitRecordWorkout_Click(object sender, RoutedEventArgs e)
        {
            // Hide the TextBox for errors about recording a Workout and set it's Text to the empty string
            this.tbxErrorRecordingWorkout.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxErrorRecordingWorkout.Text = string.Empty;

            // Reset the border thickness of each textBox to zero
            this.ResetBorderForRecordWorkout();

            // Check the TextBoxes for invalid data
            if (this.CheckRecordWorkoutTextBoxes())
            {
                List<Set> sets = this.CollectSets();

                // Check if the sets collected list is null 
                if (sets == null)
                {
                    this.tbxErrorRecordingWorkout.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    this.tbxErrorRecordingWorkout.Text = "Please enter values into the boxes highlight in red.\n";
                }
                else
                {
                    this.tbxErrorRecordingWorkout.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    this.tbxErrorRecordingWorkout.Text = "Collected sets.";
                }

                // Make the call to the API server
                JsonReply jr = ac.PostRecordWorkout(this.lvWorkouts.SelectedItem.ToString(), this.tbxRecordWorkoutNote.Text, sets);

                this.tbxErrorRecordingWorkout.Visibility = Windows.UI.Xaml.Visibility.Visible;
                this.tbxErrorRecordingWorkout.Text += "\nSelected Exercise name: " + this.lvWorkouts.SelectedItem.ToString() + "\n" + jr.errorCode + "\n" + jr.status;

                if (jr.errorCode == APIErrorCodes.Success)
                {
                    // Clear the TextBoxes
                    this.ClearRecordWorkoutElements();

                    // Hide the UI elements used for recording a Workout
                    this.CollapseRecordWorkoutElements();

                    // Set the selected item i the ListView to null
                    this.lvWorkouts.SelectedItem = null;

                    // Inform the user that the Workout was recorded successfully
                    this.tbSelectedWorkoutHeader.Text = string.Empty;
                    this.tbGridViewError.Text = "Exercise recorded successfully";
                    this.tbGridViewError.Visibility = Windows.UI.Xaml.Visibility.Visible;
                }
                else if (jr.errorCode == APIErrorCodes.ValidationError)
                {
                    this.tbxErrorRecordingWorkout.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    this.tbxErrorRecordingWorkout.Text = "\nJsonReply status:\n" + jr.status;
                }
                else
                {
                    // The JsonReply indicates an unsuccessful API call. Display the response
                    this.tbxErrorRecordingWorkout.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    this.tbxErrorRecordingWorkout.Text = "\nJsonReply status:\n" + jr.status;
                }
            }
            else
            {
                // At least one of the sets contains invalid information
                this.tbxErrorRecordingWorkout.Visibility = Windows.UI.Xaml.Visibility.Visible;
                this.tbxErrorRecordingWorkout.Text = "Please enter values into the boxes highlight in red.";
            }
        }

        #endregion RecordWorkout Button Methods

        #region DeleteWorkout Button Methods

        /// <summary>
        /// This method is invoked when the "btnDeleteWorkout" Button is clicked. This method
        /// will show the UI elements/Controls in the "DeleteWorkout" area, Clean Up any other
        /// areas, and set some TextBlock values inside the "DeleteWorkout" area.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void btnDeleteWorkout_Click(object sender, RoutedEventArgs e)
        {
            // Hide GriView Elements and Clean up the "CreateNewWorkout, "RecordWorkout", and "WorkoutInfo" areas
            this.CollapseGridViewElements();
            this.CleanUpNewWorkout();
            this.CleanUpWorkoutInfo();
            this.CleanUpRecordWorkout();

            // Check if the user has selected a Workout from the ListView on the right
            if (this.lvWorkouts.SelectedItem == null)
            {
                // If no Workout is select, inform the user and exit this method.
                this.tbGridViewError.Text = "Please select an Exercise from the List on the left to delete";
                this.tbGridViewError.Visibility = Windows.UI.Xaml.Visibility.Visible;
                return;
            }

            // Add the correct header to this section of the page
            this.tbSelectedWorkoutHeader.Text = "Delete The Exercise: " + this.lvWorkouts.SelectedItem.ToString();

            // Add the header inside the "DeleteWorkout" area
            this.tbDeleteWorkoutHeader.Text = "Are you sure you want to delete \"" + this.lvWorkouts.SelectedItem.ToString() + "\"?";

            // Show the "DeleteAWorkout" elements/controls
            this.ShowDeleteWorkoutElements();
        }

        /// <summary>
        /// This method is invoked when the "btnConfirmDeleteWorkout" Button is clicked. This method
        /// will attempt to delete the selected Workout, from the ListView above, from the database.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void btnConfirmDeleteWorkout_Click(object sender, RoutedEventArgs e)
        {
            // The ListView should have a selected item but check just in case
            // Check if the user has selected a Workout from the ListView on the right
            if (this.lvWorkouts.SelectedItem == null)
            {
                // If no Workout is select, inform the user and exit this method.
                this.tbGridViewError.Text = "Please select an Exercise from the List on the left to delete";
                this.tbGridViewError.Visibility = Windows.UI.Xaml.Visibility.Visible;
                return;
            }

            // Make the call to the API server
            JsonReply jr = this.ac.DeleteWorkoutSkeleton(this.lvWorkouts.SelectedItem.ToString());

            if (jr.errorCode == APIErrorCodes.Success)
            {
                // Clear the TextBoxes
                this.CleanUpDeleteWorkout();

                // Set the selected item in the ListView to null
                this.lvWorkouts.SelectedItem = null;

                // Inform the user that the Workout was recorded successfully
                this.tbSelectedWorkoutHeader.Text = string.Empty;
                this.tbGridViewError.Text = "Exercise deleted successfully";
                this.tbGridViewError.Visibility = Windows.UI.Xaml.Visibility.Visible;

                // Update the Workout ListView to show the new workout (which shouldn't have any entries)
                this.UpdateWorkoutListView();

                // Set the selected item in the ListView to null
                this.lvWorkouts.SelectedItem = null;
            }
            else if (jr.errorCode == APIErrorCodes.ValidationError)
            {
                this.tbGridViewError.Visibility = Windows.UI.Xaml.Visibility.Visible;
                this.tbGridViewError.Text = "\nJsonReply status:\n" + jr.status;
            }
            else
            {
                // The JsonReply indicates an unsuccessful API call. Display the response
                this.tbGridViewError.Visibility = Windows.UI.Xaml.Visibility.Visible;
                this.tbGridViewError.Text = "\nError while deleting Exercise:\n" + jr.status;
            }

        }

        /// <summary>
        /// This method is invoked when the "btnDeleteWorkout" Button is clicked. This method
        /// will hide the "DeleteWorkout" area and inform the user to select a Workout.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void btnCancelDeleteWorkout_Click(object sender, RoutedEventArgs e)
        {
            // User has decided to not delete the Workout. Hide the "DeleteWorkout" area
            this.CleanUpDeleteWorkout();

            // Set the ListView selection to null
            this.lvWorkouts.SelectedItem = null;

            // Show the standard message
            this.tbGridViewError.Text = "The Exercise was not deleted. Select an Exercise to view it.";
            this.tbGridViewError.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        #endregion DeleteWorkout Button Methods

        /// <summary>
        /// This method will attempt to update the "lvWorkouts" ListView with a List of the users Workouts.
        /// This method makes a call to the API to get a list of distinct Workout names.
        /// </summary>
        /// <returns> True if the ListView was successfully updated. Otherwise, false. </returns>
        private bool UpdateWorkoutListView()
        {
            // Get the list of workouts for this user
            //List<JsonWorkout.WorkoutDetails> workouts = ac.GetWorkout("");
            List<string> woNames = ac.GetDistinctWorkoutNames();

            try
            {
                // Set the List of workouts as the source for the ListView of Workouts
                this.lvWorkouts.ItemsSource = woNames;
                // Set the default select item to null
                this.lvWorkouts.SelectedItem = null;

                // Check if there List fo workout names is empty. If it is, inform the user
                if (woNames.Count == 0)
                {
                    this.tbGridViewError.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    this.tbGridViewError.Text = "You haven't created any Exercises yet.";
                }
                return true;
            }
            catch (NullReferenceException ex)
            {
                // Call ToString() to avoid 'variable not used' warning
                ex.ToString();
                // Failed to retrieve the List of workout names
                this.tbGridViewError.Visibility = Windows.UI.Xaml.Visibility.Visible;
                this.tbGridViewError.Text = "The List of Exercise names could not be retrieved. The server may be offline.";
                return false;
            }
        }

        /// <summary>
        /// This method will hide the "gvSelectedWorkouts" GridView and the TextBlock used for
        /// displaying error messages about the GridView
        /// </summary>
        private void CollapseGridViewElements()
        {
            this.gvSelectedWorkouts.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbGridViewError.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        /// <summary>
        /// Invoked when the user clicks on the "btnBack" Button.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            // Store previous page string in a temp variable
            string previousPage = Windows.Storage.ApplicationData.Current.LocalSettings.Values["previous_page"] as string;
            // Set the previous page local variable
            Windows.Storage.ApplicationData.Current.LocalSettings.Values["previous_page"] = "workout";

            this.Frame.Navigate(typeof(HomePage), ac);
        }
        
        #region DeleteWorkout Methods

        /// <summary>
        /// This method will set the visibility of each element used in deleting a Workout to Collapsed.
        /// </summary>
        private void CollapseDeleteWorkoutElements()
        {
            this.gridDeleteWorkout.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        /// <summary>
        /// This method will set the visibility of each element used in deleting a Workout to Visible.
        /// </summary>
        private void ShowDeleteWorkoutElements()
        {
            this.gridDeleteWorkout.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        /// <summary>
        /// This method will put an empty string in to each TextBox used for deleteing a workout
        /// </summary>
        private void ClearDeleteWorkoutElements()
        {
            this.tbDeleteWorkoutHeader.Text = string.Empty;
        }

        /// <summary>
        /// This method will tidy up all the UI elements involved in displaying
        /// information about a deleting Workout. This method should be called when the user
        /// wants to use another area on this page and stop looking at the "DeleteWorkout"
        /// area.
        /// </summary>
        private void CleanUpDeleteWorkout()
        {
            // Hide the elements for the "DeleteWorkout" area
            this.CollapseDeleteWorkoutElements();

            // Empty the text in the TextBoxes in the "DeleteWorkout" area
            this.ClearDeleteWorkoutElements();
        }

        #endregion DeleteWorkout Methods

        #region CreateNewWorkout Methods

        /// <summary>
        /// This method will set the visibility of each element used in creating a new Workout to Collapsed.
        /// </summary>
        private void CollapseNewWorkoutElements()
        {
            this.tbxNewWorkoutName.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxNewWorkoutNote.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbNewWorkoutError.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbNewWorkoutName.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbNewWorkoutNote.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.btnAddNewWorkout.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        /// <summary>
        /// This method will set the visibility of each element used in creating a new Workout to Visible.
        /// </summary>
        private void ShowNewWorkoutElements()
        {
            this.tbNewWorkoutName.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbNewWorkoutNote.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxNewWorkoutName.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxNewWorkoutNote.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.btnAddNewWorkout.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        /// <summary>
        /// This method checks for null, empty, or whitespace values in the TextBoxes that are
        /// used to create a new Workout. This method will increase the border thickness of the
        /// TextBox(es) that are not valid.
        /// </summary>
        /// <returns> True if all the TextBoxes for adding a new Workout contain valid text. </returns>
        private bool CheckNewWorkoutTextBoxes()
        {
            // This bool is set to false if any of the TextBoxes are not valid
            bool valid = true;

            // Check the new Workout name supplied
            if (string.IsNullOrWhiteSpace(this.tbxNewWorkoutName.Text))
            {
                this.tbxNewWorkoutName.BorderThickness = new Thickness(2);
                valid = false;
            }

            return valid;
        }

        /// <summary>
        /// This method will put an empty string in to each TextBox used for creating a new workout
        /// </summary>
        private void ClearNewWorkoutElements()
        {
            this.tbxNewWorkoutName.Text = string.Empty;
            this.tbxNewWorkoutNote.Text = string.Empty;
            this.tbNewWorkoutError.Text = string.Empty;
        }

        /// <summary>
        /// This method sets the Border Thickness of each textBox used in creating a new Workout to zero.
        /// </summary>
        private void ResetBorderForNewWorkout()
        {
            this.tbxNewWorkoutName.BorderThickness = new Thickness(0);
        }

        /// <summary>
        /// This method will tidy up all the UI elements involved in displaying
        /// information about a Workout. This method should be called when the user
        /// wants to use another area on this page and stop looking at the "NewWorkout"
        /// area.
        /// </summary>
        private void CleanUpNewWorkout()
        {
            // Hide the elements for the "NewWorkout" area
            this.CollapseNewWorkoutElements();

            // Empty the text in the TextBoxes in the "NewWorkout" area
            this.ClearNewWorkoutElements();

            // Reset the border thickness of the Set Textboxes
            this.ResetBorderForNewWorkout();
        }

        #endregion CreateNewWorkout Methods

        #region RecordWorkout Methods

        /// <summary>
        /// This method will set the visibility of each element used in recording a Workout to Collapsed.
        /// </summary>
        private void CollapseRecordWorkoutElements()
        {
            // Hide all weight and rep TextBoxes from 4 - 10
            this.tbxWeight4.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxReps4.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxNote4.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxWeight5.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxReps5.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxNote5.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxWeight6.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxReps6.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxNote6.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxWeight7.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxReps7.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxNote7.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxWeight8.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxReps8.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxNote8.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxWeight9.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxReps9.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxNote9.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxWeight10.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxReps10.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxNote10.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

            // Hide the workout note header, workout note TextBox, and submit Button
            this.tbRecordWorkoutNoteHeader.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxRecordWorkoutNote.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.btnSubmitRecordWorkout.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

            // Hide the header over the ScrollViewer
            this.tbWeightAndRepHeader.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

            // Hide the TextBox that contains error information
            this.tbxErrorRecordingWorkout.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

            // Hide the ScrollViewer that contains everything
            this.svRecordWorkout.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

            // Set the vertical offset of the ScrollViewer to zero
            this.svRecordWorkout.ScrollToVerticalOffset(0);
        }

        /// <summary>
        /// This method will set the visibility of each element used in recording a Workout to Visible.
        /// </summary>
        private void ShowRecordWorkoutElements()
        {
            // Show the header over the ScrollViewer
            this.tbWeightAndRepHeader.Visibility = Windows.UI.Xaml.Visibility.Visible;

            // Show the ScrollViewer that contains everything for recording a workout
            this.svRecordWorkout.Visibility = Windows.UI.Xaml.Visibility.Visible;

            // Show the workout note header, workout note TextBox and submit Button
            this.tbRecordWorkoutNoteHeader.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxRecordWorkoutNote.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.btnSubmitRecordWorkout.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        /// <summary>
        /// This method checks for null, empty, or whitespace values in the TextBoxes that are
        /// used to record a Workout. This method will increase the border thickness of the
        /// TextBox(es) that are not valid.
        /// </summary>
        /// <returns> True if all the TextBoxes for recording a Workout contain valid text. </returns>
        private bool CheckRecordWorkoutTextBoxes()
        {
            // This bool is set to false if any of the TextBoxes are not valid
            bool valid = true;

            // Check the first sets values supplied
            if (string.IsNullOrWhiteSpace(this.tbxWeight1.Text))
            {
                this.tbxWeight1.BorderThickness = new Thickness(2);
                valid = false;
            }

            if (string.IsNullOrWhiteSpace(this.tbxReps1.Text))
            {
                this.tbxReps1.BorderThickness = new Thickness(2);
                valid = false;
            }

            // Check the second set. If any TextBox contains valid data, then the others must too
            if (!string.IsNullOrWhiteSpace(this.tbxWeight2.Text) || !string.IsNullOrWhiteSpace(this.tbxReps2.Text) || !string.IsNullOrWhiteSpace(this.tbxNote2.Text))
            {
                // Check each TextBox
                if (string.IsNullOrWhiteSpace(this.tbxWeight2.Text))
                {
                    this.tbxWeight2.BorderThickness = new Thickness(2);
                    valid = false;
                }

                if (string.IsNullOrWhiteSpace(this.tbxReps2.Text))
                {
                    this.tbxReps2.BorderThickness = new Thickness(2);
                    valid = false;
                }
            }

            // Check the third set. If any TextBox contains valid data, then the others must too
            if (!string.IsNullOrWhiteSpace(this.tbxWeight3.Text) || !string.IsNullOrWhiteSpace(this.tbxReps3.Text) || !string.IsNullOrWhiteSpace(this.tbxNote3.Text))
            {
                // Check each TextBox
                if (string.IsNullOrWhiteSpace(this.tbxWeight3.Text))
                {
                    this.tbxWeight3.BorderThickness = new Thickness(2);
                    valid = false;
                }

                if (string.IsNullOrWhiteSpace(this.tbxReps3.Text))
                {
                    this.tbxReps3.BorderThickness = new Thickness(2);
                    valid = false;
                }
            }

            /*
             * The remaining TextBoxes for Sets 4 to 10 are hidden by default. First check their visibility. If they
             * are collapsed, then return the valid variable because the remaining Set TextBoxes should not contain data
             * because they are set to collapsed.
             */

            // Check visibility first, then check the data if the TextBoxes are visibile
            if (this.tbxWeight4.Visibility.Equals(Windows.UI.Xaml.Visibility.Visible))
            {
                if (!string.IsNullOrWhiteSpace(this.tbxWeight4.Text) || !string.IsNullOrWhiteSpace(this.tbxReps4.Text) || !string.IsNullOrWhiteSpace(this.tbxNote4.Text))
                {
                    // Check each TextBox
                    if (string.IsNullOrWhiteSpace(this.tbxWeight4.Text))
                    {
                        this.tbxWeight4.BorderThickness = new Thickness(2);
                        valid = false;
                    }

                    if (string.IsNullOrWhiteSpace(this.tbxReps4.Text))
                    {
                        this.tbxReps4.BorderThickness = new Thickness(2);
                        valid = false;
                    }
                }
            }
            else
            {
                return valid;
            }

            // Check visibility first, then check the data if the TextBoxes are visibile
            if (this.tbxWeight5.Visibility.Equals(Windows.UI.Xaml.Visibility.Visible))
            {
                if (!string.IsNullOrWhiteSpace(this.tbxWeight5.Text) || !string.IsNullOrWhiteSpace(this.tbxReps5.Text) || !string.IsNullOrWhiteSpace(this.tbxNote5.Text))
                {
                    // Check each TextBox
                    if (string.IsNullOrWhiteSpace(this.tbxWeight5.Text))
                    {
                        this.tbxWeight5.BorderThickness = new Thickness(2);
                        valid = false;
                    }

                    if (string.IsNullOrWhiteSpace(this.tbxReps5.Text))
                    {
                        this.tbxReps5.BorderThickness = new Thickness(2);
                        valid = false;
                    }
                }
            }
            else
            {
                return valid;
            }

            // Check visibility first, then check the data if the TextBoxes are visibile
            if (this.tbxWeight6.Visibility.Equals(Windows.UI.Xaml.Visibility.Visible))
            {
                if (!string.IsNullOrWhiteSpace(this.tbxWeight6.Text) || !string.IsNullOrWhiteSpace(this.tbxReps6.Text) || !string.IsNullOrWhiteSpace(this.tbxNote6.Text))
                {
                    // Check each TextBox
                    if (string.IsNullOrWhiteSpace(this.tbxWeight6.Text))
                    {
                        this.tbxWeight6.BorderThickness = new Thickness(2);
                        valid = false;
                    }

                    if (string.IsNullOrWhiteSpace(this.tbxReps6.Text))
                    {
                        this.tbxReps6.BorderThickness = new Thickness(2);
                        valid = false;
                    }
                }
            }
            else
            {
                return valid;
            }

            // Check visibility first, then check the data if the TextBoxes are visibile
            if (this.tbxWeight7.Visibility.Equals(Windows.UI.Xaml.Visibility.Visible))
            {
                if (!string.IsNullOrWhiteSpace(this.tbxWeight7.Text) || !string.IsNullOrWhiteSpace(this.tbxReps7.Text) || !string.IsNullOrWhiteSpace(this.tbxNote7.Text))
                {
                    // Check each TextBox
                    if (string.IsNullOrWhiteSpace(this.tbxWeight7.Text))
                    {
                        this.tbxWeight7.BorderThickness = new Thickness(2);
                        valid = false;
                    }

                    if (string.IsNullOrWhiteSpace(this.tbxReps7.Text))
                    {
                        this.tbxReps7.BorderThickness = new Thickness(2);
                        valid = false;
                    }
                }
            }
            else
            {
                return valid;
            }

            // Check visibility first, then check the data if the TextBoxes are visibile
            if (this.tbxWeight8.Visibility.Equals(Windows.UI.Xaml.Visibility.Visible))
            {
                if (!string.IsNullOrWhiteSpace(this.tbxWeight8.Text) || !string.IsNullOrWhiteSpace(this.tbxReps7.Text) || !string.IsNullOrWhiteSpace(this.tbxNote7.Text))
                {
                    // Check each TextBox
                    if (string.IsNullOrWhiteSpace(this.tbxWeight8.Text))
                    {
                        this.tbxWeight8.BorderThickness = new Thickness(2);
                        valid = false;
                    }

                    if (string.IsNullOrWhiteSpace(this.tbxReps8.Text))
                    {
                        this.tbxReps8.BorderThickness = new Thickness(2);
                        valid = false;
                    }
                }
            }
            else
            {
                return valid;
            }

            // Check visibility first, then check the data if the TextBoxes are visibile
            if (this.tbxWeight9.Visibility.Equals(Windows.UI.Xaml.Visibility.Visible))
            {
                if (!string.IsNullOrWhiteSpace(this.tbxWeight9.Text) || !string.IsNullOrWhiteSpace(this.tbxReps9.Text) || !string.IsNullOrWhiteSpace(this.tbxNote9.Text))
                {
                    // Check each TextBox
                    if (string.IsNullOrWhiteSpace(this.tbxWeight9.Text))
                    {
                        this.tbxWeight9.BorderThickness = new Thickness(2);
                        valid = false;
                    }

                    if (string.IsNullOrWhiteSpace(this.tbxReps9.Text))
                    {
                        this.tbxReps9.BorderThickness = new Thickness(2);
                        valid = false;
                    }
                }
            }
            else
            {
                return valid;
            }

            // Check visibility first, then check the data if the TextBoxes are visibile
            if (this.tbxWeight10.Visibility.Equals(Windows.UI.Xaml.Visibility.Visible))
            {
                if (!string.IsNullOrWhiteSpace(this.tbxWeight10.Text) || !string.IsNullOrWhiteSpace(this.tbxReps10.Text) || !string.IsNullOrWhiteSpace(this.tbxNote10.Text))
                {
                    // Check each TextBox
                    if (string.IsNullOrWhiteSpace(this.tbxWeight10.Text))
                    {
                        this.tbxWeight10.BorderThickness = new Thickness(2);
                        valid = false;
                    }

                    if (string.IsNullOrWhiteSpace(this.tbxReps10.Text))
                    {
                        this.tbxReps10.BorderThickness = new Thickness(2);
                        valid = false;
                    }
                }
            }
            else
            {
                return valid;
            }

            return valid;
        }

        /// <summary>
        /// This method will put an empty string in to each TextBox used for recording a workout
        /// </summary>
        private void ClearRecordWorkoutElements()
        {
            this.tbxWeight1.Text = string.Empty;
            this.tbxReps1.Text = string.Empty;
            this.tbxNote1.Text = string.Empty;
            this.tbxWeight2.Text = string.Empty;
            this.tbxReps2.Text = string.Empty;
            this.tbxNote2.Text = string.Empty;
            this.tbxWeight3.Text = string.Empty;
            this.tbxReps3.Text = string.Empty;
            this.tbxNote3.Text = string.Empty;
            this.tbxWeight4.Text = string.Empty;
            this.tbxReps4.Text = string.Empty;
            this.tbxNote4.Text = string.Empty;
            this.tbxWeight5.Text = string.Empty;
            this.tbxReps5.Text = string.Empty;
            this.tbxNote5.Text = string.Empty;
            this.tbxWeight6.Text = string.Empty;
            this.tbxReps6.Text = string.Empty;
            this.tbxNote6.Text = string.Empty;
            this.tbxWeight7.Text = string.Empty;
            this.tbxReps7.Text = string.Empty;
            this.tbxNote7.Text = string.Empty;
            this.tbxWeight8.Text = string.Empty;
            this.tbxReps8.Text = string.Empty;
            this.tbxNote8.Text = string.Empty;
            this.tbxWeight9.Text = string.Empty;
            this.tbxReps9.Text = string.Empty;
            this.tbxNote9.Text = string.Empty;
            this.tbxWeight10.Text = string.Empty;
            this.tbxReps10.Text = string.Empty;
            this.tbxNote10.Text = string.Empty;

            this.tbxRecordWorkoutNote.Text = string.Empty;
        }

        /// <summary>
        /// This method sets the Border Thickness of each textBox used in recording sets to zero.
        /// </summary>
        private void ResetBorderForRecordWorkout()
        {
            this.tbxWeight1.BorderThickness = new Thickness(0);
            this.tbxReps1.BorderThickness = new Thickness(0);
            this.tbxNote1.BorderThickness = new Thickness(0);

            this.tbxWeight2.BorderThickness = new Thickness(0);
            this.tbxReps2.BorderThickness = new Thickness(0);
            this.tbxNote2.BorderThickness = new Thickness(0);

            this.tbxWeight3.BorderThickness = new Thickness(0);
            this.tbxReps3.BorderThickness = new Thickness(0);
            this.tbxNote3.BorderThickness = new Thickness(0);

            this.tbxWeight4.BorderThickness = new Thickness(0);
            this.tbxReps4.BorderThickness = new Thickness(0);
            this.tbxNote4.BorderThickness = new Thickness(0);

            this.tbxWeight5.BorderThickness = new Thickness(0);
            this.tbxReps5.BorderThickness = new Thickness(0);
            this.tbxNote5.BorderThickness = new Thickness(0);

            this.tbxWeight6.BorderThickness = new Thickness(0);
            this.tbxReps6.BorderThickness = new Thickness(0);
            this.tbxNote6.BorderThickness = new Thickness(0);

            this.tbxWeight7.BorderThickness = new Thickness(0);
            this.tbxReps7.BorderThickness = new Thickness(0);
            this.tbxNote7.BorderThickness = new Thickness(0);

            this.tbxWeight8.BorderThickness = new Thickness(0);
            this.tbxReps8.BorderThickness = new Thickness(0);
            this.tbxNote8.BorderThickness = new Thickness(0);

            this.tbxWeight9.BorderThickness = new Thickness(0);
            this.tbxReps9.BorderThickness = new Thickness(0);
            this.tbxNote9.BorderThickness = new Thickness(0);

            this.tbxWeight10.BorderThickness = new Thickness(0);
            this.tbxReps10.BorderThickness = new Thickness(0);
            this.tbxNote10.BorderThickness = new Thickness(0);
        }

        /// <summary>
        /// This method will tidy up all the UI elements involved in displaying
        /// information about a Program. This method should be called when the user
        /// wants to use another area on this page and stop looking at the "RecordWorkout"
        /// area.
        /// </summary>
        private void CleanUpRecordWorkout()
        {
            // Hide the elements for the "RecordWorkout" area
            this.CollapseRecordWorkoutElements();

            // Empty the text in the TextBoxes in the "RecordWorkout" area
            this.ClearRecordWorkoutElements();

            // Reset the border thickness of the Set Textboxes
            this.ResetBorderForRecordWorkout();
        }

        /// <summary>
        /// This method is invoked when a key is presssed down in one of the TextBoxes used for holding data
        /// about either the Weight or Repetitions for a Set. This method will only accept characters that are
        /// digits. It will also accept a single decimal point (.) from the num pad. Any other characters will
        /// not be added to the TextBox that invoked this method.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void SetTextBox_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            // Check for only one decimal point
            if (e.Key == Windows.System.VirtualKey.Decimal && (sender as TextBox).Text.IndexOf('.') == -1)
            {
                return;
            }

            // Check if the value is too small or big to be a number
            if (e.Key < Windows.System.VirtualKey.Number0 || e.Key > Windows.System.VirtualKey.NumberPad9)
            {
                e.Handled = true;
            }

            // Check if the value is in between 0 - 9 and 0 - 9 on the num pad
            if (e.Key > Windows.System.VirtualKey.Number9 && e.Key < Windows.System.VirtualKey.NumberPad0)
            {
                e.Handled = true;
            }
        }

        #endregion RecordWorkout Methods

        #region WorkoutInfo Methods

        /// <summary>
        /// This method will set the visibility of each element used in displaying information about
        /// a Workout to Collapsed.
        /// </summary>
        private void CollapseWorkoutInfoElements()
        {
            // Hide the GridView containing all of the elements for "WorkoutInfo"
            this.gridWorkoutInfo.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        /// <summary>
        /// This method will set the visibility of each element used in displaying information about
        /// a Workout to Visible.
        /// </summary>
        private void ShowWorkoutInfoElements()
        {
            // Show the GridView containing all of the elements for "WorkoutInfo"
            this.gridWorkoutInfo.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        /// <summary>
        /// This method will put an empty string in to each TextBox used for displaying information
        /// about a Workout.
        /// </summary>
        private void ClearWorkoutInfoElements()
        {
            this.tbWorkoutInfoSubheader.Text = string.Empty;
            this.tbWorkoutInfoNotesDetails.Text = string.Empty;
            this.tbWorkoutInfoSetsDetails.Text = string.Empty;
        }

        /// <summary>
        /// This method will tidy up all the UI elements involved in displaying
        /// information about a Workout. This method should be called when the user
        /// wants to use another area on this page and stop looking at the "WorkoutInfo"
        /// area.
        /// </summary>
        private void CleanUpWorkoutInfo()
        {
            // Hide the elements for the "WorkoutInfo" area
            this.CollapseWorkoutInfoElements();

            // Empty the text in the TextBoxes in the "WorkoutInfo" area
            this.ClearWorkoutInfoElements();
        }

        #endregion WorkoutInfo Methods

        #region Set Collecting Methods

        /// <summary>
        /// This method will get all of the sets entered by the user when they
        /// are recording a workout.
        /// </summary>
        /// <returns> A List of Set objects for each Set the user entered. </returns>
        private List<Set> CollectSets()
        {
            List<Set> sets = new List<Set>();

            // Try get the first Set
            Set first = this.TryGetSet(this.tbxWeight1, this.tbxReps1, this.tbxNote1);

            if (first != null)
            {
                sets.Add(first);
            }
            else
            {
                return null;
            }

            // Check if the user entered data for the second and third Set
            // If the Weight and Rep TextBoxes are empty for the second Set, no more sets were entered
            if (string.IsNullOrWhiteSpace(this.tbxWeight2.Text) && string.IsNullOrWhiteSpace(this.tbxReps2.Text))
            {
                return sets;
            }
            else
            {
                // Collect the data for the second Set. if the Weight or Rep values are invalid,
                // return null and highlight the TextBox with invalid data
                Set second = this.TryGetSet(this.tbxWeight2, this.tbxReps2, this.tbxNote2);
                if (second != null)
                {
                    sets.Add(second);
                }
                else
                {
                    return null;
                }
            }

            // If the Weight and Rep TextBoxes are empty for the third Set, no more sets were entered
            if (string.IsNullOrWhiteSpace(this.tbxWeight3.Text) && string.IsNullOrWhiteSpace(this.tbxReps3.Text))
            {
                return sets;
            }
            else
            {
                // Collect the data for the third Set. if the Weight or Rep values are invalid,
                // return null and highlight the TextBox with invalid data
                Set third = this.TryGetSet(this.tbxWeight3, this.tbxReps3, this.tbxNote3);
                if (third != null)
                {
                    sets.Add(third);
                }
                else
                {
                    return null;
                }
            }

            // Now check if the user entered data for the remaining Sets (4 to 10).
            // Before checking each set, check the visibility of the Set's TextBoxes.
            // If the Visibility is set to Collapsed, stop checking the Sets and return
            // what has been found already.

            // Check if the fourth Set's TextBoxes are visible and they contain valid data
            if (this.SetHasValuesAndIsVisible(this.tbxWeight4, this.tbxReps4))
            {
                Set fourth = this.TryGetSet(this.tbxWeight4, this.tbxReps4, this.tbxNote4);
                if (fourth != null)
                {
                    sets.Add(fourth);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                // User did not enter data for this Set, return the current List of sets
                return sets;
            }

            // Check if the fifth Set's TextBoxes are visible and they contain valid data
            if (this.SetHasValuesAndIsVisible(this.tbxWeight5, this.tbxReps5))
            {
                Set fifth = this.TryGetSet(this.tbxWeight5, this.tbxReps5, this.tbxNote5);
                if (fifth != null)
                {
                    sets.Add(fifth);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                // User did not enter data for this Set, return the current List of sets
                return sets;
            }

            // Check if the sixth Set's TextBoxes are visible and they contain valid data
            if (this.SetHasValuesAndIsVisible(this.tbxWeight6, this.tbxReps6))
            {
                Set sixth = this.TryGetSet(this.tbxWeight6, this.tbxReps6, this.tbxNote6);
                if (sixth != null)
                {
                    sets.Add(sixth);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                // User did not enter data for this Set, return the current List of sets
                return sets;
            }

            // Check if the seventh Set's TextBoxes are visible and they contain valid data
            if (this.SetHasValuesAndIsVisible(this.tbxWeight7, this.tbxReps7))
            {
                Set seventh = this.TryGetSet(this.tbxWeight7, this.tbxReps7, this.tbxNote7);
                if (seventh != null)
                {
                    sets.Add(seventh);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                // User did not enter data for this Set, return the current List of sets
                return sets;
            }

            // Check if the eighth Set's TextBoxes are visible and they contain valid data
            if (this.SetHasValuesAndIsVisible(this.tbxWeight8, this.tbxReps8))
            {
                Set eighth = this.TryGetSet(this.tbxWeight8, this.tbxReps8, this.tbxNote8);
                if (eighth != null)
                {
                    sets.Add(eighth);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                // User did not enter data for this Set, return the current List of sets
                return sets;
            }

            // Check if the ninth Set's TextBoxes are visible and they contain valid data
            if (this.SetHasValuesAndIsVisible(this.tbxWeight9, this.tbxReps9))
            {
                Set ninth = this.TryGetSet(this.tbxWeight9, this.tbxReps9, this.tbxNote9);
                if (ninth != null)
                {
                    sets.Add(ninth);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                // User did not enter data for this Set, return the current List of sets
                return sets;
            }

            // Check if the tenth Set's TextBoxes are visible and they contain valid data
            if (this.SetHasValuesAndIsVisible(this.tbxWeight10, this.tbxReps10))
            {
                Set tenth = this.TryGetSet(this.tbxWeight10, this.tbxReps10, this.tbxNote10);
                if (tenth != null)
                {
                    sets.Add(tenth);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                // User did not enter data for this Set, return the current List of sets
                return sets;
            }

            return sets;
        }

        /// <summary>
        /// This method takes two TextBoxes (that contian data for a Set) and detemines if a user
        /// entered incorrect values or did not enter anything at all.
        /// </summary>
        /// <param name="tbxWeight"> The TextBox containing a value for the Weight in a Set. </param>
        /// <param name="tbxReps"> The TextBox containing a value for the Repetitions in a Set. </param>
        /// <returns> True if a Set object can be created from the data in the TextBoxes. </returns>
        private bool SetHasValuesAndIsVisible(TextBox tbxWeight, TextBox tbxReps)
        {
            // Check visibility of this Set object's TextBoxes
            if (tbxWeight.Visibility.Equals(Windows.UI.Xaml.Visibility.Collapsed))
            {
                return false;
            }

            // If the Weight and Rep TextBoxes are empty for the this Set, no more sets were entered
            if (string.IsNullOrWhiteSpace(tbxWeight.Text) && string.IsNullOrWhiteSpace(tbxReps.Text))
            {

                return false;
            }

            // The Set's TextBoxes are visible and contain valid data
            return true;
        }

        /// <summary>
        /// This method takes three TextBoxes as parameters and tries to parse the values
        /// to craete a Set object. If a "weight" and a "repetitions" value can not be
        /// parsed, then null is returned.
        /// </summary>
        /// <param name="tbxWeight"> A TextBox containing the value for the weight in the Set. </param>
        /// <param name="tbxReps"> A TextBox containing the value for the reps in the Set. </param>
        /// <param name="tbxNote"> A TextBox containing the value for the note in the Set. </param>
        /// <returns> A Set object with the values from the TextBox parameters. Returns null
        /// if a "weight" and a "repetitions" value could not be parsed. </returns>
        private Set TryGetSet(TextBox tbxWeight, TextBox tbxReps, TextBox tbxNote)
        {
            // Try parse the value in the Weight TextBox
            double weight = this.TextBoxContainsDouble(tbxWeight);
            if (weight == -1)
            {
                tbxWeight.BorderThickness = new Thickness(2);
                return null;
            }

            // Try parse the value in the Repetitions textBox
            int reps = this.TextBoxContainsInt(tbxReps);
            if (reps == -1)
            {
                tbxReps.BorderThickness = new Thickness(2);
                return null;
            }

            // If the value supplied for the note is null replace it with the empty string
            if (string.IsNullOrWhiteSpace(tbxNote.Text))
            {
                tbxNote.Text = string.Empty;
            }

            return new Set(weight, reps, tbxNote.Text);
        }

        /// <summary>
        /// Tries to parse the Text value of a TextBox to a Double.
        /// </summary>
        /// <param name="tbx"> The TextBox that contains the data to try parse. </param>
        /// <returns> Returns the Double value of the data in the TextBox. If the value can't
        /// be parsed thent his method returns -1. </returns>
        private double TextBoxContainsDouble(TextBox tbx)
        {
            // Check if the TextBox contains 0
            if (tbx.Text.Equals("0") || tbx.Text.Equals("0.0") || tbx.Text.Equals(".0"))
            {
                return 0;
            }

            double dValue;

            // Try parse the value in the TextBox to a Double
            if (!Double.TryParse(tbx.Text, out dValue))
            {
                // The value can't be parsed to either a Double so return -1
                return -1;
            }

            return dValue;
        }

        /// <summary>
        /// Tries to parse the Text value of a TextBox to an int.
        /// </summary>
        /// <param name="tbx"> The TextBox that contains the data to try parse. </param>
        /// <returns> Returns the int value of the data in the TextBox. If the value can't
        /// be parsed thent his method returns -1. </returns>
        private int TextBoxContainsInt(TextBox tbx)
        {
            int dInt;

            // Try parse the value in the TextBox to an int
            if (!Int32.TryParse(tbx.Text, out dInt))
            {
                // The value can't be parsed to either an int so return -1
                return -1;
            }

            return dInt;
        }

        #endregion Set Collecting Methods

        #region OnFocus Methods for RecordWorkout TextBoxes

        /// <summary>
        /// This method is invoked when the "tbxWeight3" TextBox is focused. This method will
        /// make the TextBoxes "tbxWeight4" and "tbxReps4" visible.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void tbxWeight3_GotFocus(object sender, RoutedEventArgs e)
        {
            this.tbxWeight4.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxReps4.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxNote4.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        /// <summary>
        /// This method is invoked when the "tbxWeight4" TextBox is focused. This method will
        /// make the TextBoxes "tbxWeight5" and "tbxReps5" visible.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void tbxWeight4_GotFocus(object sender, RoutedEventArgs e)
        {
            this.tbxWeight5.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxReps5.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxNote5.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        /// <summary>
        /// This method is invoked when the "tbxWeight5" TextBox is focused. This method will
        /// make the TextBoxes "tbxWeight6" and "tbxReps6" visible.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void tbxWeight5_GotFocus(object sender, RoutedEventArgs e)
        {
            this.tbxWeight6.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxReps6.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxNote6.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        /// <summary>
        /// This method is invoked when the "tbxWeight6" TextBox is focused. This method will
        /// make the TextBoxes "tbxWeight7" and "tbxReps7" visible.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void tbxWeight6_GotFocus(object sender, RoutedEventArgs e)
        {
            this.tbxWeight7.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxReps7.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxNote7.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        /// <summary>
        /// This method is invoked when the "tbxWeight7" TextBox is focused. This method will
        /// make the TextBoxes "tbxWeight8" and "tbxReps8" visible.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void tbxWeight7_GotFocus(object sender, RoutedEventArgs e)
        {
            this.tbxWeight8.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxReps8.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxNote8.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        /// <summary>
        /// This method is invoked when the "tbxWeight8" TextBox is focused. This method will
        /// make the TextBoxes "tbxWeight9" and "tbxReps9" visible.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void tbxWeight8_GotFocus(object sender, RoutedEventArgs e)
        {
            this.tbxWeight9.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxReps9.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxNote9.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        /// <summary>
        /// This method is invoked when the "tbxWeight9" TextBox is focused. This method will
        /// make the TextBoxes "tbxWeight10" and "tbxReps10" visible.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void tbxWeight9_GotFocus(object sender, RoutedEventArgs e)
        {
            this.tbxWeight10.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxReps10.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxNote10.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        #endregion OnFocus Methods for RecordWorkout TextBoxes
    }
}
