﻿//-----------------------------------------------------------------------
// <copyright file="MainPage.xaml.cs" company="EvanDolan">
//     Copyright (c) Evan Dolan. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace KeepFit
{
    using JsonClasses;
    using KeeptFitAPI;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text.RegularExpressions;
    using Windows.Data.Xml.Dom;
    using Windows.Foundation;
    using Windows.Foundation.Collections;
    using Windows.UI.Notifications;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;
    using Windows.UI.Xaml.Controls.Primitives;
    using Windows.UI.Xaml.Data;
    using Windows.UI.Xaml.Input;
    using Windows.UI.Xaml.Media;
    using Windows.UI.Xaml.Navigation;

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainPage"/> class.
        /// </summary>
        public MainPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // Check if a parameter was passed before checking its values
            if (e.Parameter != null)
            {
                if ((e.Parameter as string).Equals("logout"))
                {
                    // Show a toast to show successful logout
                    this.ShowLogoutToast();
                }
            }
        }
        
        /// <summary>
        /// This method is called when the user clicks the 'Login' button. The method will
        /// attempt to log the user into the system. It will use the username and password
        /// supplied by the user in from the 'Login Username' TextBox and 'Login Password' PasswordBox.
        /// If the login is successful, the method will navigate to the next page and pass an APICaller
        /// object with the users login credentials. If the login fails, the App will not change page
        /// and the user will be informed that the login has failed.
        /// </summary>
        /// <param name="sender"> The parameter is not used. </param>
        /// <param name="e"> The parameter is not used. </param>
        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            // Set the username TextBox and password PasswordBox to default border thickness of 0
            this.tbxLoginUsername.BorderThickness = new Thickness(0);
            this.pbLoginPassword.BorderThickness = new Thickness(0);

            // Create JsonReply for the hold the reply from the server, if any
            JsonReply jr = new JsonReply();

            // Check if the Username or Password entered is null or empty
            if (this.AreUsernameAndPasswordValid()) 
            {
                // Get the users login credentials, create an APICaller object,
                byte[] auth = System.Text.Encoding.UTF8.GetBytes(this.tbxLoginUsername.Text + ":" + this.pbLoginPassword.Password);
                APICaller ac = new APICaller(System.Convert.ToBase64String(auth), this.tbxLoginUsername.Text);

                // Make call to server and try log in
                jr = ac.Login();

                if (jr.errorCode == APIErrorCodes.Success)
                {
                    // Set the previous page local variable
                    Windows.Storage.ApplicationData.Current.LocalSettings.Values["previous_page"] = "main";

                    // Successful login, navigate to next page
                    this.Frame.Navigate(typeof(HomePage), ac);
                }
            }
            else
            {
                // No call will be made to the server. Set the JsonReply status manually
                jr.status = "The username or password entered is not valid.";
            }

            // Login has failed, inform user and stay on this page
            this.tbFailedLogin.Text = "The login has failed. The following error was received:\n";
            this.tbFailedLogin.Text += jr.status;

            // If the username or password are incorrect, set their TextBox BorderThickness to 2
            if (jr.errorCode.Equals(APIErrorCodes.IncorrectUsername))
            {
                this.tbxLoginUsername.BorderThickness = new Thickness(2);
            }
            else if (jr.errorCode.Equals(APIErrorCodes.IncorrectPassword))
            {
                this.pbLoginPassword.BorderThickness = new Thickness(2);
            }
            
            // Set the "btnGoToRegisters" button visibility to collapsed.
            this.btnGoToRegisterSmall.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

            // Set the "btnGoToRegisters" button visibility to visible.
            this.btnGoToRegister.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        /// <summary>
        /// This method displays a toast to notify the user of a successful logout.
        /// </summary>
        private void ShowLogoutToast()
        {
            ToastTemplateType toastType = ToastTemplateType.ToastImageAndText02;
            XmlDocument toastXML = ToastNotificationManager.GetTemplateContent(toastType);
            XmlNodeList toastText = toastXML.GetElementsByTagName("text");
            XmlNodeList toastImages = toastXML.GetElementsByTagName("image");
            toastText[0].InnerText = "Successfully Logged Out";
            toastText[1].InnerText = "Goodbye!";
            ((XmlElement)toastImages[0]).SetAttribute("src", "ms-appx:///Assets/Images/150x150 White.png");

            // Toast duration is set to "short" by default
            ToastNotification logoutToast = new ToastNotification(toastXML);
            logoutToast.ExpirationTime = DateTimeOffset.UtcNow.AddSeconds(3000);
            ToastNotificationManager.CreateToastNotifier().Show(logoutToast);
        }

        /// <summary>
        /// This method checks if the string supplied represents a valid username.
        /// </summary>
        /// <param name="username"> A String representing a username. </param>
        /// <returns> True is the string represents a valid username. Otherwise false. </returns>
        private bool IsValidUsername(string username)
        {
            // The username must begin with a letter and can contain any combination of letters, numbers, and the "_" and "-" symbols
            Regex rx = new Regex("^[A-Za-z]+[A-Za-z0-9_-]*$");
            return rx.IsMatch(username);
        }

        /// <summary>
        /// This method validates the values entered in the Username and Password text fields.
        /// If either value is null, empty, or just white space the method will return false.
        /// </summary>
        /// <returns> True if a non-empty and non-null value is present in the Username and Password
        /// fields. Otherwise, false. </returns>
        private bool AreUsernameAndPasswordNonEmpty()
        {
            // Check Username field
            if (string.IsNullOrWhiteSpace(this.tbxLoginUsername.Text))
            {
                this.tbxLoginUsername.BorderThickness = new Thickness(2);
                return false;
            }

            // Check Password field
            if (string.IsNullOrWhiteSpace(this.pbLoginPassword.Password))
            {
                this.pbLoginPassword.BorderThickness = new Thickness(2);
                return false;
            }

            // If this statement is reached, both of the fields are not empty
            return true;
        }

        /// <summary>
        /// This method validates the values entered in the Username and Password text fields.
        /// </summary>
        /// <returns> True if a non-empty and non-null value is present in the Username and Password
        /// fields and the username is in a vlid format. Otherwise, false. </returns>
        private bool AreUsernameAndPasswordValid()
        {
            // Check if the username value is valid and then if all values are non-empty
            if (!this.IsValidUsername(this.tbxLoginUsername.Text))
            {
                this.tbxLoginUsername.BorderThickness = new Thickness(2);
                return false;
            }

            return this.AreUsernameAndPasswordNonEmpty();
        }

        /// <summary>
        /// This method is called then the "btnGoToRegister" button is clicked. This method will
        /// navigate the App to the Register page. This method will pass the contents of the
        /// the text in the "tbxLoginUsername" TextBox. If the TextBox is empty, an empty string will
        /// be passed instead.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void btnGoToRegister_Click(object sender, RoutedEventArgs e)
        {
            // Set the previous page local variable
            Windows.Storage.ApplicationData.Current.LocalSettings.Values["previous_page"] = "main";

            // If the user entered something in the "tbxLoginUsername" TextBox, pass the
            // string as an object to the next page
            if (this.tbxLoginUsername.Text != null)
            {
                this.Frame.Navigate(typeof(RegisterPage), this.tbxLoginUsername.Text);
            }
            else
            {
                this.Frame.Navigate(typeof(RegisterPage), string.Empty);
            }
        }
    }
}
