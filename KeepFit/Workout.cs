﻿//-----------------------------------------------------------------------
// <copyright file="Workout.cs" company="EvanDolan">
//     Copyright (c) Evan Dolan. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace KeepFit
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    
    /// <summary>
    /// This class will act as a data structure that represents a Workout.
    /// </summary>
    public class Workout
    {
        #region Variable Fields

        /// <summary>
        /// This is the name given to this Workout by the user.
        /// </summary>
        private string name;

        /// <summary>
        /// This is a note added by the user about this Workout.
        /// </summary>
        private string note;

        /////// <summary>
        /////// This is a list of all of the Sets performed in this Workout.
        /////// </summary>
        ////private List<Set> sets;

        #endregion Variable Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Workout"/> class.
        /// </summary>
        /// <param name="name"> The name of the Workout. </param>
        public Workout(string name)
        {
            this.Name = name;
            this.Note = string.Empty;
            this.Sets = new List<Set>();
        }

        #endregion Constructors

        #region Variable Properties

        /// <summary>
        /// Gets or sets the name of the Workout.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        /// <summary>
        /// Gets or sets the note in the Workout.
        /// </summary>
        public string Note
        {
            get { return this.note; }
            set { this.note = value; }
        }

        /// <summary>
        /// Gets the list of Sets in this Workout.
        /// </summary>
        public List<Set> Sets
        {
            get; // { return this.sets; }
            private set;
        }

        #endregion Variable Properties

        #region Methods

        /// <summary>
        /// This method returns a short String that contains the data held in this object.
        /// </summary>
        /// <returns> A String that represents a Workout object. </returns>
        public override string ToString()
        {
            string info = this.Name + "\n";
            
            foreach (Set s in this.Sets)
            {
                info += "\t" + s.ToString() + "\n";
            }
            
            return info;
        }

        /// <summary>
        /// This method takes two Integers, uses them to create a Set object,
        /// and then adds the Set object to the list of Sets.
        /// </summary>
        /// <param name="weight"> The weight of the Set to be added to the list. </param>
        /// <param name="repetitions"> The amount of repetitions of the Set to be added to the list. </param>
        public void AddSet(double weight, int repetitions)
        {
            this.Sets.Add(new Set(weight, repetitions));
        }

        /// <summary>
        /// This method takes a Set object and adds it to the list of Sets.
        /// </summary>
        /// <param name="s"> The Set object to be added to the list. </param>
        public void AddSet(Set s)
        {
            this.Sets.Add(s);
        }

        /// <summary>
        /// This method returns a list of each Sets' weight and repetition amounts.
        /// </summary>
        /// <returns> A list containing all the numerical values of the Sets in this Workout. </returns>
        public List<double> GetSetsAsDoubleList()
        {
            List<double> setValues = new List<double>();
            
            foreach (Set s in this.Sets)
            {
                setValues.Add(s.Weight);
                setValues.Add(s.Repetitions);
            }
            
            return setValues;
        }

        /// <summary>
        /// This method will return all of the data in this object as a String. 
        /// </summary>
        /// <returns> A String representing all data contained in a Workout object. </returns>
        public string ToStringLong()
        {
            string setsLong = this.Name + "\n";

            // Check if there is an entry for Note
            if (this.Note.Length > 0)
            {
                setsLong += "Notes: " + this.Note + "\n";
            }

            foreach (Set s in this.Sets)
            {
                setsLong += "\t\t" + s.ToStringLong() + "\n";
            }

            return setsLong;
        }

        /// <summary>
        /// This method returns a String containing only the name of the Workout and the numerical values in each Set.
        /// </summary>
        /// <returns> A String containing the values of each Sets' weight and repetition amounts. </returns>
        public string ToStringShort()
        {
            string setsShort = this.Name + "\n";

            foreach (Set s in this.Sets)
            {
                setsShort += s.ToString() + "\n";
            }

            return setsShort;
        }

        #endregion Methods
    }
}
