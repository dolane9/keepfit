﻿//-----------------------------------------------------------------------
// <copyright file="JsonReply.cs" company="EvanDolan">
//     Copyright (c) Evan Dolan. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace KeepFit.JsonClasses
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents the structure of a JSON object for a reply from the server for
    /// certain API calls.
    /// This class will Suppress StyleCops warning message SA1300 because Json objects
    /// used all lower-case for the naming of variables.
    /// </summary>
    [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter",
        Justification = "This class represents a JSON object. All variables should be all lower-case.")]
    public class JsonReply
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="JsonReply"/> class.
        /// </summary>
        public JsonReply()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="JsonReply"/> class.
        /// </summary>
        /// <param name="status"> A status for this JsonReply. </param>
        /// <param name="errorCode"> An error code for this JsonReply. </param>
        public JsonReply(string status, int errorCode)
        {
            this.status = status;
            this.errorCode = errorCode;
        }

        #endregion Constructors

        #region Variable Fields

        /// <summary>
        /// Gets or sets the status message for the reply from the server.
        /// </summary>
        public string status { get; set; }
        
        /// <summary>
        /// Gets or sets an error code that represents the error returned. This variable will be zero
        /// if there was no error returned.
        /// </summary>
        public int errorCode { get; set; }

        #endregion  Variable Fields

        #region Methods

        /// <summary>
        /// Overrides the ToString() method for the base class.
        /// </summary>
        /// <returns> A String containing the data held in each variable in this class. </returns>
        public override string ToString()
        {
            return "Status: " + this.status + "\nErrorCode: " + this.errorCode;
        }

        #endregion Methods
    }
}
