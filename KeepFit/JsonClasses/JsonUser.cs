﻿//-----------------------------------------------------------------------
// <copyright file="JsonUser.cs" company="EvanDolan">
//     Copyright (c) Evan Dolan. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace KeepFit.JsonClasses
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// A Class that encapsulates a User as a Json object and a RootObject
    /// which is a status message and a List of users.
    /// </summary>
    class JsonUser
    {
        /// <summary>
        /// A class that represetns a JSON object which represents a user.
        /// This class will Suppress StyleCops warning message SA1300 because Json objects
        /// used all lower-case for the naming of variables.
        /// </summary>
        [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter",
            Justification = "This class represents a JSON object. All variables should be all lower-case.")]
        public class User
        {
            #region Variable Fields

            /// <summary>
            /// Gets or sets the users email.
            /// </summary>
            public string email { get; set; }

            /// <summary>
            /// Gets or sets the users first name.
            /// </summary>
            public string first { get; set; }

            /// <summary>
            /// Gets or sets the users last name.
            /// </summary>
            public string last { get; set; }

            /// <summary>
            /// Gets or sets the users username.
            /// </summary>
            public string username { get; set; }

            /// <summary>
            /// Gets or sets the users hashed password.
            /// </summary>
            public string pass_hash { get; set; }

            /// <summary>
            /// Gets or sets the salt value used to hash the users password.
            /// </summary>
            public string salt { get; set; }

            /// <summary>
            /// Gets or sets the object ID for this JSON object in the database.
            /// </summary>
            public string _id { get; set; }

            /// <summary>
            /// Gets or sets the version number of this object.
            /// </summary>
            public int __v { get; set; }

            #endregion Variable Fields

            #region Methods

            /// <summary>
            /// Overrides the ToString() method for the base class.
            /// </summary>
            /// <returns> A String containing the data held in each variable in this class. </returns>
            public override string ToString()
            {
                string info = "Email: " + this.email;
                info += "\nName: " + this.first + " " + this.last;
                info += "\nUsername: " + this.username;
                info += "\nID: " + this._id;
                return info + "\nVersion: " + this.__v;
            }

            #endregion Methods
        }

        /// <summary>
        /// Contains a status message from the server and a List of User objects
        /// </summary>
        public class RootObject
        {
            /// <summary>
            /// Gets or sets the status message.
            /// </summary>
            public string status { get; set; }

            /// <summary>
            /// Gets or sets the List of Users
            /// </summary>
            public User users { get; set; }
        }
    }
}
