﻿//-----------------------------------------------------------------------
// <copyright file="JsonWorkoutProgram.cs" company="EvanDolan">
//     Copyright (c) Evan Dolan. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace KeepFit.JsonClasses
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents the structure of a JSON object for the <see cref="WorkoutProgram"/> class.
    /// This class will Suppress StyleCops warning message SA1300 because Json objects
    /// used all lower-case for the naming of variables.
    /// </summary>
    [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter",
        Justification = "This class represents a JSON object. All variables should be all lower-case.")]

    public class JsonWorkoutProgram
    {
        /// <summary>
        /// A class that represents  <see cref="WorkoutProgram"/> class in Json format.
        /// </summary>
        public class WorkoutProgramDetails
        {
            #region Variable Fields

            /// <summary>
            /// Gets or sets the ID of the User that owns this <see cref="WorkoutProgram"/> object.
            /// </summary>
            public string user_id { get; set; }

            /// <summary>
            /// Gets or sets the name of the <see cref="WorkoutProgram"/> object.
            /// </summary>
            public string name { get; set; }

            /// <summary>
            /// Gets or sets the note, is any, in the <see cref="WorkoutProgram"/> object.
            /// </summary>
            public string note { get; set; }
            
            /// <summary>
            /// Gets or sets a shorted version of the note variable. 20 chars at most.
            /// </summary>
            public string note_formatted
            {
                get
                {
                    if (string.IsNullOrWhiteSpace(this.note))
                    {
                        return "No note recorded.";
                    }
                    else if (this.note.Length < 21)
                    {
                        return this.note;
                    }
                    else
                    {
                        return this.note.Substring(0, 16) + "... ";
                    }
                }

                set
                {
                }
            }

            /// <summary>
            /// Gets or sets the ID for this <see cref="WorkoutProgram"/> object.
            /// </summary>
            public string _id { get; set; }

            /// <summary>
            /// Gets or sets the version number for this <see cref="WorkoutProgram"/> object.
            /// </summary>
            public int __v { get; set; }

            /// <summary>
            /// Gets or sets the timestamp for this <see cref="WorkoutProgram"/> object.
            /// </summary>
            public string timestamp { get; set; }

            /// <summary>
            /// Gets or sets a formatted version of the timestamp variable
            /// </summary>
            public string timestamp_formatted
            {
                // Return the first 10 characters in the timestamp (the data)
                get
                {
                    if (this.timestamp.Length > 9)
                    {
                        return this.timestamp.Substring(0, 10);
                    }
                    else
                    {
                        return this.timestamp;
                    }
                }

                set
                {
                }
            }

            /// <summary>
            /// Gets or sets the List of <see cref="JsonWorkout"/> objects owned by this object.
            /// </summary>
            public List<JsonWorkout.WorkoutDetails> workouts { get; set; }

            /// <summary>
            /// Gets or sets a string that contains the amount of JsonSet.SetDetails objects in this object
            /// </summary>
            public string workouts_count
            {
                get
                {
                    if (this.workouts == null)
                    {
                        return "Workouts: 0";
                    }
                    else
                    {
                        return "Workouts: " + this.workouts.Count;
                    }
                }

                set
                {
                }
            }

            #endregion Variable Fields

            #region Methods

            /// <summary>
            /// Overrides the ToString() method for the base class.
            /// </summary>
            /// <returns> A String containing the data held in each variable in this class. </returns>
            public override string ToString()
            {
                string info = "User ID: " + this.user_id;
                info += "\nName: " + this.name;
                info += "\nNote: " + this.note;
                info += "\nWorkouts: ";
                if (this.workouts != null)
                {
                    info += this.workouts.Count;
                }
                else
                {
                    info += "0";
                }

                return info + "\nTime Stamp: " + this.timestamp;
            }

            /// <summary>
            /// This method returns a string containing all of the JsonWorkout.WorkoutDetails
            /// objects in the workouts List. The method uses the ToStringLong() method
            /// from the JsonWorkout.WorkoutDetails class.
            /// </summary>
            /// <returns> A String containing maximum information on the sets in this class. </returns>
            public string WorkoutsToLongString()
            {
                // Check for if this.workouts is null
                if (this.workouts == null)
                {
                    return "No Workouts.";
                }

                string allWorkouts = string.Empty;
                for (int i = 0; i < this.workouts.Count; i++)
                {
                    allWorkouts += "Workout " + (i + 1) + ": " + this.workouts[i].name + "\n" + this.workouts[i].ToStringLong() + "\n";
                }

                return allWorkouts;
            }

            #endregion Methods
        }

        /// <summary>
        /// The class contains a List of <see cref="WorkoutProgramDetails"/> objects.
        /// Each <see cref="WorkoutProgramDetails"/> object represents a <see cref="WorkoutProgram"/> object.
        /// </summary>
        public class RootObject
        {
            #region Variable Fields

            /// <summary>
            /// Gets or sets a List of <see cref="WorkoutProgramDetails"/> objects, representing <see cref="WorkoutProgram"/> objects.
            /// </summary>
            public List<WorkoutProgramDetails> workoutprograms { get; set; }

            #endregion Variable Fields

            #region Methods

            /// <summary>
            /// Overrides the ToString() method for the base class.
            /// </summary>
            /// <returns> A String containing the data held in each variable in this class. </returns>
            public override string ToString()
            {
                string info = string.Empty;
                foreach (WorkoutProgramDetails d in this.workoutprograms)
                {
                    info += d.ToString();
                }

                return info;
            }

            #endregion Methods
        }
    }
}
