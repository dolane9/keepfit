﻿//-----------------------------------------------------------------------
// <copyright file="JsonWorkout.cs" company="EvanDolan">
//     Copyright (c) Evan Dolan. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace KeepFit.JsonClasses
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents the structure of a JSON object for the <see cref="Workout"/> class.
    /// This class will Suppress StyleCops warning message SA1300 because Json objects
    /// used all lower-case for the naming of variables.
    /// </summary>
    [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter",
        Justification = "This class represents a JSON object. All variables should be all lower-case.")]
    public class JsonWorkout
    {
        /// <summary>
        /// A class that represents  <see cref="Workout"/> class in Json format.
        /// </summary>
        public class WorkoutDetails
        {
            #region Variable Fields
            
            /// <summary>
            /// Gets or sets the ID of the User that owns this <see cref="Workout"/> object.
            /// </summary>
            public string user_id { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether: : True if this object is a target;
            /// otherwise, false. A target is a workout that a user is trying to achieve.
            /// It contains values that the user wants to achieve in their own workouts.
            /// </summary>
            public bool is_target { get; set; }

            /// <summary>
            /// Gets or sets the name of the <see cref="Workout"/> object.
            /// </summary>
            public string name { get; set; }

            /// <summary>
            /// Gets or sets the note, is any, in the <see cref="Workout"/> object.
            /// </summary>
            public string note { get; set; }

            /// <summary>
            /// Gets or sets a shorted version of the note variable. 20 chars at most.
            /// </summary>
            public string note_formatted
            {
                get
                {
                    if (string.IsNullOrWhiteSpace(this.note))
                    {
                        return "No note recorded.";
                    }
                    else if (this.note.Length < 21)
                    {
                        // Use PadRight(20) to ensure the note will be at least 20 characters long
                        return this.note.PadRight(20);
                    }
                    else
                    {
                        return this.note.Substring(0, 16) + "... ";
                    }
                }

                set
                {
                }
            }

            /// <summary>
            /// Gets or sets the ID for this <see cref="Workout"/> object.
            /// </summary>
            public string _id { get; set; }

            /// <summary>
            /// Gets or sets the version number for this <see cref="Workout"/> object.
            /// </summary>
            public int __v { get; set; }

            /// <summary>
            /// Gets or sets the timestamp for this <see cref="Workout"/> object.
            /// </summary>
            public string timestamp { get; set; }

            /// <summary>
            /// Gets or sets a formatted version of the timestamp variable
            /// </summary>
            public string timestamp_formatted
            {
                // Return the first 10 characters in the timestamp (the data)
                get
                {
                    if (this.timestamp.Length > 9)
                    {
                        return this.timestamp.Substring(0, 10);
                    }
                    else
                    {
                        return this.timestamp;
                    }
                }

                set
                {
                }
            }

            /// <summary>
            /// Gets or sets the List of <see cref="JsonSet"/> objects owned by this object.
            /// </summary>
            public List<JsonSet.SetDetails> sets { get; set; }

            /// <summary>
            /// Gets or sets a string of all the sets in a readable format.
            /// </summary>
            public string sets_formatted
            {
                get
                {
                    if (this.sets == null || this.sets.Count == 0)
                    {
                        return "empty sets";
                    }
                    else
                    {
                        return this.SetsToShortString();
                    }
                }

                set
                {
                }
            }

            /// <summary>
            /// Gets or sets the number of sets that belong to this object.
            /// </summary>
            public int num_sets { get; set; }

            /// <summary>
            /// Gets or sets the program index, if any, for this object.
            /// </summary>
            public int program_index { get; set; }

            #endregion Variable Fields

            #region Methods

            /// <summary>
            /// Overrides the ToString() method for the base class.
            /// </summary>
            /// <returns> A String containing the data held in each variable in this class. </returns>
            public override string ToString()
            {
                string info = "User ID: " + this.user_id;
                info += "\nName: " + this.name;
                info += "\nNote: " + this.note;
                info += "\nNum of sets: " + this.num_sets;
                info += "\nProgram index: " + this.program_index;
                info += "\n_id: " + this._id;
                if (this.num_sets > 0)
                {
                    info += "\nSets:\n" + this.SetsToShortString() + "\n";
                }

                return info;
            }

            /// <summary>
            /// This method returns a long String containing the data in this object.
            /// </summary>
            /// <returns> A String that contains a lot of detail on the data in this object. </returns>
            public string ToStringLong()
            {
                string info = "\nSets: " + this.sets.Count + "\n";
                if (this.num_sets > 0)
                {
                    info += this.SetsToLongString() + "\n";
                }

                if (string.IsNullOrWhiteSpace(this.note))
                {
                    info += "\nNote: " + this.note;
                }

                return info;
            }

            /// <summary>
            /// This method returns a string containing all of the Sets in this object.
            /// The string contains only the important details of each set.
            /// </summary>
            /// <returns> A String containing short information on all Sets. </returns>
            public string AllSetsToShortString()
            {
                // Check if the this.sets object is null
                if (this.sets == null)
                {
                    return "No sets recorded.";
                }

                // Create a String represeting the Sets values
                string allSets = "Weight   Reps\n";
                for (int i = 0; i < this.sets.Count; i++)
                {
                    allSets += this.sets[i].ToStringShort() + "\n";
                }

                return allSets;
            }

            /// <summary>
            /// This method returns a string containing all of the JsonSet.SetDetails
            /// objects in the sets List. The method uses the ToStringShort() method
            /// from the JsonSet.SetDetails class.
            /// </summary>
            /// <returns> A String containing minimum information on the sets in this class. </returns>
            public string SetsToShortString()
            {
                // Check if the this.sets object is null
                if (this.sets == null)
                {
                    return "No sets recorded.";
                }

                // Create a String represeting the Sets values
                string allSets = "Weight   Reps\n";
                for (int i = 0; i < 3 && i < this.sets.Count; i++)
                {
                    allSets += this.sets[i].ToStringShort() + "\n";
                }

                // Add new line characters if there are less than 3 sets
                for (int i = this.sets.Count; i < 3; i++)
                {
                    allSets += "\n";
                }

                if (this.sets.Count > 3)
                {
                    allSets += (this.sets.Count - 3) + " more sets...";
                }

                return allSets + "\n";
            }

            /// <summary>
            /// This method returns a string containing all of the JsonSet.SetDetails
            /// objects in the sets List. The method uses the ToStringLong() method
            /// from the JsonSet.SetDetails class.
            /// </summary>
            /// <returns> A String containing maximum information on the sets in this class. </returns>
            public string SetsToLongString()
            {
                // Check if the this.sets object is null
                if (this.sets == null)
                {
                    return "No sets recorded.";
                }

                string allSets = string.Empty;
                for (int i = 0; i < this.sets.Count; i++)
                {
                    allSets += this.sets[i].ToStringLong() + "\n\n";
                }

                return allSets;
            }

            #endregion Methods
        }

        /// <summary>
        /// The class contains a List of <see cref="WorkoutDetails"/> objects.
        /// Each <see cref="WorkoutDetails"/> object represents a <see cref="Workout"/> object.
        /// </summary>
        public class RootObject
        {
            #region Variable Fields
            
            /// <summary>
            /// Gets or sets a List of <see cref="WorkoutDetails"/> objects, representing <see cref="Workout"/> objects.
            /// </summary>
            public List<WorkoutDetails> workouts { get; set; }

            #endregion Variable Fields

            #region Methods

            /// <summary>
            /// Overrides the ToString() method for the base class.
            /// </summary>
            /// <returns> A String containing the data held in each variable in this class. </returns>
            public override string ToString()
            {
                string info = string.Empty;
                foreach (WorkoutDetails d in this.workouts)
                {
                    info += d.ToString() + "\n";
                }

                return info;
            }

            #endregion Methods
        }
    }
}
