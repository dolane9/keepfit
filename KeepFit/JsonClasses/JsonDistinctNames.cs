﻿//-----------------------------------------------------------------------
// <copyright file="JsonDistinctWorkout.cs" company="EvanDolan">
//     Copyright (c) Evan Dolan. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace KeepFit.JsonClasses
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents the structure of a JSON object for a list of workout names.
    /// This class will Suppress StyleCops warning message SA1300 because Json objects
    /// used all lower-case for the naming of variables.
    /// </summary>
    [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter",
        Justification = "This class represents a JSON object. All variables should be all lower-case.")]
    public class JsonDistinctNames
    {
        #region Variable Fields

        /// <summary>
        /// Gets or sets the list of workout names
        /// </summary>
        public List<string> nameList { get; set; }

        #endregion Variable Fields

        #region Methods

        /// <summary>
        /// Overrides the ToString() method for the base class.
        /// </summary>
        /// <returns> A String containing the workout names in the List variable in this class. </returns>
        public override string ToString()
        {
            string s = "Workout Name List:\n";
            foreach (string str in this.nameList)
            {
                s += str + "\n";
            }

            return s;
        }

        #endregion Methods
    }
}
