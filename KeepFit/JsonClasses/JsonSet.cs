﻿//-----------------------------------------------------------------------
// <copyright file="JsonSet.cs" company="EvanDolan">
//     Copyright (c) Evan Dolan. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace KeepFit.JsonClasses
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents the structure of a JSON object for the <see cref="Set"/> class.
    /// This class will Suppress StyleCops warning message SA1300 because Json objects
    /// used all lower-case for the naming of variables.
    /// </summary>
    [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter",
        Justification = "This class represents a JSON object. All variables should be all lower-case.")]
    public class JsonSet
    {
        /// <summary>
        /// A class that represents the <see cref="Set"/> class in Json format.
        /// </summary>
        public class SetDetails
        {
            #region Variable Fields

            /// <summary>
            /// Gets or sets the weight used in the <see cref="Set"/> object.
            /// </summary>
            public double weight { get; set; }

            /// <summary>
            /// Gets or sets the repetitions performed in the <see cref="Set"/> object.
            /// </summary>
            public int repetitions { get; set; }

            /// <summary>
            /// Gets or sets the note, is any, in the <see cref="Set"/> object.
            /// </summary>
            public string note { get; set; }

            /// <summary>
            /// Gets or sets the timestamp, is any, in the <see cref="Set"/> object
            /// </summary>
            public string timestamp_string { get; set; }

            /// <summary>
            /// Gets or sets the ID for this <see cref="Workout"/> object.
            /// </summary>
            public string _id { get; set; }

            /// <summary>
            /// Gets or sets the version number for this <see cref="Workout"/> object.
            /// </summary>
            public int __v { get; set; }

            #endregion Variable Fields

            #region Methods

            /// <summary>
            /// Overrides the ToString() method for the base class.
            /// </summary>
            /// <returns> A String containing the data held in each variable in this class. </returns>
            public override string ToString()
            {
                string info = "Weight: " + this.weight;
                info += "\nRepetitions: " + this.repetitions;
                info += "\nNote: " + this.note;
                info += "\nID: " + this._id;
                return info + "\nVersion: " + this.__v;
            }

            /// <summary>
            /// Returns a short string about this <see cref="SetDetails"/> object.
            /// </summary>
            /// <returns> A Short string with the values for weight and repetitions. </returns>
            public string ToStringShort()
            {
                return this.weight + "\t" + this.repetitions;
            }

            /// <summary>
            /// Returns a long string about this <see cref="SetDetails"/> object.
            /// </summary>
            /// <returns> A long string with the values for weight, repetitions, and note is the
            /// value for note is not null, empty, or whitespace.. </returns>
            public string ToStringLong()
            {
                string longSet = "Weight: " + this.weight + "\tReps: " + this.repetitions;
                if (string.IsNullOrWhiteSpace(this.note))
                {
                    return longSet;
                }

                return longSet + "\n   Note: " + this.note;
            }

            #endregion Methods
        }

        /// <summary>
        /// The class contains a List of <see cref="SetDetails"/> objects.
        /// Each <see cref="SetDetails"/> object represents a <see cref="Set"/> object.
        /// </summary>
        public class RootObject
        {
            #region Variable Fields

            /// <summary>
            /// Gets or sets a List of <see cref="SetDetails"/> objects, representing <see cref="Set"/> objects.
            /// </summary>
            public List<SetDetails> sets { get; set; }

            #endregion Variable Fields

            #region Methods

            /// <summary>
            /// Overrides the ToString() method for the base class.
            /// </summary>
            /// <returns> A String containing the data held in each variable in this class. </returns>
            public override string ToString()
            {
                string info = string.Empty;
                foreach (SetDetails d in this.sets)
                {
                    info += d.ToString() + "\n";
                }

                return info;
            }

            /// <summary>
            /// Returns a short string about this <see cref="SetDetails"/> object.
            /// </summary>
            /// <returns> A Short string with the values for weight and repetitions. </returns>
            public string ToStringShort()
            {
                string info = "Weight\tReps\n";
                foreach (SetDetails sd in this.sets)
                {
                    info += sd.ToStringShort() + "\n";
                }

                return info;
            }

            #endregion Methods
        }
    }
}
