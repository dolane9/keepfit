﻿//-----------------------------------------------------------------------
// <copyright file="APIErrorCodes.cs" company="EvanDolan">
//     Copyright (c) Evan Dolan. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace KeepFit.KeeptFitAPI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    class APIErrorCodes
    {
        #region Variable Fields
        
        /// <summary>
        /// The ErrorCode used when the server can not be reached and is presumed to be offline.
        /// </summary>
        public static int ServerOffline
        {
            get
            {
                return -1;
            }

            private set { }
        }

        /// <summary>
        /// The ErrorCode for a successfully request.
        /// </summary>
        public static int Success
        {
            get
            {
                return 1;
            }

            private set { }
        }

        /// <summary>
        /// The ErrorCode for a failed request.
        /// The reason for failure is on the Client (App) side.
        /// </summary>
        public static int Failure
        {
            get
            {
                return 2;
            }

            private set { }
        }
        
        /// <summary>
        /// The ErrorCode received when a username already exists.
        /// </summary>
        public static int UsernameAlreadyExists
        {
            get
            {
                return 101;
            }

            private set { }
        }

        /// <summary>
        /// The ErrorCode received when an incorrect username is used.
        /// </summary>
        public static int IncorrectUsername
        {
            get
            {
                return 102;
            }

            private set { }
        }

        /// <summary>
        /// The ErrorCode received when an incorrect password is used.
        /// </summary>
        public static int IncorrectPassword
        {
            get
            {
                return 103;
            }

            private set { }
        }

        /// <summary>
        /// The ErrorCode received when an email already exists.
        /// </summary>
        public static int EmailAlreadyExists
        {
            get
            {
                return 104;
            }

            private set { }
        }

        /// <summary>
        /// The ErrorCode received when an incorrect password is used.
        /// </summary>
        public static int SetNotFound
        {
            get
            {
                return 112;
            }

            private set { }
        }

        /// <summary>
        /// The ErrorCode received when a Workout already exists.
        /// </summary>
        public static int WorkoutAlreadyExists
        {
            get
            {
                return 201;
            }

            private set { }
        }

        /// <summary>
        /// The ErrorCode received when a Workout could not be found.
        /// </summary>
        public static int WorkoutNotFound
        {
            get
            {
                return 202;
            }

            private set { }
        }
        
        /// <summary>
        /// The ErrorCode received when a WorkoutProgram already exists.
        /// </summary>
        public static int ProgramAlreadyExists
        {
            get
            {
                return 301;
            }

            private set { }
        }

        /// <summary>
        /// The ErrorCode received when a WorkoutProgram could not be found.
        /// </summary>
        public static int ProgramNotFound
        {
            get
            {
                return 302;
            }

            private set { }
        }

        /// <summary>
        /// The ErrorCode received when a Validation error occurs.
        /// Normally occurs when a parameter is not included, or an incorrect
        /// parameter is included, in a POST request.
        /// </summary>
        public static int ValidationError
        {
            get
            {
                return 401;
            }

            private set { }
        }

        /// <summary>
        /// The ErrorCode received when a "Not Found" error occurs.
        /// Normally, the url that is being requested could not be found.
        /// </summary>
        public static int NotFound
        {
            get
            {
                return 404;
            }

            private set { }
        }

        /// <summary>
        /// The ErrorCode received when a "Internal Server" error occurs.
        /// </summary>
        public static int InternalServerError
        {
            get
            {
                return 500;
            }

            private set { }
        }

        #endregion Variables Fields
    }
}
