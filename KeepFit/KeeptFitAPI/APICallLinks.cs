﻿//-----------------------------------------------------------------------
// <copyright file="APICall.cs" company="EvanDolan">
//     Copyright (c) Evan Dolan. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace KeepFit.KeeptFitAPI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// This class contains the links for each API call.
    /// </summary>
    class APICallLinks
    {
        #region Variables Fields

        /// <summary>
        /// The port number the server is hosted on.
        /// </summary>
        public static int Port
        {
            get
            {
                return 1401;
            }

            private set {}
        }

        /// <summary>
        /// The address of the host of the server.
        /// </summary>
        public static string Host
        {
            get
            {
                return "http://macker57.com";
            }

            private set { }
        }

        /// <summary>
        /// The address of the host and the port that is the server is running on.
        /// </summary>
        public static string FullHost
        {
            get
            {
                return Host + ":" + Port;
            }

            private set {}
        }

        /// <summary>
        /// The URL to query to register a user.
        /// </summary>
        public static string Register
        {
            get
            {
                return FullHost + "/register";
            }

            private set { }
        }

        /// <summary>
        /// The URL to query to log in a user.
        /// </summary>
        public static string Login
        {
            get
            {
                return FullHost + "/login";
            }

            private set { }
        }

        /// <summary>
        /// The URL to query to get a users full name.
        /// </summary>
        public static string UsersFullName
        {
            get
            {
                return FullHost + "/usersfullname";
            }

            private set { }
        }
        
        /// <summary>
        /// The URL to query to POST or GET to the '/set' url.
        /// </summary>
        public static string Set
        {
            get
            {
                return FullHost + "/set";
            }

            private set { }
        }

        /// <summary>
        /// The URL to query to POST or GET Sets from the database.
        /// </summary>
        public static string Sets
        {
            get
            {
                return FullHost + "/sets";
            }

            private set { }
        }

        /// <summary>
        /// The URL to query to POST or GET a Workout from the database.
        /// </summary>
        public static string Workout
        {
            get
            {
                return FullHost + "/workout";
            }

            private set { }
        }

        /// <summary>
        /// The URL to query to POST or GET Workouts from the database.
        /// </summary>
        public static string Workouts
        {
            get
            {
                return FullHost + "/workouts";
            }

            private set { }
        }

        /// <summary>
        /// The URL to query to POST or GET a Workout Skeleton from the database.
        /// </summary>
        public static string WorkoutSkeleton
        {
            get
            {
                return FullHost + "/workout_skeleton";
            }

            private set { }
        }

        /// <summary>
        /// The URL to query to POST or GET Workouts (that have a num_sets value higher than 0)
        /// from the database.
        /// </summary>
        public static string WorkoutsThatContainSets
        {
            get
            {
                return FullHost + "/workouts_contains_sets";
            }

            private set { }
        }

        /// <summary>
        /// The URL to query to POST or GET Workouts by TimeStamp from the database.
        /// </summary>
        public static string WorkoutsByTimeStamp
        {
            get
            {
                return FullHost + "/workouts_ts";
            }

            private set { }
        }

        /// <summary>
        /// The URL to query to POST to create a New Workout in the database.
        /// </summary>
        public static string NewWorkout
        {
            get
            {
                return FullHost + "/new_workout";
            }

            private set { }
        }

        /// <summary>
        /// The URL to query to GET the distinct Workout names from the database.
        /// </summary>
        public static string DistinctWorkouts
        {
            get
            {
                return FullHost + "/distinct_workouts";
            }

            private set { }
        }

        /// <summary>
        /// The URL to query to GET the distinct WorkoutProgram names from the database.
        /// </summary>
        public static string DistinctWorkoutPrograms
        {
            get
            {
                return FullHost + "/distinctworkoutprograms";
            }

            private set { }
        }

        /// <summary>
        /// The URL to query to POST or GET one Program from the database.
        /// </summary>
        public static string Program
        {
            get
            {
                return FullHost + "/program";
            }

            private set { }
        }

        /// <summary>
        /// The URL to query to POST or GET Programs from the database.
        /// </summary>
        public static string Programs
        {
            get
            {
                return FullHost + "/programs";
            }

            private set { }
        }

        /// <summary>
        /// The URL to query to POST or GET a Program Skeleton from the database.
        /// </summary>
        public static string ProgramSkeleton
        {
            get
            {
                return FullHost + "/program_skeleton";
            }

            private set { }
        }

        /// <summary>
        /// The URL to query to POST to create a new Program in the database.
        /// </summary>
        public static string NewProgram
        {
            get
            {
                return FullHost + "/new_program";
            }

            private set { }
        }

        /// <summary>
        /// The URL to query to GET the distinct Program names from the database.
        /// </summary>
        public static string DistinctPrograms
        {
            get
            {
                return FullHost + "/distinct_programs";
            }

            private set { }
        }

        #endregion Variables Fields
    }
}
