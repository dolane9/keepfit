﻿//-----------------------------------------------------------------------
// <copyright file="APICaller.cs" company="EvanDolan">
//     Copyright (c) Evan Dolan. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace KeepFit.KeeptFitAPI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading.Tasks;

    using Newtonsoft.Json;
    using JsonClasses;

    /// <summary>
    /// This class contains methods that can be used to make calls to the KeeptFit API.
    /// Each method will return either the a JSON object of a List of JSON objects.
    /// </summary>
    class APICaller
    {
        #region Variable Fields

        /// <summary>
        /// The authentication for the user making the API call.
        /// </summary>
        private string auth;

        /// <summary>
        /// The users username
        /// </summary>
        private string username;

        /// <summary>
        /// The users first and second name
        /// </summary>
        private string fullname;

        #endregion Variable Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="APICaller"/> class.
        /// </summary>
        /// <param name="auth"> A String in base64 that represents the authentication
        /// needed to make an API call.. </param>
        /// <param name="username"> The username of the user. </param>
        public APICaller(string auth, string username)
        {
            this.Auth = auth;
            this.Username = username;
        }

        #endregion Constructors

        #region Variable Properties

        /// <summary>
        /// Gets or sets the authentication for this <see cref="APICaller"/> object.
        /// </summary>
        public string Auth
        {
            get { return this.auth; }
            private set { this.auth = value; }
        }

        /// <summary>
        /// Gets or sets the username for this <see cref="APICaller"/> object.
        /// </summary>
        public string Username
        {
            get { return this.username; }
            private set { this.username = value; }
        }

        /// <summary>
        /// Gets or sets the fullname for this <see cref="APICaller"/> object.
        /// </summary>
        public string Fullname
        {
            get { return this.fullname; }
            set { this.fullname = value; }
        }

        #endregion Variable Properties

        #region Methods

        /// <summary>
        /// This method will attempt to add a user to the database. The username and password for the user
        /// will be supplied in the auth variable in this class. The rest of the information on the user
        /// will be the parameters supplied when this method is called.
        /// </summary>
        /// <param name="email"> The users email. </param>
        /// <param name="first"> The users first name.</param>
        /// <param name="last"> The users last name. </param>
        /// <returns> A JsonReply object indiciating if the call to the server was a success
        /// or not. The JsonReply object will include an error code is there was an error. </returns>
        public JsonReply RegisterUser(string email, string first, string last)
        {
            // Create a HttpClient to POST and set it to read JSON
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + auth);

            // Convert the URL we are going to POST to to a URI
            Uri apiCallUri = new Uri(APICallLinks.Register);

            // Put the data we are going to POST into a List of KayValuePair objects
            var registeringUserInfo = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("email", email),
                new KeyValuePair<string, string>("first", first),
                new KeyValuePair<string, string>("last", last)
            };

            // Convert the registering users information in to an HttpContent object
            HttpContent userInfoHttpContent = new FormUrlEncodedContent(registeringUserInfo);

            try
            {
                // POST the users information to the URI we created
                var response = httpClient.PostAsync(apiCallUri, userInfoHttpContent).Result;

                // Read the result
                string resultContent = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<JsonReply>(resultContent);
            }
            catch (Exception e)
            {
                e.ToString(); // To prevent warning of un-used variable e
                JsonReply jr = new JsonReply();
                jr.errorCode = APIErrorCodes.ServerOffline;
                jr.status = "Server Offline.";
                return jr;
            }
        }

        /// <summary>
        /// This method will make a GET request to the database, sending along a users log in credentials.
        /// It will attempt to log the user in to the system.
        /// </summary>
        /// <returns> A JsonReply object indicating the success status of the login attempt. </returns>
        public JsonReply Login()
        {
            // Create a HttpClient to GET and set it to read JSON
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + this.auth);

            // Convert the URL we are going to GET to a URI
            Uri apiCallUri = new Uri(APICallLinks.Login);

            try
            {
                // Make GET to log the users in from the URI created
                var response = httpClient.GetAsync(apiCallUri).Result;

                // Read the result
                string resultContent = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<JsonReply>(resultContent);
            }
            catch (Exception e)
            {
                e.ToString(); // To prevent warning of un-used variable e
                JsonReply jr = new JsonReply();
                jr.errorCode = APIErrorCodes.ServerOffline;
                jr.status = "Server Offline.";
                return jr;
            }
        }

        /// <summary>
        /// This method will make a GET request to the database with the users log in credentials.
        /// It will attempt to get the users full name.
        /// </summary>
        /// <returns> A JsonReply object with a success status and the users full name if successful. </returns>
        public JsonReply GetUsersFullName()
        {
            // Create a HttpClient to GET and set it to read JSON
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + auth);

            // Convert the URL we are going to POST to to a URI
            Uri apiCallUri = new Uri(APICallLinks.UsersFullName);

            try
            {
                // GET the users full name from the URI created
                var response = httpClient.GetAsync(apiCallUri).Result;

                // Read the result
                string resultContent = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<JsonReply>(resultContent);
            }
            catch (Exception e)
            {
                e.ToString(); // To prevent warning of un-used variable e
                JsonReply jr = new JsonReply();
                jr.errorCode = APIErrorCodes.ServerOffline;
                jr.status = "Server Offline.";
                return jr;
            }
        }

        /// <summary>
        /// This method will attempt to get all of the Set objects from the database that
        /// have the timestamp_string matched by the param supplied.
        /// </summary>
        /// <param name="timestamp_string"> The timestamp, as a string, that each Set object returned
        /// should contain. </param>
        /// <param name="workoutName"> The name of the Workout (Exercise) that the set belongs to. </param>
        /// <returns> A List of Set objects that have a matching timestamp if successful.
        /// Otherwise, null. </returns>
        public List<JsonSet.SetDetails> GetSetsByTimestamp(string timestamp_string, string workoutName)
        {
            // Create a HttpClient to GET and set it to read JSON
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + auth);

            // Convert the URL we are going to GET from a URI
            Uri apiCallUri = new Uri(APICallLinks.Sets + "/\"" + workoutName + timestamp_string + "\"");

            // Check for OK status code
            try
            {
                // GET the users workout from the URI created
                var response = httpClient.GetAsync(apiCallUri).Result;

                // Read the result of a successful GET and print it on screen
                string resultContent = response.Content.ReadAsStringAsync().Result;
                resultContent = "{ \"sets\": " + resultContent + " }";

                var rootObject = JsonConvert.DeserializeObject<JsonSet.RootObject>(resultContent);
                List<JsonSet.SetDetails> sets = rootObject.sets;

                return sets;
            }
            catch (Exception e)
            {
                // Exception caught
                e.ToString();
                return null;
            }
        }

        /// <summary>
        /// This method will attempt to add a list of sets to the database. Each set will have
        /// the same timestamp value.
        /// </summary>
        /// <param name="sets"> A List containing the sets to be added to the database. </param>
        /// <param name="timestamp"> The timestamp that each set will have. </param>
        /// <param name="workoutName"> The name of the workout (exercise) the set belongs to. </param>
        /// <returns> A JsonReply object indicating the success status of the API calls. </returns>
        public JsonReply PostSets(List<Set> sets, string timestamp, string workoutName)
        {
            // Check if the List supplied is null
            if (sets == null)
            {
                return new JsonReply("Failure: The TextBox highlighted in red contains invalid data.", 2);
            }

            // Check if the timestamp supplied is null
            if (timestamp == null)
            {
                return new JsonReply("Failure: The timestamp object supplied is null.", 2);
            }

            // Temporary JsonReply object
            JsonReply jr;

            foreach(Set s in sets)
            {
                // Check if a Set object is null. If null, return a failure
                if (s == null)
                {
                    return new JsonReply("Failure: The Set object at index [" + sets.IndexOf(s) + "] is null.",2);
                }

                // Check if the Set.Weight and Set.Repetitons object is the default value. If null, return a failure
                if (s.Weight < 0 || s.Repetitions < 0)
                {
                    return new JsonReply("Failure: The TextBox highlighted in red contains invalid data.", 2);
                }

                // Check if the Set.Note object is null. If null, replace with empty string
                if (s.Note == null)
                {
                    s.Note = string.Empty;
                }

                // Try add each set to the database
                jr = this.PostSet(s.Weight, s.Repetitions, s.Note, workoutName+timestamp);
                
                // Check if the set was added successfully
                // If not, return the JsonReply response from the server
                if (jr.errorCode != APIErrorCodes.Success)
                {
                    return jr;
                }
            }

            // All sets added successfully, return a successful JsonReply object
            return new JsonReply("All sets added", APIErrorCodes.Success);
        }

        /// <summary>
        /// This method will attempt to add a new Set to the database for the currently
        /// logged in user.
        /// </summary>
        /// <param name="weight"> The weight in the Set. </param>
        /// <param name="reps"> The repetitions in the Set. </param>
        /// <param name="note"> The note for the Set. </param>
        /// <param name="timestamp"> A timestamp as a string for the Set. </param>
        /// <returns> A JsonReply object indicating a success status of the API call. </returns>
        public JsonReply PostSet(double weight, int reps, string note, string timestamp)
        {
            // Create a HttpClient to POST and set it to read JSON
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + auth);

            // Convert the URL we are going to POST to to a URI
            Uri apiCallUri = new Uri(APICallLinks.Sets);

            // Put the data we are going to POST into a List of KayValuePair objects
            var newWorkoutInfo = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("weight", weight.ToString()),
                new KeyValuePair<string, string>("repetitions", reps.ToString()),
                new KeyValuePair<string, string>("note", note),
                new KeyValuePair<string, string>("timestamp_string", timestamp)
            };

            // Convert the registering users information in to an HttpContent object
            HttpContent userInfoHttpContent = new FormUrlEncodedContent(newWorkoutInfo);

            try
            {
                // POST the users information to the URI we created
                var response = httpClient.PostAsync(apiCallUri, userInfoHttpContent).Result;

                // Read the result
                string resultContent = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<JsonReply>(resultContent);
            }
            catch (Exception e)
            {
                // Server is offline/unreachable
                e.ToString(); // To prevent warning of un-used variable e
                JsonReply jr = new JsonReply();
                jr.errorCode = APIErrorCodes.ServerOffline;
                jr.status = "Server Offline.";
                return jr;
            }
        }

        /// <summary>
        /// This method will call attempt to delete the Set in the database that matches the
        /// id parameter.
        /// </summary>
        /// <param name="id"> The _id of the Set to delete. </param>
        /// <returns> A JsonReply indicating the success status of deleting the Set from the database. </returns>
        public JsonReply DeleteSet(string id)
        {
            // Create a HttpClient to DELETE and set it to read JSON
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + auth);

            // Convert the URL we are going to POST to to a URI
            Uri apiCallUri = new Uri(APICallLinks.Set + "/" + id);

            try
            {
                // Make a DELETE request to the URI we created
                var response = httpClient.DeleteAsync(apiCallUri).Result;

                // Read the result
                string resultContent = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<JsonReply>(resultContent);
            }
            catch (Exception e)
            {
                e.ToString(); // To prevent warning of un-used variable e
                JsonReply jr = new JsonReply();
                jr.errorCode = APIErrorCodes.ServerOffline;
                jr.status = "Server Offline.";
                return jr;
            }
        }

        /// <summary>
        /// This method will attempt to get a Workout in the database that contain the name supplied
        /// as a parameter and belong to the user logged in. The Workout will not contain a List of
        /// Set objects. It will only contain information about the Workout.
        /// </summary>
        /// <param name="workoutName"> The name of the Workout to be returned. </param>
        /// <returns> A <see cref="JsonWorkout.WorkoutDetails"/> object representing the Workout returned.
        /// Returns null if the Workout could not be found or the call to the API fails. </returns>
        public JsonWorkout.WorkoutDetails GetWorkout(string workoutName)
        {
            // Create a HttpClient to GET and set it to read JSON
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + auth);

            // Convert the URL we are going to GET from a URI
            Uri apiCallUri = new Uri(APICallLinks.Workout + "/" + workoutName);

            // Check for OK status code
            try
            {
                // GET the users workout from the URI created
                var response = httpClient.GetAsync(apiCallUri).Result;

                // Read the result of a successful GET and print it on screen
                string resultContent = response.Content.ReadAsStringAsync().Result;
                resultContent = "{ \"workouts\": " + resultContent + " }";

                var rootObject = JsonConvert.DeserializeObject<JsonWorkout.RootObject>(resultContent);
                List<JsonWorkout.WorkoutDetails> workoutList = rootObject.workouts;
                
                // If the List is empty, then throw an exception (workout not found)
                if(workoutList.Count < 1)
                {
                    throw new Exception();
                }

                // The List is not empty, get the first element. There should only be one element.
                JsonWorkout.WorkoutDetails workout = workoutList[0];
                
                // Make sure the length of the workout name is greater than the length of the
                // users id before trying to remove the users id from the workout name string
                if (workout.name.Length > workout.user_id.Length)
                {
                    workout.name = workout.name.Substring(workout.user_id.Length);
                }
                
                return workout;
            }
            catch (Exception e)
            {
                // Exception caught
                e.ToString();
                return null;
            }
        }

        /// <summary>
        /// This method will attempt to get a Workout Skeleton from the database that contains the name supplied
        /// as a parameter and belong to the user logged in. A Workout Skeleton is the layout of a particular
        /// Workout. A Workout Skeleton will never contain a List of Set objects. It will only contain
        /// information about the Workout.
        /// </summary>
        /// <param name="workoutName"> The name of the Workout to be returned. </param>
        /// <returns> A <see cref="JsonWorkout.WorkoutDetails"/> object representing the Workout returned.
        /// Returns null if the Workout could not be found or the call to the API fails. </returns>
        public JsonWorkout.WorkoutDetails GetWorkoutSkeleton(string workoutName)
        {
            // Create a HttpClient to GET and set it to read JSON
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + auth);

            // Convert the URL we are going to GET from a URI
            Uri apiCallUri = new Uri(APICallLinks.WorkoutSkeleton + "/" + workoutName);

            // Check for OK status code
            try
            {
                // GET the users workout from the URI created
                var response = httpClient.GetAsync(apiCallUri).Result;

                // Read the result of a successful GET and print it on screen
                string resultContent = response.Content.ReadAsStringAsync().Result;
                resultContent = "{ \"workouts\": " + resultContent + " }";

                var rootObject = JsonConvert.DeserializeObject<JsonWorkout.RootObject>(resultContent);
                List<JsonWorkout.WorkoutDetails> workoutList = rootObject.workouts;

                // If the List is empty, then throw an exception (workout not found)
                if (workoutList.Count < 1)
                {
                    throw new Exception();
                }

                // The List is not empty, get the first element. There should only be one element.
                JsonWorkout.WorkoutDetails workout = workoutList[0];

                // Make sure the length of the workout name is greater than the length of the
                // users id before trying to remove the users id from the workout name string
                if (workout.name.Length > workout.user_id.Length)
                {
                    workout.name = workout.name.Substring(workout.user_id.Length);
                }

                return workout;
            }
            catch (Exception e)
            {
                // Exception caught
                e.ToString();
                return null;
            }
        }

        /// <summary>
        /// This method will attempt to get the workouts in the database that contain the name supplied
        /// as a parameter and belong to the user logged in.
        /// This method will not return a Workout that does not contain any Sets.
        /// </summary>
        /// <param name="workoutName"> The name of the workouts to be returned. </param>
        /// <returns> A list of <see cref="JsonWorkout.WorkoutDetails"/> objects representing the workouts returned.
        /// Returns an empty list if no workouts could be found. Returns null if the call to the API fails. </returns>
        public List<JsonWorkout.WorkoutDetails> GetWorkouts(string workoutName)
        {
            // Create a HttpClient to GET and set it to read JSON
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + auth);

            // Convert the URL we are going to GET from a URI
            Uri apiCallUri = new Uri(APICallLinks.Workouts + "/" + workoutName);

            // Check for OK status code
            try
            {
                // GET the users workout from the URI created
                var response = httpClient.GetAsync(apiCallUri).Result;

                // Read the result of a successful GET and print it on screen
                string resultContent = response.Content.ReadAsStringAsync().Result;

                // Check if the Workouts could not be found
                if (resultContent.Equals("[]"))
                {
                    return new List<JsonWorkout.WorkoutDetails>();
                }

                // Add the array name to the result
                resultContent = "{ \"workouts\": " + resultContent + " }";

                var rootObject = JsonConvert.DeserializeObject<JsonWorkout.RootObject>(resultContent);
                List<JsonWorkout.WorkoutDetails> workouts = rootObject.workouts;

                // Each workout's name has the users id attached to it at the head of the string
                // Remove this extra information
                for (int i = 0; i < workouts.Count; i++)
                {
                    // Make sure the length of the workout name is great than the length of the
                    // users id before trying to remove the users id from the workout name string
                    if (workouts[i].name.Length > workouts[i].user_id.Length)
                    {
                        workouts[i].name = workouts[i].name.Substring(workouts[i].user_id.Length);
                    }

                    // Try get the List of Sets for the Workout
                    List<JsonSet.SetDetails> setList = this.GetSetsByTimestamp(workouts[i].timestamp, workouts[i].name);
                    if (setList != null && setList.Count > 0)
                    {
                        workouts[i].sets = setList;
                    }
                    else
                    {
                        // No Sets could be found for this Workout so remove it from the List
                        workouts.Remove(workouts[i]);
                        // Index goes back one because we just removed the element at the current index
                        i--;
                    }
                }
                
                return workouts;
            }
            catch (Exception e)
            {
                // Exception caught
                e.ToString();
                return null;
            }
        }

        /// <summary>
        /// This method will attempt to get the workouts in the database that contain the name supplied
        /// as a parameter and belong to the user logged in.
        /// This method will not return a Workout that does not contain any Sets.
        /// </summary>
        /// <param name="workoutName"> The name of the workouts to be returned. </param>
        /// <returns> A list of <see cref="JsonWorkout.WorkoutDetails"/> objects representing the workouts returned.
        /// Returns an empty list if no workouts could be found. Returns null if the call to the API fails. </returns>
        public List<JsonWorkout.WorkoutDetails> GetWorkoutsWithoutSets(string workoutName)
        {
            // Create a HttpClient to GET and set it to read JSON
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + auth);

            // Convert the URL we are going to GET from a URI
            Uri apiCallUri = new Uri(APICallLinks.WorkoutsThatContainSets + "/" + workoutName);

            // Check for OK status code
            try
            {
                // GET the users workout from the URI created
                var response = httpClient.GetAsync(apiCallUri).Result;

                // Read the result of a successful GET and print it on screen
                string resultContent = response.Content.ReadAsStringAsync().Result;

                // Check if the Workouts could not be found
                if (resultContent.Equals("[]"))
                {
                    return new List<JsonWorkout.WorkoutDetails>();
                }

                // Add the array name to the result
                resultContent = "{ \"workouts\": " + resultContent + " }";

                var rootObject = JsonConvert.DeserializeObject<JsonWorkout.RootObject>(resultContent);
                List<JsonWorkout.WorkoutDetails> workouts = rootObject.workouts;

                // Each workout's name has the users id attached to it at the head of the string
                // Remove this extra information
                for (int i = 0; i < workouts.Count; i++)
                {
                    // Make sure the length of the workout name is great than the length of the
                    // users id before trying to remove the users id from the workout name string
                    if (workouts[i].name.Length > workouts[i].user_id.Length)
                    {
                        workouts[i].name = workouts[i].name.Substring(workouts[i].user_id.Length);
                    }
                }

                return workouts;
            }
            catch (Exception e)
            {
                // Exception caught
                e.ToString();
                return null;
            }
        }

        /// <summary>
        /// This method will attempt to get all of the Workout objects from the database that
        /// have the timestamp variable matched by the param supplied.
        /// </summary>
        /// <param name="timestamp_string"> The timestamp, as a string, that each Workout object
        /// returned will contain. </param>
        /// <returns> A List of Workout objects that have a matching timestamp if successful.
        /// Otherwise, null. </returns>
        public List<JsonWorkout.WorkoutDetails> GetWorkoutsByTimestamp(string timestamp_string)
        {
            // Create a HttpClient to GET and set it to read JSON
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + auth);

            // Convert the URL we are going to GET from a URI
            Uri apiCallUri = new Uri(APICallLinks.WorkoutsByTimeStamp + "/\"" + timestamp_string + "\"");

            // Check for OK status code
            try
            {
                // GET the users workout from the URI created
                var response = httpClient.GetAsync(apiCallUri).Result;

                // Read the result of a successful GET and print it on screen
                string resultContent = response.Content.ReadAsStringAsync().Result;

                // Check if the Workouts could not be found
                if (resultContent.Equals("[]"))
                {
                    return new List<JsonWorkout.WorkoutDetails>();
                }

                // Add the array name to the result
                resultContent = "{ \"workouts\": " + resultContent + " }";

                var rootObject = JsonConvert.DeserializeObject<JsonWorkout.RootObject>(resultContent);
                List<JsonWorkout.WorkoutDetails> workouts = rootObject.workouts;

                // Each workout's name has the users id attached to it at the head of the string
                // Remove this extra information
                for (int i = 0; i < workouts.Count; i++)
                {
                    // Make sure the length of the workout name is great than the length of the
                    // users id before trying to remove the users id from the workout name string
                    if (workouts[i].name.Length > workouts[i].user_id.Length)
                    {
                        workouts[i].name = workouts[i].name.Substring(workouts[i].user_id.Length);
                    }

                    // Try get the List of Sets for the Workout
                    List<JsonSet.SetDetails> setList = this.GetSetsByTimestamp(workouts[i].timestamp, workouts[i].name);
                    if (setList != null && setList.Count > 0)
                    {
                            workouts[i].sets = setList;

                    }
                    else
                    {
                        // No Sets could be found for this Workout so remove it from the List
                        workouts.Remove(workouts[i]);
                        // Index goes back one because we just removed the element at the current index
                        i--;
                    }
                }

                return workouts;
            }
            catch (Exception e)
            {
                // Exception caught
                e.ToString();
                return null;
            }
        }

        /// <summary>
        /// This method will attempt to get all of the Workout objects from the database that
        /// have the timestamp variable matched by the param supplied. The Workouts will not
        /// contain a List fo Sets.
        /// </summary>
        /// <param name="timestamp_string"> The timestamp, as a string, that each Workout object
        /// returned will contain. </param>
        /// <returns> A List of Workout objects that have a matching timestamp if successful.
        /// Otherwise, null. </returns>
        public List<JsonWorkout.WorkoutDetails> GetWorkoutsByTimestampWithoutSets(string timestamp_string)
        {
            // Create a HttpClient to GET and set it to read JSON
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + auth);

            // Convert the URL we are going to GET from a URI
            Uri apiCallUri = new Uri(APICallLinks.WorkoutsByTimeStamp + "/\"" + timestamp_string + "\"");

            // Check for OK status code
            try
            {
                // GET the users workout from the URI created
                var response = httpClient.GetAsync(apiCallUri).Result;

                // Read the result of a successful GET and print it on screen
                string resultContent = response.Content.ReadAsStringAsync().Result;

                // Check if the Workouts could not be found
                if (resultContent.Equals("[]"))
                {
                    return new List<JsonWorkout.WorkoutDetails>();
                }

                // Add the array name to the result
                resultContent = "{ \"workouts\": " + resultContent + " }";

                var rootObject = JsonConvert.DeserializeObject<JsonWorkout.RootObject>(resultContent);
                List<JsonWorkout.WorkoutDetails> workouts = rootObject.workouts;

                // Each workout's name has the users id attached to it at the head of the string
                // Remove this extra information
                for (int i = 0; i < workouts.Count; i++)
                {
                    // Make sure the length of the workout name is great than the length of the
                    // users id before trying to remove the users id from the workout name string
                    if (workouts[i].name.Length > workouts[i].user_id.Length)
                    {
                        workouts[i].name = workouts[i].name.Substring(workouts[i].user_id.Length);
                    }
                }

                return workouts;
            }
            catch (Exception e)
            {
                // Exception caught
                e.ToString();
                return null;
            }
        }

        /// <summary>
        /// This method will attempt to GET a List of all the distinct names for each Workout
        /// for the currently logged in user.
        /// </summary>
        /// <returns> A List of string objects. Each string represents the name of a Workout
        /// that belongs to the user who made the AIP call. </returns>
        public List<string> GetDistinctWorkoutNames()
        {
            // Create a HttpClient to GET and set it to read JSON
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + auth);

            // Convert the URL we are going to GET from a URI
            Uri apiCallUri = new Uri(APICallLinks.DistinctWorkouts);

            // Check for OK status code
            try
            {
                // GET the users workout from the URI created
                var response = httpClient.GetAsync(apiCallUri).Result;

                // Read the result of a successful GET and print it on screen
                string resultContent = response.Content.ReadAsStringAsync().Result;
                resultContent = "{ \"nameList\": " + resultContent + " }";

                var rootObject = JsonConvert.DeserializeObject<JsonDistinctNames>(resultContent);
                List<string> workoutNames = rootObject.nameList;

                // Each workout's name has the users id attached to it at the head of the string
                // Remove this extra information
                for (int i = 0; i < workoutNames.Count; i++)
                {
                    // Make sure the length of the workout name is great than the length of the
                    // users id before trying to remove the users id from the workout name string
                    if (workoutNames[i].Length > this.Username.Length)
                    {
                        workoutNames[i] = workoutNames[i].Substring(this.Username.Length);
                    }
                }

                return workoutNames;
            }
            catch (Exception e)
            {
                // Exception caught
                e.ToString();
                List<string> s = new List<string>();
                s.Add("empty list");
                s.Add(e.ToString());
                s.Add(e.StackTrace);
                return s;
            }
        }

        /// <summary>
        /// This method calls the PostRecordWorkout(string, string, List-Set, int) method. It will pass
        /// the parameters supplied to that method as well as a -1 for the programIndex parameter. This is
        /// a default method used if a caller does not specifiy the Program index of this Workout (in this
        /// case, Workout is assumed to not be part of a Program).
        /// </summary>
        /// <param name="workoutName"> The name of the Workout to record. </param>
        /// <param name="note"> A note for the Workout to record. </param>
        /// <param name="sets"> A List of Sets that will belong to the recorded Workout. </param>
        /// <returns> A JsonReply indicating the success status of the API call to the database. </returns>
        public JsonReply PostRecordWorkout(string workoutName, string note, List<Set> sets)
        {
            // Try post the workout, return the JsonReply if it is not successful
            JsonReply jr;

            if (sets == null)
            {
                jr = this.PostRecordWorkout(workoutName, note, 0, -1);
            }
            else
            {
                jr = this.PostRecordWorkout(workoutName, note, sets.Count, -1);
            }

            if (jr.errorCode != APIErrorCodes.Success)
            {
                return jr;
            }

            // If the workout was posted successfully, the JsonReply.status returned will be a timestamp
            return this.PostSets(sets, jr.status, workoutName);
        }

        /// <summary>
        /// This method will attempt to POST a Workout object to the workouts database. The method will
        /// also attempt to post the List of Set objects (supplied as a parameter) to the sets database.
        /// When this method POSTs the Workout, it will recieve a time stamp for the server. This time
        /// stamp is is post along with each Set object so that the Workout object can reference the Sets.
        /// </summary>
        /// <param name="workoutName"> The name of the Workout to record. </param>
        /// <param name="note"> A note for the Workout to record. </param>
        /// <param name="sets"> A List of Sets that will belong to the recorded Workout. </param>
        /// <param name="programIndex"> The index number of the Workout in the Program. </param>
        /// <returns> A JsonReply indicating the success status of the API call to the database. </returns>
        public JsonReply PostRecordWorkout(string workoutName, string note, List<Set> sets, int programIndex)
        {
            return this.PostRecordWorkout(workoutName, note, sets, programIndex, null);
        }

        /// <summary>
        /// This method will attempt to POST a Workout object to the workouts database. The method will
        /// also attempt to post the List of Set objects (supplied as a parameter) to the sets database.
        /// When this method POSTs the Workout, it will recieve a time stamp for the server. This time
        /// stamp is is post along with each Set object so that the Workout object can reference the Sets.
        /// </summary>
        /// <param name="workoutName"> The name of the Workout to record. </param>
        /// <param name="note"> A note for the Workout to record. </param>
        /// <param name="sets"> A List of Sets that will belong to the recorded Workout. </param>
        /// <param name="programIndex"> The index number of the Workout in the Program. </param>
        /// <param name="timestamp"> The timestamp that will be used to submit the Workout. </param>
        /// <returns> A JsonReply indicating the success status of the API call to the database. </returns>
        public JsonReply PostRecordWorkout(string workoutName, string note, List<Set> sets, int programIndex, string timestamp)
        {
            // Try post the workout, return the JsonReply if it is not successful
            JsonReply jr;

            if (sets == null)
            {
                jr = this.PostRecordWorkout(workoutName, note, 0, programIndex);
            }
            else if (string.IsNullOrWhiteSpace(timestamp))
            {
                jr = this.PostRecordWorkout(workoutName, note, sets.Count, programIndex, null);
            }
            else
            {
                jr = this.PostRecordWorkout(workoutName, note, sets.Count, programIndex, timestamp);
            }

            if (jr.errorCode != APIErrorCodes.Success)
            {
                return jr;
            }

            // If the workout was posted successfully, the JsonReply.status returned will be a timestamp
            return this.PostSets(sets, jr.status, workoutName);
        }

        /// <summary>
        /// This method will attempt to POST a Workout object to the database based on the parameter values
        /// supplied. This method does not specify a timestamp so the current time will be used.
        /// </summary>
        /// <param name="workoutName"> The name of the Workout to POST. </param>
        /// <param name="note"> A note for the Workout to POST. </param>
        /// <param name="numberOfSets"> The number of Sets that belong to this Workout. </param>
        /// <param name="programIndex"> The index of this Workout in a Program (if it belongs to one). </param>
        /// <returns> A JsonReply indicating the success status of the API call to the database. </returns>
        public JsonReply PostRecordWorkout(string workoutName, string note, int numberOfSets, int programIndex)
        {
            return this.PostRecordWorkout(workoutName, note, numberOfSets, programIndex, null);
        }

        /// <summary>
        /// This method will attempt to POST a Workout object to the workout database based on the
        /// parameter values supplied.
        /// </summary>
        /// <param name="workoutName"> The name of the Workout to POST. </param>
        /// <param name="note"> A note for the Workout to POST. </param>
        /// <param name="numberOfSets"> The number of Sets that belong to this Workout. </param>
        /// <param name="programIndex"> The index of this Workout in a Program (if it belongs to one). </param>
        /// <param name="timestamp"> The timestamp that will be used to submit the Workout. </param>
        /// <returns> A JsonReply indicating the success status of the API call to the database. </returns>
        public JsonReply PostRecordWorkout(string workoutName, string note, int numberOfSets, int programIndex, string timestamp)
        {
            // Create a HttpClient to POST and set it to read JSON
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + auth);

            // Convert the URL we are going to POST to to a URI
            Uri apiCallUri = new Uri(APICallLinks.Workouts);

            // Put the data we are going to POST into a List of KayValuePair objects
            var newWorkoutInfo = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("name", workoutName),
                new KeyValuePair<string, string>("is_target", "false"),
                new KeyValuePair<string, string>("num_sets", numberOfSets.ToString()),
                new KeyValuePair<string, string>("program_index", programIndex.ToString())
            };

            // If the note supplied is not null or empty then add it to the List of KeyValuePairs
            if (!string.IsNullOrWhiteSpace(note))
            {
                newWorkoutInfo.Add(new KeyValuePair<string, string>("note", note));
            }

            // If the timestamp supplied is not null or empty add it to the List of KeyValuePairs
            if (!string.IsNullOrWhiteSpace(timestamp))
            {
                newWorkoutInfo.Add(new KeyValuePair<string, string>("timestamp", "\"" + timestamp + "\""));
            }

            // Convert the registering users information in to an HttpContent object
            HttpContent userInfoHttpContent = new FormUrlEncodedContent(newWorkoutInfo);

            try
            {
                // POST the users information to the URI we created
                var response = httpClient.PostAsync(apiCallUri, userInfoHttpContent).Result;

                // Read the result
                string resultContent = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<JsonReply>(resultContent);
            }
            catch (Exception e)
            {
                e.ToString(); // To prevent warning of un-used variable e
                JsonReply jr = new JsonReply();
                jr.errorCode = APIErrorCodes.ServerOffline;
                jr.status = "Server Offline.";
                return jr;
            }
        }

        /// <summary>
        /// This method will call the PostWorkout(string, string, bool, int, string) method and set the
        /// bool parameter to false, the program_index value to -1 and the empty string to the timestamp.
        /// </summary>
        /// <param name="name"> The name of the new Workout. </param>
        /// <param name="note"> An optional note that can be added to the new Workout. </param>
        /// <returns> A JsonReply indicating the success status of adding the new Workout to the database. </returns>
        public JsonReply PostNewWorkout(string name, string note)
        {
            return this.PostNewWorkout(name, note, false, -1, string.Empty);
        }

        /// <summary>
        /// This method will call the PostWorkout(string, string, bool, int, string) method and the
        /// program_index value to -1 and the empty string to the timestamp.
        /// </summary>
        /// <param name="name"> The name of the new Workout. </param>
        /// <param name="note"> An optional note that can be added to the new Workout. </param>
        /// <param name="is_target"> A bool indicating if the workout is a target or not. </param>
        /// <returns> A JsonReply indicating the success status of adding the new Workout to the database. </returns>
        public JsonReply PostNewWorkout(string name, string note, bool is_target)
        {
            return this.PostNewWorkout(name, note, is_target, -1, string.Empty);
        }

        /// <summary>
        /// This method will call the PostWorkout(string, string, bool, int, string) method and the empty string
        /// to the timestamp.
        /// </summary>
        /// <param name="name"> The name of the new Workout. </param>
        /// <param name="note"> An optional note that can be added to the new Workout. </param>
        /// <param name="is_target"> A bool indicating if the workout is a target or not. </param>
        /// <param name="program_index"> The position of the Workout in a Program. </param>
        /// <returns> A JsonReply indicating the success status of adding the new Workout to the database. </returns>
        public JsonReply PostNewWorkout(string name, string note, bool is_target, int program_index)
        {
            return this.PostNewWorkout(name, note, is_target, program_index, string.Empty);
        }

        /// <summary>
        /// This method will attempt to add a new Workout to the database for the currently
        /// logged in user.
        /// </summary>
        /// <param name="name"> The name of the new Workout. </param>
        /// <param name="note"> An optional note that can be added to the new Workout. </param>
        /// <param name="is_target"> A bool indicating if the workout is a target or not. </param>
        /// <param name="program_index"> The position of the Workout in a Program. </param>
        /// <param name="timestamp"> A string representing the timestamp to give to this object. </param>
        /// <returns> A JsonReply indicating the success status of adding the new Workout to the database. </returns>
        public JsonReply PostNewWorkout(string name, string note, bool is_target, int program_index, string timestamp)
        {
            // Create a HttpClient to POST and set it to read JSON
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + auth);

            // Convert the URL we are going to POST to to a URI
            Uri apiCallUri = new Uri(APICallLinks.NewWorkout);

            // Check if the bool parameter is true or false
            string is_target_str = "false";
            if (is_target.Equals(true))
            {
                is_target_str = "true";
            }

            // Put the data we are going to POST into a List of KayValuePair objects
            // Zero is passed to num-sets because creating a New Workout can never have any sets
            var newWorkoutInfo = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("name", name),
                new KeyValuePair<string, string>("is_target", is_target_str),
                new KeyValuePair<string, string>("note", note),
                new KeyValuePair<string, string>("num_sets", "0"),
                new KeyValuePair<string, string>("program_index", program_index.ToString())
            };

            // If the timestamp supplied is not null/empty/whitespace, send it to the server
            if (!string.IsNullOrWhiteSpace(timestamp))
            {
                newWorkoutInfo.Add(new KeyValuePair<string, string>("timestamp", timestamp));
            }

            // Convert the registering users information in to an HttpContent object
            HttpContent userInfoHttpContent = new FormUrlEncodedContent(newWorkoutInfo);

            try
            {
                // POST the users information to the URI we created
                var response = httpClient.PostAsync(apiCallUri, userInfoHttpContent).Result;

                // Read the result
                string resultContent = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<JsonReply>(resultContent);
            }
            catch (Exception e)
            {
                e.ToString(); // To prevent warning of un-used variable e
                JsonReply jr = new JsonReply();
                jr.errorCode = APIErrorCodes.ServerOffline;
                jr.status = "Server Offline.";
                return jr;
            }
        }

        /// <summary>
        /// This method will attempt to add a Workout to the database for the currently
        /// logged in user. The Workout name can already exist.
        /// </summary>
        /// <param name="name"> The name of the new Workout. </param>
        /// <param name="note"> An optional note that can be added to the new Workout. </param>
        /// <param name="is_target"> A bool indicating if the workout is a target or not. </param>
        /// <param name="program_index"> The position of the Workout in a Program. </param>
        /// <param name="timestamp"> A string representing the timestamp to give to this object. </param>
        /// <returns> A JsonReply indicating the success status of adding the new Workout to the database. </returns>
        public JsonReply PostWorkout(string name, string note, bool is_target, int program_index, string timestamp)
        {
            // Create a HttpClient to POST and set it to read JSON
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + auth);

            // Convert the URL we are going to POST to to a URI
            Uri apiCallUri = new Uri(APICallLinks.Workouts);

            // Check if the bool parameter is true or false
            string is_target_str = "false";
            if (is_target.Equals(true))
            {
                is_target_str = "true";
            }

            // Put the data we are going to POST into a List of KayValuePair objects
            // Zero is passed to num-sets because creating a New Workout can never have any sets
            var newWorkoutInfo = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("name", name),
                new KeyValuePair<string, string>("is_target", is_target_str),
                new KeyValuePair<string, string>("note", note),
                new KeyValuePair<string, string>("num_sets", "0"),
                new KeyValuePair<string, string>("program_index", program_index.ToString())
            };

            // If the timestamp supplied is not null/empty/whitespace, send it to the server
            if (!string.IsNullOrWhiteSpace(timestamp))
            {
                // Add quotations to timestamp so server can read it correctly
                newWorkoutInfo.Add(new KeyValuePair<string, string>("timestamp", "\"" + timestamp + "\""));
            }

            // Convert the registering users information in to an HttpContent object
            HttpContent userInfoHttpContent = new FormUrlEncodedContent(newWorkoutInfo);

            try
            {
                // POST the users information to the URI we created
                var response = httpClient.PostAsync(apiCallUri, userInfoHttpContent).Result;

                // Read the result
                string resultContent = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<JsonReply>(resultContent);
            }
            catch (Exception e)
            {
                e.ToString(); // To prevent warning of un-used variable e
                JsonReply jr = new JsonReply();
                jr.errorCode = APIErrorCodes.ServerOffline;
                jr.status = "Server Offline.";
                return jr;
            }
        }

        /// <summary>
        /// This method will call attempt to delete the Workout in the database that matches the
        /// id parameter.
        /// </summary>
        /// <param name="id"> The _id of the Workout to delete. </param>
        /// <returns> A JsonReply indicating the success status of deleting the Workout from the database. </returns>
        public JsonReply DeleteWorkout(string id)
        {
            // Create a HttpClient to DELETE and set it to read JSON
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + auth);

            // Convert the URL we are going to POST to to a URI
            Uri apiCallUri = new Uri(APICallLinks.Workout + "/" + id);

            try
            {
                // Make a DELETE request to the URI we created
                var response = httpClient.DeleteAsync(apiCallUri).Result;

                // Read the result
                string resultContent = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<JsonReply>(resultContent);
            }
            catch (Exception e)
            {
                e.ToString(); // To prevent warning of un-used variable e
                JsonReply jr = new JsonReply();
                jr.errorCode = APIErrorCodes.ServerOffline;
                jr.status = "Server Offline.";
                return jr;
            }
        }

        /// <summary>
        /// This method will call attempt to delete the Workout Skeleton in the database that matches the
        /// id parameter.
        /// </summary>
        /// <param name="name"> The name of the Workout Skeleton to delete. </param>
        /// <returns> A JsonReply indicating the success status of deleting the Workout Skelton from the database. </returns>
        public JsonReply DeleteWorkoutSkeleton(string name)
        {
            // Try get the Workout Skeleton object
            JsonWorkout.WorkoutDetails workoutSkeleton = this.GetWorkoutSkeleton(name);

            JsonReply jr = new JsonReply();

            // Check if the Workout Skeleton could be found
            if (workoutSkeleton == null || workoutSkeleton._id == null)
            {
                jr.status = "Workout could not be found.";
                jr.errorCode = APIErrorCodes.WorkoutNotFound;
            }
            else
            {
                // Try delete the Workout Skeleton and return the response
                jr = this.DeleteWorkout(workoutSkeleton._id);
            }

            return jr;
        }

        /// <summary>
        /// This method will attempt to get a Program in the database that contain the name supplied as
        /// a parameter and belong to the user logged in. The Program will contain a List of Workout
        /// objects. If this Program does not contain any Workouts, the Workout List will be null.
        /// </summary>
        /// <param name="programName"> The name of the Workout to be returned. </param>
        /// <returns> A <see cref="JsonWorkoutProgram.WorkoutProgramDetails"/> object representing the Program
        ///  returned. Returns null if the Program could not be found or the call to the API fails. </returns>
        public JsonWorkoutProgram.WorkoutProgramDetails GetProgram(string programName)
        {
            // Create a HttpClient to GET and set it to read JSON
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + auth);

            // Convert the URL we are going to GET from a URI
            Uri apiCallUri = new Uri(APICallLinks.Program + "/" + programName);

            // Check for OK status code
            try
            {
                // GET the users workout from the URI created
                var response = httpClient.GetAsync(apiCallUri).Result;

                // Read the result of a successful GET and print it on screen
                string resultContent = response.Content.ReadAsStringAsync().Result;
                resultContent = "{ \"workouts\": " + resultContent + " }";

                var rootObject = JsonConvert.DeserializeObject<JsonWorkoutProgram.RootObject>(resultContent);
                List<JsonWorkoutProgram.WorkoutProgramDetails> programList = rootObject.workoutprograms;

                // If the List is empty, then throw an exception (workout not found)
                if (programList.Count < 1)
                {
                    throw new Exception();
                }

                // The List is not empty, get the first element. There should only be one element.
                JsonWorkoutProgram.WorkoutProgramDetails program = programList[0];


                // Make sure the length of the Program name is greater than the length of the
                // users_id before trying to remove the users_id from the Program name string
                if (program.name.Length > program.user_id.Length)
                {
                    program.name = program.name.Substring(program.user_id.Length);
                }

                return program;
            }
            catch (Exception e)
            {
                // Exception caught
                e.ToString();
                return null;
            }
        }

        /// <summary>
        /// This method will attempt to get a Program Skeleton from the database that contains the name supplied
        /// as a parameter and belong to the user logged in. A Program Skeleton is the layout of a particular
        /// Program. The Program Skeleton will not contain a List of Workout objects. It will only contain
        /// information about the Program.
        /// </summary>
        /// <param name="programName"> The name of the Program to be returned. </param>
        /// <returns> A <see cref="JsonWorkoutProgram.WorkoutProgramDetails"/> object representing the Program
        /// returned. Returns null if the Program could not be found or the call to the API fails. </returns>
        public JsonWorkoutProgram.WorkoutProgramDetails GetProgramSkeleton(string programName)
        {
            // Create a HttpClient to GET and set it to read JSON
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + auth);

            // Convert the URL we are going to GET from a URI
            Uri apiCallUri = new Uri(APICallLinks.ProgramSkeleton + "/" + programName);

            // Check for OK status code
            try
            {
                // GET the users Program from the URI created
                var response = httpClient.GetAsync(apiCallUri).Result;

                // Read the result of a successful GET and print it on screen
                string resultContent = response.Content.ReadAsStringAsync().Result;
                resultContent = "{ \"workoutprograms\": " + resultContent + " }";

                var rootObject = JsonConvert.DeserializeObject<JsonWorkoutProgram.RootObject>(resultContent);
                List<JsonWorkoutProgram.WorkoutProgramDetails> programList = rootObject.workoutprograms;

                // If the List is empty, then throw an exception (workout not found)
                if (programList.Count < 1)
                {
                    throw new Exception();
                }

                // The List is not empty, get the first element. There should only be one element.
                JsonWorkoutProgram.WorkoutProgramDetails program = programList[0];

                // Make sure the length of the Program name is greater than the length of the
                // users id before trying to remove the users id from the Program name string
                if (program.name.Length > program.user_id.Length)
                {
                    program.name = program.name.Substring(program.user_id.Length);
                }

                return program;
            }
            catch (Exception e)
            {
                // Exception caught
                e.ToString();
                return null;
            }
        }

        /// <summary>
        /// This method will get the Program Skeleton for the name supplied as a parameter. It will
        /// include a List of the Workouts owned by this Program.
        /// </summary>
        /// <param name="programName"> The name of the Program to be returned. </param>
        /// <returns> A <see cref="JsonWorkoutProgram.WorkoutProgramDetails"/> object representing the Program
        /// returned. Returns null if the Program could not be found or the call to the API fails. </returns>
        public JsonWorkoutProgram.WorkoutProgramDetails GetProgramSkeletonWithWorkouts(string programName)
        {
            JsonWorkoutProgram.WorkoutProgramDetails program = this.GetProgramSkeleton(programName);
            
            // Get the programs list of Workouts
            program.workouts = this.GetWorkoutsByTimestampWithoutSets(program.timestamp);

            return program;
        }

        /// <summary>
        /// This method will attempt to get a Program in the database that contain the name supplied as
        /// a parameter and belong to the user logged in. The Program will contain a List of Workout
        /// objects. If this Program does not contain any Workouts, the Workout List will be null.
        /// </summary>
        /// <param name="program"> The JsonWorkoutProgram.WorkoutProgramDetails object without a List
        /// of Workouts. </param>
        /// <returns> A <see cref="JsonWorkoutProgram.WorkoutProgramDetails"/> object containing a List of
        /// Workouts. Returns null if the Program could not be found or the call to the API fails. </returns>
        public JsonWorkoutProgram.WorkoutProgramDetails GetProgramWorkouts(JsonWorkoutProgram.WorkoutProgramDetails program)
        {
            // Create a HttpClient to GET and set it to read JSON
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + auth);

            // Check for OK status code
            try
            {
                program.workouts = this.GetWorkoutsByTimestamp(program.timestamp);
                return program;
            }
            catch (Exception e)
            {
                // Exception caught
                e.ToString();
                return null;
            }
        }

        /// <summary>
        /// This method will attempt to get a Program in the database that contain the name supplied
        /// as a parameter and belong to the user logged in. The Program will not contain a List of
        /// Workout objects.
        /// </summary>
        /// <param name="programName"> The name of the Workout to be returned. </param>
        /// <returns> A <see cref="JsonWorkoutProgram.WorkoutProgramDetails"/> object representing the Program
        ///  returned. Returns null if the Program could not be found or the call to the API fails. </returns>
        public JsonWorkoutProgram.WorkoutProgramDetails GetProgramWithoutWorkouts(string programName)
        {
            // Create a HttpClient to GET and set it to read JSON
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + auth);

            // Convert the URL we are going to GET from a URI
            Uri apiCallUri = new Uri(APICallLinks.Program + "/" + programName);

            // Check for OK status code
            try
            {
                // GET the users workout from the URI created
                var response = httpClient.GetAsync(apiCallUri).Result;

                // Read the result of a successful GET and print it on screen
                string resultContent = response.Content.ReadAsStringAsync().Result;
                resultContent = "{ \"workouts\": " + resultContent + " }";

                var rootObject = JsonConvert.DeserializeObject<JsonWorkoutProgram.RootObject>(resultContent);
                List<JsonWorkoutProgram.WorkoutProgramDetails> programList = rootObject.workoutprograms;

                // If the List is empty, then throw an exception (workout not found)
                if (programList.Count < 1)
                {
                    throw new Exception();
                }

                // The List is not empty, get the first element. There should only be one element.
                JsonWorkoutProgram.WorkoutProgramDetails program = programList[0];


                // Make sure the length of the Program name is greater than the length of the
                // users_id before trying to remove the users_id from the Program name string
                if (program.name.Length > program.user_id.Length)
                {
                    program.name = program.name.Substring(program.user_id.Length);
                }

                return program;
            }
            catch (Exception e)
            {
                // Exception caught
                e.ToString();
                return null;
            }
        }

        /// <summary>
        /// This method will attempt to get the Programs in the database that contain the name supplied
        /// as a parameter and belong to the user logged in. Programs will be returned with the List
        /// of Workouts.
        /// </summary>
        /// <param name="programName"> The name of the Workout Programs to be returned. </param>
        /// <returns> A list of <see cref="JsonWorkoutProgram.WorkoutProgramDetails"/> objects representing the
        /// Workout Programs returned. Returns an empty list if no workouts could be found.
        /// Returns null if the call to the API fails. </returns>
        public List<JsonWorkoutProgram.WorkoutProgramDetails> GetPrograms(string programName)
        {
            // Create a HttpClient to GET and set it to read JSON
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + this.auth);
            
            // Convert the URL we are going to GET from a URI
            Uri apiCallUri = new Uri(APICallLinks.Programs + "/" + programName);

            // Check for OK status code
            try
            {
                // GET the users workout from the URI created
                var response = httpClient.GetAsync(apiCallUri).Result;

                // Read the result of a successful GET and print it on screen
                string resultContent = response.Content.ReadAsStringAsync().Result;

                // Check if the Workout Programs could not be found
                if(resultContent.Equals("[]"))
                {
                    return new List<JsonWorkoutProgram.WorkoutProgramDetails>();
                }

                // Add the array name to the result
                resultContent = "{ \"workoutprograms\": " + resultContent + " }";

                var rootObject = JsonConvert.DeserializeObject<JsonWorkoutProgram.RootObject>(resultContent);
                List<JsonWorkoutProgram.WorkoutProgramDetails> programs = rootObject.workoutprograms;

                // Each Program's name has the users_id attached to it at the head of the string
                // Remove this extra information
                for (int i = 0; i < programs.Count; i++)
                {
                    // Try get the List of Workouts for the Program
                    List<JsonWorkout.WorkoutDetails> workoutList = this.GetWorkoutsByTimestamp(programs[i].timestamp);
                    if (workoutList != null && workoutList.Count > 0)
                    {
                        programs[i].workouts = workoutList;

                        // Make sure the length of the Program name is great than the length of the
                        // users_id before trying to remove the users_id from the Program name string
                        if (programs[i].name.Length > programs[i].user_id.Length)
                        {
                            programs[i].name = programs[i].name.Substring(programs[i].user_id.Length);
                        }
                    }
                    else
                    {
                        // No Workouts could be found for this Program so remove it from the List
                        programs.Remove(programs[i]);

                        // Index goes back one because we just removed the element at the current index
                        i--;
                    }
                }

                return programs;
            }
            catch (Exception e)
            {
                // Exception caught
                e.ToString();
                return null;
            }
        }

        /// <summary>
        /// This method will attempt to get the Programs in the database that contain the name supplied
        /// as a parameter and belong to the user logged in. Programs will be returned with the List
        /// of Workouts.
        /// </summary>
        /// <param name="programName"> The name of the Workout Programs to be returned. </param>
        /// <returns> A list of <see cref="JsonWorkoutProgram.WorkoutProgramDetails"/> objects representing the
        /// Workout Programs returned. Returns an empty list if no workouts could be found.
        /// Returns null if the call to the API fails. </returns>
        public List<JsonWorkoutProgram.WorkoutProgramDetails> GetProgramsWithoutWorkouts(string programName)
        {
            // Create a HttpClient to GET and set it to read JSON
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + this.auth);

            // Convert the URL we are going to GET from a URI
            Uri apiCallUri = new Uri(APICallLinks.Programs + "/" + programName);

            // Check for OK status code
            try
            {
                // GET the users workout from the URI created
                var response = httpClient.GetAsync(apiCallUri).Result;

                // Read the result of a successful GET and print it on screen
                string resultContent = response.Content.ReadAsStringAsync().Result;

                // Check if the Workout Programs could not be found
                if (resultContent.Equals("[]"))
                {
                    return new List<JsonWorkoutProgram.WorkoutProgramDetails>();
                }

                // Add the array name to the result
                resultContent = "{ \"workoutprograms\": " + resultContent + " }";

                var rootObject = JsonConvert.DeserializeObject<JsonWorkoutProgram.RootObject>(resultContent);
                List<JsonWorkoutProgram.WorkoutProgramDetails> programs = rootObject.workoutprograms;

                // Each Program's name has the users_id attached to it at the head of the string
                // Remove this extra information
                for (int i = 0; i < programs.Count; i++)
                {
                    // Make sure the length of the Program name is great than the length of the
                    // users_id before trying to remove the users_id from the Program name string
                    if (programs[i].name.Length > programs[i].user_id.Length)
                    {
                        programs[i].name = programs[i].name.Substring(programs[i].user_id.Length);
                    }
                }

                return programs;
            }
            catch (Exception e)
            {
                // Exception caught
                e.ToString();
                return null;
            }
        }

        /// <summary>
        /// This method will attempt to GET a List of all the distinct names for each Workout
        /// Program for the currently logged in user.
        /// </summary>
        /// /// <returns> A List of string objects. Each string represents the name of a Workout
        /// Program that belongs to the user who made the AIP call. </returns>
        public List<string> GetDistinctProgramNames()
        {
            // Create a HttpClient to GET and set it to read JSON
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + auth);

            // Convert the URL we are going to GET from a URI
            Uri apiCallUri = new Uri(APICallLinks.DistinctPrograms);

            // Check for OK status code
            try
            {
                // GET the users workout from the URI created
                var response = httpClient.GetAsync(apiCallUri).Result;

                // Read the result of a successful GET and print it on screen
                string resultContent = response.Content.ReadAsStringAsync().Result;
                resultContent = "{ \"nameList\": " + resultContent + " }";

                var rootObject = JsonConvert.DeserializeObject<JsonDistinctNames>(resultContent);
                List<string> programNames = rootObject.nameList;

                // Each workout's name has the users id attached to it at the head of the string
                // Remove this extra information
                for (int i = 0; i < programNames.Count; i++)
                {
                    // Make sure the length of the workout name is great than the length of the
                    // users id before trying to remove the users id from the workout name string
                    if (programNames[i].Length > this.Username.Length)
                    {
                        programNames[i] = programNames[i].Substring(this.Username.Length);
                    }
                }

                return programNames;
            }
            catch (Exception e)
            {
                // Exception caught
                e.ToString();
                List<string> s = new List<string>();
                s.Add("empty list");
                s.Add(e.ToString());
                s.Add(e.StackTrace);
                return s;
            }
        }
        
        /// <summary>
        /// This method will call the PostNewProgram(string, string, bool) method and set the bool
        /// parameter to false.
        /// </summary>
        /// <param name="name"> The name of the new Program. </param>
        /// <param name="note"> An optional note that can be added to the new Program. </param>
        /// <returns> A JsonReply indicating the success status of adding the new Program to the database. </returns>
        public JsonReply PostNewProgram(string name, string note)
        {
            return this.PostNewProgram(name, note, false);
        }

        /// <summary>
        /// This method will attempt to add a new Program to the database for the currently
        /// logged in user.
        /// </summary>
        /// <param name="name"> The name of the new Program. </param>
        /// <param name="note"> An optional note that can be added to the new Program. </param>
        /// <param name="contains_workouts"> A bool indicating if the Program contains Workouts or not. </param>
        /// <returns> A JsonReply indicating the success status of adding the new Program to the database. </returns>
        public JsonReply PostNewProgram(string name, string note, bool contains_workouts)
        {
            // Create a HttpClient to POST and set it to read JSON
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + auth);

            // Convert the URL we are going to POST to to a URI
            Uri apiCallUri = new Uri(APICallLinks.NewProgram);

            // Check if the bool parameter is true or false
            string contains_workouts_str = "false";
            if (contains_workouts==true)
            {
                contains_workouts_str = "true";
            }

            // Put the data we are going to POST into a List of KayValuePair objects
            var newWorkoutInfo = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("name", name),
                new KeyValuePair<string, string>("contains_workouts", contains_workouts_str),
                new KeyValuePair<string, string>("note", note)
            };

            // Convert the registering users information in to an HttpContent object
            HttpContent userInfoHttpContent = new FormUrlEncodedContent(newWorkoutInfo);

            try
            {
                // POST the users information to the URI we created
                var response = httpClient.PostAsync(apiCallUri, userInfoHttpContent).Result;

                // Read the result
                string resultContent = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<JsonReply>(resultContent);
            }
            catch (Exception e)
            {
                e.ToString(); // To prevent warning of un-used variable e
                JsonReply jr = new JsonReply();
                jr.errorCode = APIErrorCodes.ServerOffline;
                jr.status = "Server Offline.";
                return jr;
            }
        }

        /// <summary>
        /// This method will call the PostNewProgram(string, string, List-JsonWorkout.WorkoutDetails, bool) method
        /// and set the bool parameter to false.
        /// </summary>
        /// <param name="name"> The name of the new Program. </param>
        /// <param name="note"> An optional note that can be added to the new Program. </param>
        /// <param name="workouts"> A List of the Workouts to be added </param>
        /// <returns> A JsonReply indicating the success status of adding the new Program to the database. </returns>
        public JsonReply PostNewProgram(string name, string note, List<string> workouts)
        {
            return this.PostNewProgram(name, note, workouts, false);
        }

        /// <summary>
        /// This method will attempt to POST a new Program as well as try to POST one Workout
        /// for each element in the List parameter.
        /// </summary>
        /// <param name="name"> The name of the new Program. </param>
        /// <param name="note"> An optional note that can be added to the new Program. </param>
        /// <param name="workouts"> A List of the Workouts to be added </param>
        /// <param name="contains_workouts"> A bool indicating if the Program contains Workouts or not. </param>
        /// <returns> A JsonReply indicating the success status of adding the new Program to the database. </returns>
        public JsonReply PostNewProgram(string name, string note, List<string> workouts, bool contains_workouts)
        {
            // Try post the Program, return the JsonReply if it is not successful
            JsonReply jr = this.PostNewProgram(name, note, false);
            if (jr.errorCode != APIErrorCodes.Success)
            {
                return jr;
            }

            // If the Program was posted successfully, the JsonReply.status returned will be a timestamp

            for (int i = 0; i < workouts.Count; i++)
            {
                JsonReply jrWorkout = this.PostWorkout(workouts[i], string.Empty, false, i, jr.status);

                // Check the JsonReply, return if it is not a success
                if (jrWorkout.errorCode != APIErrorCodes.Success)
                {
                    return jrWorkout;
                }
            }

            // Return a success JsonReply
            return jr;
        }

        /// <summary>
        /// This method will attempt to Record a Program in the database for the currently
        /// logged in user.
        /// </summary>
        /// <param name="name"> The name of the Recorded Program. </param>
        /// <param name="note"> An optional note that can be added to the new Program. </param>
        /// <param name="contains_workouts"> A bool indicating if the Program contains Workouts or not. </param>
        /// <returns> A JsonReply indicating the success status of adding the new Program to the database. </returns>
        public JsonReply PostRecordProgram(string name, string note, bool contains_workouts)
        {
            // Create a HttpClient to POST and set it to read JSON
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + auth);

            // Convert the URL we are going to POST to to a URI
            Uri apiCallUri = new Uri(APICallLinks.Programs);

            // Check if the bool parameter is true or false
            string contains_workouts_str = "false";
            if (contains_workouts == true)
            {
                contains_workouts_str = "true";
            }

            // Put the data we are going to POST into a List of KayValuePair objects
            var newWorkoutInfo = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("name", name),
                new KeyValuePair<string, string>("contains_workouts", contains_workouts_str),
                new KeyValuePair<string, string>("note", note)
            };

            // Convert the registering users information in to an HttpContent object
            HttpContent userInfoHttpContent = new FormUrlEncodedContent(newWorkoutInfo);

            try
            {
                // POST the users information to the URI we created
                var response = httpClient.PostAsync(apiCallUri, userInfoHttpContent).Result;

                // Read the result
                string resultContent = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<JsonReply>(resultContent);
            }
            catch (Exception e)
            {
                e.ToString(); // To prevent warning of un-used variable e
                JsonReply jr = new JsonReply();
                jr.errorCode = APIErrorCodes.ServerOffline;
                jr.status = "Server Offline.";
                return jr;
            }
        }

        /// <summary>
        /// This method will call attempt to delete the Program in the database that matches the
        /// id parameter.
        /// </summary>
        /// <param name="id"> The _id of the Program to delete. </param>
        /// <returns> A JsonReply indicating the success status of deleting the Program from the database. </returns>
        public JsonReply DeleteProgram(string id)
        {
            // Create a HttpClient to DELETE and set it to read JSON
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + auth);

            // Convert the URL we are going to POST to to a URI
            Uri apiCallUri = new Uri(APICallLinks.Program + "/" + id);

            try
            {
                // Make a DELETE request to the URI we created
                var response = httpClient.DeleteAsync(apiCallUri).Result;

                // Read the result
                string resultContent = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<JsonReply>(resultContent);
            }
            catch (Exception e)
            {
                e.ToString(); // To prevent warning of un-used variable e
                JsonReply jr = new JsonReply();
                jr.errorCode = APIErrorCodes.ServerOffline;
                jr.status = "Server Offline.";
                return jr;
            }
        }
        
        /// <summary>
        /// This method will call attempt to delete the Program Skeleton in the database that matches the
        /// id parameter.
        /// </summary>
        /// <param name="name"> The name of the Program Skeleton to delete. </param>
        /// <returns> A JsonReply indicating the success status of deleting the Program Skelton from the database. </returns>
        public JsonReply DeleteProgramSkeleton(string name)
        {
            // Try get the Program Skeleton object
            JsonWorkoutProgram.WorkoutProgramDetails programSkeleton = this.GetProgramSkeleton(name);

            JsonReply jr = new JsonReply();

            // Check if the Program Skeleton could be found
            if (programSkeleton == null || programSkeleton._id == null)
            {
                jr.status = "Program could not be found.";
                jr.errorCode = APIErrorCodes.ProgramNotFound;
            }
            else
            {
                // Try delete the Program Skeleton and return the response
                jr = this.DeleteProgram(programSkeleton._id);
            }

            return jr;
        }

        /// <summary>
        /// This method overrides the base classes implementation of ToString().
        /// </summary>
        /// <returns> A String that represents a <see cref="APICaller"/> object. </returns>
        public override string ToString()
        {
            return "This class contains methods to make calls to the API for a particular user.";
        }

        #endregion Methods
    }
}
