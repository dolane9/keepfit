﻿//-----------------------------------------------------------------------
// <copyright file="HomePage.xaml.cs" company="EvanDolan">
//     Copyright (c) Evan Dolan. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace KeepFit
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Windows.Foundation;
    using Windows.Foundation.Collections;
    using Windows.UI.Notifications;
    using Windows.UI.Popups;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;
    using Windows.UI.Xaml.Controls.Primitives;
    using Windows.UI.Xaml.Data;
    using Windows.UI.Xaml.Input;
    using Windows.UI.Xaml.Media;
    using Windows.UI.Xaml.Navigation;

    using System.Net.Http;
    using System.Net.Http.Headers;

    using JsonClasses;
    using KeeptFitAPI;

    using Newtonsoft.Json;

    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class HomePage : KeepFit.Common.LayoutAwarePage
    {
        /// <summary>
        /// An APICaller object is supplied when the App navigates to this page.
        /// </summary>
        APICaller ac;

        /// <summary>
        /// Initializes a new instance of the <see cref="HomePage"/> class.
        /// </summary>
        public HomePage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // An instance of an APICaller object should be passed when the App
            // navigates to this page.
            this.ac = e.Parameter as APICaller;

            // Get and display the users name on the top right of the screen
            this.tbUsersFullname.Text = string.Empty + this.GetUsersFullName();
            this.ac.Fullname = this.tbUsersFullname.Text;

            /* Testing Data */
            //JsonWorkoutProgram.WorkoutProgramDetails prog = this.ac.GetProgramSkeletonWithWorkouts("test5");
            //this.tbErrors.Text += "\n" + prog.workouts[0].ToStringLong();
            //this.tbErrors.Text += "\n" + prog.workouts[1].ToStringLong();
            //this.tbErrors.Text += "\n" + prog.workouts[2].ToStringLong();

            //JsonReply jr = this.ac.PostWorkout("testing-as.postworkout2", string.Empty, false, 0, "2014-05-30T15:20:38.871Z");
            //this.tbErrors.Text += "\n" + jr.ToString();
        }

        /// <summary>
        /// This method will attempt to get the full name of the users currently logged in.
        /// </summary>
        /// <returns> On success, a string containing the users full name. On fialure, a string
        /// containing an error message. </returns>
        private string GetUsersFullName()
        {
            // Check if the APICaller object is null
            if (this.ac.Equals(null))
            {
                return "Error: APICaller object is null.";
            }

            // Make call to API to get users full name and return the JsonReply
            JsonReply jr = ac.GetUsersFullName();
            return jr.status;
        }

        /// <summary>
        /// Invoked when the user clicks on the "btnBack" Button.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            // Store previous page string in a temp variable
            string previousPage = Windows.Storage.ApplicationData.Current.LocalSettings.Values["previous_page"] as string;
            // Set the previous page local variable
            Windows.Storage.ApplicationData.Current.LocalSettings.Values["previous_page"] = "home";

            // Make the user confirm that they want to log out
            this.ConfirmUserLogout();
        }

        /// <summary>
        /// This method is invoked then the "btnGoToWorkouts" button is clicked. This method will
        /// navigate the App to the Workout page. This method will pass an APICaller object
        /// which contains the logged in users details.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void btnGoToWorkouts_Click(object sender, RoutedEventArgs e)
        {
            // Set the previous page local variable
            Windows.Storage.ApplicationData.Current.LocalSettings.Values["previous_page"] = "home";
            
            this.Frame.Navigate(typeof(WorkoutPage), this.ac);
        }

        /// <summary>
        /// This method is invoked then the "btnGoToPrograms" button is clicked. This method will
        /// navigate the App to the WorkoutProgram page. This method will pass an APICaller object
        /// which contains the logged in users details.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void btnGoToPrograms_Click(object sender, RoutedEventArgs e)
        {
            // Set the previous page local variable
            Windows.Storage.ApplicationData.Current.LocalSettings.Values["previous_page"] = "home";

            this.Frame.Navigate(typeof(WorkoutProgramPage), this.ac);
        }

        /// <summary>
        /// This method is invoked then the "btnGoToGraphs" Button is clicked. This method will
        /// navigate the App to the Graph page. This method will pass an APICaller object
        /// which contains the logged in users details.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void btnGoToGraphs_Click(object sender, RoutedEventArgs e)
        {
            // Set the previous page local variable
            Windows.Storage.ApplicationData.Current.LocalSettings.Values["previous_page"] = "home";

            this.Frame.Navigate(typeof(GraphPage), this.ac);
        }

        /// <summary>
        /// This method is invoked when the "btnLogout" Button is clicked. This method will
        /// navigate the App to the Main page and log the user out. The "ac" APICaller
        /// object will be deleted.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            // Call the Logout method to gracefully log out.
            this.Logout(null);
        }

        /// <summary>
        /// This method will display a popup MessageDialog box on the page and ask the
        /// user to logout or cancel the logout. This method will log the user out if
        /// they confirm the lougout request.
        /// </summary>
        private async void ConfirmUserLogout()
        {
            MessageDialog m = new MessageDialog("Are you sure you want to logout?", "Logout");
            m.Commands.Add(new UICommand("Cancel", this.CancelLogout, 1));
            m.Commands.Add(new UICommand("Logout", this.Logout, 1));
            await m.ShowAsync();
        }

        /// <summary>
        /// This method will set the APICaller object to null nad navigate the App to the MainPaige.
        /// </summary>
        /// <param name="command"> This parameter is never used. </param>
        private void Logout(IUICommand command)
        {
            // Nullify the APICaller object while logging out.
            this.ac = null;

            // Store previous page string in a temp variable
            string previousPage = Windows.Storage.ApplicationData.Current.LocalSettings.Values["previous_page"] as string;
            // Set the previous page local variable
            Windows.Storage.ApplicationData.Current.LocalSettings.Values["previous_page"] = "home";

            this.Frame.Navigate(typeof(MainPage), "logout");
        }

        /// <summary>
        /// This method is called when the user cancels a loutout request. The previous_page variable
        /// stored in the LocalSettings will be changed back to "main";
        /// </summary>
        /// <param name="command"> This parameter is never used. </param>
        private void CancelLogout(IUICommand command)
        {
            Windows.Storage.ApplicationData.Current.LocalSettings.Values["previous_page"] = "main";
        }
    }
}
