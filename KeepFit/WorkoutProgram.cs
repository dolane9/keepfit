﻿//-----------------------------------------------------------------------
// <copyright file="WorkoutProgram.cs" company="EvanDolan">
//     Copyright (c) Evan Dolan. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace KeepFit
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// This class will act as a data structure that represents a Workout.
    /// </summary>
    public class WorkoutProgram
    {
        #region Variable Fields

        /// <summary>
        /// This is the name given to this WorkoutProgram by the user.
        /// </summary>
        private string name;

        /// <summary>
        /// This is a note added by the user about this WorkoutProgram.
        /// </summary>
        private string note;

        /////// <summary>
        /////// This is a list of all of the Sets performed in this WorkoutProgram.
        /////// </summary>
        ////private List<Workout> workouts;

        #endregion Variable Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkoutProgram"/> class.
        /// </summary>
        /// <param name="name"> The name of the WorkoutProgram. </param>
        public WorkoutProgram(string name)
        {
            this.Name = name;
            this.Note = string.Empty;
            this.Workouts = new List<Workout>();
        }

        #endregion Constructors

        #region Variable Properties

        /// <summary>
        /// Gets or sets the name of the WorkoutProgram.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        /// <summary>
        /// Gets or sets the note in the WorkoutProgram.
        /// </summary>
        public string Note
        {
            get { return this.note; }
            set { this.note = value; }
        }

        /// <summary>
        /// Gets the list of Workouts in this WorkoutProgram.
        /// </summary>
        public List<Workout> Workouts
        {
            get; // { return this.workouts; }
            private set;
        }

        #endregion Variable Properties

        #region Methods

        /// <summary>
        /// This method returns a short String that contains the data held in this object.
        /// </summary>
        /// <returns> A String that represents a WorkoutProgram object. </returns>
        public override string ToString()
        {
            string info = this.Name + "\n";

            foreach (Workout w in this.Workouts)
            {
                info += w.ToString() + "\n";
            }

            return info;
        }

        /// <summary>
        /// This method will add the Workout supplied to the list of Workouts.
        /// </summary>
        /// <param name="w"> The Workout object to be added to the list. </param>
        public void AddWorkout(Workout w)
        {
            this.Workouts.Add(w);
        }

        /// <summary>
        /// This method will return all of the data in this object as a String. 
        /// </summary>
        /// <returns> A String representing all data contained in a WorkoutProgram object. </returns>
        public string ToStringLong()
        {
            string setsLong = this.Name + "\n";

            // Check if there is an entry for Note
            if (this.Note.Length > 0)
            {
                setsLong += "Notes:" + this.Note + "\n";
            }

            foreach (Workout w in this.Workouts)
            {
                setsLong += "\t" + w.ToStringLong() + "\n";
            }

            return setsLong;
        }

        /// <summary>
        /// This method returns a String containing basic information on the Workouts in this object.
        /// </summary>
        /// <returns> A String containing the values of each Workouts' data. </returns>
        public string ToStringShort()
        {
            string setsShort = this.Name + "\n";

            foreach (Workout w in this.Workouts)
            {
                setsShort += w.ToStringShort() + "\n";
            }

            return setsShort;
        }

        #endregion Methods
    }
}
