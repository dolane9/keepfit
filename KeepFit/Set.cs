﻿//-----------------------------------------------------------------------
// <copyright file="Set.cs" company="EvanDolan">
//     Copyright (c) Evan Dolan. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace KeepFit
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// This class will act as a data structure that represents a Set.
    /// </summary>
    public class Set
    {
        #region Variable Fields

        /// <summary>
        /// The weight used in this Set.
        /// </summary>
        private double weight;

        /// <summary>
        /// The amount of repetitions completed in this Set.
        /// </summary>
        private int repetitions;

        /// <summary>
        /// The time this set object was recorded.
        /// </summary>
        private string timestamp;

        /// <summary>
        /// A note that can be added by the user for this Set.
        /// </summary>
        private string note;

        #endregion Variable Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Set"/> class.
        /// </summary>
        public Set()
        {
            // No args constructor. Initialize integers to a default value.
            this.Weight = -1;
            this.Repetitions = -1;
            this.Note = string.Empty;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Set"/> class.
        /// </summary>
        /// <param name="weight"> The weight used in the Set. </param>
        /// <param name="repetitions"> The amount of repetitions completed in the Set. </param>
        public Set(double weight, int repetitions)
        {
            this.Weight = weight;
            this.Repetitions = repetitions;
            this.Note = string.Empty;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Set"/> class.
        /// </summary>
        /// <param name="weight"> The weight used in the Set. </param>
        /// <param name="timestamp"> The timestamp for the Set. </param>
        public Set(double weight, string timestamp)
        {
            this.Weight = weight;
            this.Repetitions = -1;
            this.Timestamp = this.ParseTimestamp(timestamp);
            this.Note = string.Empty;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Set"/> class.
        /// </summary>
        /// <param name="weight"> The weight used in the Set. </param>
        /// <param name="repetitions"> The amount of repetitions completed in the Set. </param>
        /// <param name="timestamp"> The timestamp for the Set. </param>
        public Set(double weight, int repetitions, string timestamp)
        {
            this.Weight = weight;
            this.Repetitions = repetitions;
            this.Timestamp = this.ParseTimestamp(timestamp);
            this.Note = string.Empty;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Set"/> class.
        /// </summary>
        /// <param name="weight"> The weight used in the Set. </param>
        /// <param name="repetitions"> The amount of repetitions completed in the Set. </param>
        /// <param name="timestamp"> The timestamp for the Set. </param>
        /// <param name="note"> A note added by a user for this Set. </param>
        public Set(double weight, int repetitions, string timestamp, string note)
        {
            this.Weight = weight;
            this.Repetitions = repetitions;
            this.Timestamp = timestamp;
            this.Note = note;
        }

        #endregion Constructors

        #region Variable Properties

        /// <summary>
        /// Gets or sets the weight for the Set.
        /// </summary>
        public double Weight
        {
            get { return this.weight; }
            set { this.weight = value; }
        }

        /// <summary>
        /// Gets or sets the repetitions for the Set.
        /// </summary>
        public int Repetitions
        {
            get { return this.repetitions; }
            set { this.repetitions = value; }
        }

        /// <summary>
        /// Gets or sets a value for the timestamp.
        /// </summary>
        public string Timestamp
        {
            get { return this.timestamp; }
            set { this.timestamp = value; }
        }

        /// <summary>
        /// Gets or sets the note in the Workout.
        /// </summary>
        public string Note
        {
            get { return this.note; }
            set { this.note = value; }
        }
        
        #endregion Variable Properties

        #region Methods

        /// <summary>
        /// This method will take a string and try to get the timestamp from it.
        /// </summary>
        /// <param name="timestamp"> The string containing the timestamp. </param>
        /// <returns> A strign representing the timestamp. </returns>
        private string ParseTimestamp(string timestamp)
        {
            if (string.IsNullOrWhiteSpace(timestamp) || timestamp.Length <= 24)
            {
                return string.Empty;
            }

            return timestamp.Substring(timestamp.Length - 24, 10) + " " + timestamp.Substring(timestamp.Length - 13, 12);
        }

        /// <summary>
        /// Returns the data from the timestamp in this object. 
        /// </summary>
        /// <returns> The date for this Set. Empty string if none. </returns>
        public string GetDate()
        {
            if (!string.IsNullOrWhiteSpace(this.Timestamp) && this.Timestamp.Length >= 10)
            {
                return this.Timestamp.Substring(0, 10);
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// This method overrides the base classes implementation of ToString().
        /// </summary>
        /// <returns> A String that represents a <see cref="Set"/> object. </returns>
        public override string ToString()
        {
            return this.Weight + " " + this.Repetitions + " ";
        }

        /// <summary>
        /// This method will return all of the data in this object as a String. 
        /// </summary>
        /// <returns> A String representing all data contained in a <see cref="Set"/> object. </returns>
        public string ToStringLong()
        {
            // Check if there is an entry for Timestamp and/or Note
            if (this.Timestamp.Length > 0 && this.Note.Length > 0)
            {
                return "Weight: " + this.Weight + " \tRepetitions: " + this.Repetitions + "\nTimestamp: " + this.Timestamp + "\nNote: " + this.Note;
            }
            else if (this.Note.Length > 0)
            {
                return "Weight: " + this.Weight + " \tRepetitions: " + this.Repetitions + "\nNote: " + this.Note;
            }
            else
            {
                return "Weight: " + this.Weight + " \tRepetitions: " + this.Repetitions;
            }
        }

        /// <summary>
        /// This method returns this.ToString(). This purpose of this method is to maintain
        /// similarity with the Workout and WorkoutProgram classes which have a method with
        /// the same header.
        /// </summary>
        /// <returns> A String containing the weight and repetition amounts in this object. </returns>
        public string ToStringShort()
        {
            return this.ToString();
        }

        #endregion Methods
    }
}
