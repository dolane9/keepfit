﻿//-----------------------------------------------------------------------
// <copyright file="WorkoutProgramPage.xaml.cs" company="EvanDolan">
//     Copyright (c) Evan Dolan. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace KeepFit
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Windows.Foundation;
    using Windows.Foundation.Collections;
    using Windows.UI.ViewManagement;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;
    using Windows.UI.Xaml.Controls.Primitives;
    using Windows.UI.Xaml.Data;
    using Windows.UI.Xaml.Input;
    using Windows.UI.Xaml.Media;
    using Windows.UI.Xaml.Navigation;

    using JsonClasses;
    using KeeptFitAPI;

    /// <summary>
    /// A page that displays a group title, a list of items within the group, and details for
    /// the currently selected item.
    /// </summary>
    public sealed partial class WorkoutProgramPage : KeepFit.Common.LayoutAwarePage
    {
        /// <summary>
        /// An APICaller object is supplied when the App navigates to this page.
        /// </summary>
        APICaller ac;

        /// <summary>
        /// This int is used to keep track of which Workout is being recorded (for a Program).
        /// </summary>
        int recordProgramCounter;

        /// <summary>
        /// A List of the Workouts being currently recorded.
        /// </summary>
        List<JsonWorkout.WorkoutDetails> recordProgramWorkouts;

        /// <summary>
        /// This string represents the timestamp of the Program being recorded.
        /// </summary>
        string recordProgramTimestamp;

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkoutProgramPage"/> class.
        /// </summary>
        public WorkoutProgramPage()
        {
            this.InitializeComponent();
            this.recordProgramCounter = 0;
            this.recordProgramWorkouts = null;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // An instance of an APICaller object should be passed when the App
            // navigates to this page.
            this.ac = e.Parameter as APICaller;

            // Display users fullname on top right of screen
            this.tbUsersFullname.Text = this.ac.Fullname;

            // Update the ListView of Workouts.
            this.UpdateProgramListView();
        }

        /// <summary>
        /// Invoked when an item within the "lvPrograms" ListView is selected.
        /// </summary>
        /// <param name="sender">The GridView (or ListView when the application is Snapped)
        /// displaying the selected item.</param>
        /// <param name="e">Event data that describes how the selection was changed.</param>
        void lvPrograms_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // The ListView's default selection item is null. If the selected item is null
            // then return and don't update the "gvSelectedWorkouts" GridView which shows
            // information on the selected Workout
            if (this.lvPrograms.SelectedItem == null)
            {
                this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;
                this.tbCentreScreen.Text = "No Program selected.";
                return;
            }

            // Clean up the elements for the "NewProgram", "RecordProgram", "DeleteProgram", and "WorkoutInfo" areas
            this.CleanUpNewProgram();
            this.CleanUpRecordProgram();
            this.CleanUpProgramInfo();
            this.CleanUpDeleteProgram();            

            // Change the header to the name of the selected Program
            this.tbSelectedProgramHeader.Text = this.lvPrograms.SelectedItem.ToString();

            // Try to populate the GridView 
            try
            {
                // Get a list of the Programs in the database that have the same name as the 
                // Program selected in the ListView and set it to the ItemSource of the GridView.
                List<JsonWorkoutProgram.WorkoutProgramDetails> programDetails = ac.GetProgramsWithoutWorkouts(this.lvPrograms.SelectedItem.ToString());
                
                // Reverse the order of the List so that the newest workouts appear first
                programDetails.Reverse(0, programDetails.Count);

                // Show the GridView and hide the error TextBlock
                this.gvSelectedPrograms.Visibility = Windows.UI.Xaml.Visibility.Visible;
                this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

                // Set the "tbSelectedProgramHeader" TextBlock to be the name of the selected Program
                this.tbSelectedProgramHeader.Text = string.Empty + this.lvPrograms.SelectedItem.ToString(); ;
                
                // Check if the List returned is empty (user has not recorded any of these Programs)
                if (programDetails.Count == 0)
                {
                    // Hide the GridView and show the error TextBlock
                    this.gvSelectedPrograms.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    this.tbCentreScreen.Text = "You have not recorded anything for this Program";
                }
                else
                {
                    this.gvSelectedPrograms.ItemsSource = programDetails;
                }

            }
            catch (Exception ex)
            {
                // This is called to get read of the "Variable unused" warning
                ex.ToString();
                // If the list of workouts is null or empty, the user has not made any entries for this workout
                // An ArgumentOutOfRangeException, ArgumentNullException, or ArgumentException can be thrown here
                // Inform the user that there is no data about the workouts for the selected workout
                this.tbSelectedProgramHeader.Text = string.Empty;

                // Hide the GridView and show the error TextBlock
                this.gvSelectedPrograms.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;
                this.tbCentreScreen.Text = "You have not recorded anything for this Program";
            }

        }

        /// <summary>
        /// This method is invoked when the user selects an item from the "gvSelectedPrograms"
        /// GridView. This method will update the "gridProgramInfo" Grid which is located
        /// to the right of this GridView. It will set the visibility of the Grid to Visible
        /// and put some information in the UI elements inside of the Grid.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void gvSelectedPrograms_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Check if the selected item ahs changed to null
            if (this.gvSelectedPrograms.SelectedItem == null)
            {
                // Selected item is null, clean the page
                this.CleanUpProgramInfo();
                this.CleanUpRecordProgram();
                this.CleanUpNewProgram();

                // Informing the user will be handled by the "Selection Changed" event for the ListView
                return;
            }

            // Show the "WorkoutInfo" elements
            this.ShowProgramInfoElements();

            // Get the selected item as a JsonWorkout.WorkoutDetails object
            JsonWorkoutProgram.WorkoutProgramDetails selectedItem = this.gvSelectedPrograms.SelectedItem as JsonWorkoutProgram.WorkoutProgramDetails;

            // Set text of the subheader to the timestamp and number of sets for the selected item
            this.tbProgramInfoSubheader.Text = string.Empty + selectedItem.timestamp_formatted + "\t" + selectedItem.workouts_count;

            // Set the text of the Workouts details TextBox to the value for "workouts" for the selected Workout
            this.tbProgramInfoWorkoutsDetails.Text = string.Empty + this.ac.GetProgramWorkouts(selectedItem).WorkoutsToLongString();
            this.tbPageTitle.Text = selectedItem.name;
        }

        /// <summary>
        /// This method will attempt to update the "lvPrograms" ListView with a List of the users 
        /// Programs. This method makes a call to the API to GET a list of distinct Program names.
        /// </summary>
        /// <returns> True if the ListView was successfully updated. Otherwise, false. </returns>
        private bool UpdateProgramListView()
        {
            // Get the list of workouts for this user
            List<string> programNames = this.ac.GetDistinctProgramNames();
            
            try
            {
                // Set the List of workouts as the source for the ListView of Programs
                this.lvPrograms.ItemsSource = programNames;
                // Set the default select item to null
                this.lvPrograms.SelectedItem = null;

                // Check if there List fo workout names is empty. If it is, inform the user
                if (programNames.Count == 0)
                {
                    this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    this.tbCentreScreen.Text = "You haven't created any Programss yet.";
                }
                return true;
            }
            catch (NullReferenceException ex)
            {
                // Call ToString() to avoid 'variable not used' warning
                ex.ToString();
                // Failed to retrieve the List of workout names
                this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;
                this.tbCentreScreen.Text = "The List of Program names could not be retrieved. The server may be offline.";
                return false;
            }

        }

        #region Logical page navigation

        // Visual state management typically reflects the four application view states directly
        // (full screen landscape and portrait plus snapped and filled views.)  The split page is
        // designed so that the snapped and portrait view states each have two distinct sub-states:
        // either the item list or the details are displayed, but not both at the same time.
        //
        // This is all implemented with a single physical page that can represent two logical
        // pages.  The code below achieves this goal without making the user aware of the
        // distinction.

        /// <summary>
        /// Invoked to determine whether the page should act as one logical page or two.
        /// </summary>
        /// <param name="viewState">The view state for which the question is being posed, or null
        /// for the current view state.  This parameter is optional with null as the default
        /// value.</param>
        /// <returns>True when the view state in question is portrait or snapped, false
        /// otherwise.</returns>
        private bool UsingLogicalPageNavigation(ApplicationViewState? viewState = null)
        {
            if (viewState == null) viewState = ApplicationView.Value;
            return viewState == ApplicationViewState.FullScreenPortrait ||
                viewState == ApplicationViewState.Snapped;
        }

        /// <summary>
        /// Invoked when the page's back button is pressed.
        /// </summary>
        /// <param name="sender">The back button instance.</param>
        /// <param name="e">Event data that describes how the back button was clicked.</param>
        protected override void GoBack(object sender, RoutedEventArgs e)
        {
            if (this.UsingLogicalPageNavigation() && lvPrograms.SelectedItem != null)
            {
                // When logical page navigation is in effect and there's a selected item that
                // item's details are currently displayed.  Clearing the selection will return to
                // the item list.  From the user's point of view this is a logical backward
                // navigation.
                this.lvPrograms.SelectedItem = null;
            }
            else
            {
                // When logical page navigation is not in effect, or when there is no selected
                // item, use the default back button behavior.
                base.GoBack(sender, e);
            }
        }

        /// <summary>
        /// Invoked to determine the name of the visual state that corresponds to an application
        /// view state.
        /// </summary>
        /// <param name="viewState">The view state for which the question is being posed.</param>
        /// <returns>The name of the desired visual state.  This is the same as the name of the
        /// view state except when there is a selected item in portrait and snapped views where
        /// this additional logical page is represented by adding a suffix of _Detail.</returns>
        protected override string DetermineVisualState(ApplicationViewState viewState)
        {
            // Update the back button's enabled state when the view state changes
            var logicalPageBack = this.UsingLogicalPageNavigation(viewState) && this.lvPrograms.SelectedItem != null;
            var physicalPageBack = this.Frame != null && this.Frame.CanGoBack;
            this.DefaultViewModel["CanGoBack"] = logicalPageBack || physicalPageBack;

            // Determine visual states for landscape layouts based not on the view state, but
            // on the width of the window.  This page has one layout that is appropriate for
            // 1366 virtual pixels or wider, and another for narrower displays or when a snapped
            // application reduces the horizontal space available to less than 1366.
            if (viewState == ApplicationViewState.Filled ||
                viewState == ApplicationViewState.FullScreenLandscape)
            {
                var windowWidth = Window.Current.Bounds.Width;
                if (windowWidth >= 1366) return "FullScreenLandscapeOrWide";
                return "FilledOrNarrow";
            }

            // When in portrait or snapped start with the default visual state name, then add a
            // suffix when viewing details instead of the list
            var defaultStateName = base.DetermineVisualState(viewState);
            return logicalPageBack ? defaultStateName + "_Detail" : defaultStateName;
        }

        #endregion

        #region CreateNewProgram Button Methods

        /// <summary>
        /// This method is invoked when the "btnCreateNewProgram" Button is clicked. This method
        /// will show the UI elements used for creating a new Workout Program and hide any other elements.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void btnCreateNewProgram_Click(object sender, RoutedEventArgs e)
        {
            // Hide and clean up any other area that may be open
            this.CollapseGridViewElements();
            this.CleanUpProgramInfo();
            this.CleanUpRecordProgram();
            this.CleanUpDeleteProgram();

            // Change the header above the area the user is using
            this.tbSelectedProgramHeader.Text = "Create A Program";

            // Show the "NewProgram" area
            this.ShowNewProgramElements();

            // Populate the ListView of Workouts
            this.UpdateWorkoutListView();
        }

        /// <summary>
        /// This method is invoked when the btnAddNewProgram" Button is clicked. This method will
        /// attempt to add a New Program to the 
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void btnAddNewProgram_Click(object sender, RoutedEventArgs e)
        {
            // Reset the default values of each element in the "NewProgram" area
            this.ResetDefaults();

            // Check that each element contains valid data. If not, inform user and exit method
            if (this.CheckNewProgramTextBoxes() == false)
            {
                this.tbNewProgramError.Visibility = Windows.UI.Xaml.Visibility.Visible;
                this.tbNewProgramError.Text = "Please enter values in to the areas highlighted in red.";
                return;
            }

            List<string> workoutList = this.lbxNewProgramSelectedWorkouts.Items.OfType<string>().ToList();
            this.lbxNewProgramSelectedWorkouts.ItemsSource = null;
            JsonReply jr = this.ac.PostNewProgram(this.tbxNewProgramName.Text, this.tbxNewProgramNote.Text, workoutList, false);

            // Check if the Workout was added successfully
            if (jr.errorCode.Equals(APIErrorCodes.Success))
            {
                // Set the UI elements used to create the new Program to default and hide them
                this.ClearNewProgramElements();

                // Hide the UI elements used for creating a new Program
                this.CollapseNewProgramElements();

                // Inform the user that the Workout was created successfully
                this.tbSelectedProgramHeader.Text = string.Empty;
                this.tbCentreScreen.Text = "Program added successfully";
                this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;

                // Update the Workout ListView to show the new Program (which shouldn't have any entries)
                this.UpdateProgramListView();

                // Set the selected item in the ListView to null
                this.lvPrograms.SelectedItem = null;
            }
            else if (jr.errorCode.Equals(APIErrorCodes.ProgramAlreadyExists))
            {
                // The Program name has already been used
                this.tbxNewProgramName.BorderThickness = new Thickness(0);
                this.tbNewProgramError.Visibility = Windows.UI.Xaml.Visibility.Visible;
                this.tbNewProgramError.Text = "This name is being used by another one of yours Programs.";
            }
            else if (jr.errorCode.Equals(APIErrorCodes.ServerOffline))
            {
                // The server is offline
                this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;
                this.tbCentreScreen.Text = "The server is offline. Please try again in a moment.";
            }
            else
            {
                // The Program could not be added to the database
                this.tbNewProgramError.Visibility = Windows.UI.Xaml.Visibility.Visible;
                this.tbNewProgramError.Text = "The Program could not be added. Please try using new details.";
                this.tbNewProgramError.Text += "\n" + jr.errorCode + "\n" + jr.status;
            }
        }
        
        #endregion CreateNewProgram Button Methods

        #region RecordProgram Button Methods

        /// <summary>
        /// This method is invoked when the "btnRecordProgram" Button is clicked. This method
        /// will show the UI elements used for recording a Workout Program and hide any other elements.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void btnRecordProgram_Click(object sender, RoutedEventArgs e)
        {
            // Hide the GridView elements and add the correct header to this section of the page
            this.CollapseGridViewElements();
            this.tbSelectedProgramHeader.Text = "Record A Program";

            // Clean up the "CreateNewProgram", "ProgramInfo", and "DeleteProgram" areas
            this.CleanUpNewProgram();
            this.CleanUpProgramInfo();
            this.CleanUpDeleteProgram();

            // Check if the user has selected a Workout from the ListView on the right
            if (this.lvPrograms.SelectedItem == null)
            {
                // If no Program is select, inform the user and exit this method.
                this.tbCentreScreen.Text = "Please select a Program from the list on the left";
                this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;
                return;
            }

            // Set the counter and list of workouts
            this.recordProgramCounter = 0;
            this.recordProgramWorkouts = this.ac.GetProgramSkeletonWithWorkouts(this.lvPrograms.SelectedItem.ToString()).workouts;

            // Check if the Program would not be found
            if (this.recordProgramWorkouts == null)
            {
                this.tbCentreScreen.Text = "The selected Program could not be found!";
                this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;
                return;
            }

            // Update the text on the submit button
            this.btnNextAndSubmitRecordProgram.Content = "Submit And Go To Next Exercise";

            // Show the "RecordAProgram" elements/controls
            this.ShowRecordProgramElements();

            // Get the name of the Workout being recorded and display it in the header
            string recordWorkoutName = this.recordProgramWorkouts[this.recordProgramCounter].name;
            this.tbSelectedProgramHeader.Text = "Recording " + this.lvPrograms.SelectedItem.ToString() + ": " + recordWorkoutName;
        }

        /// <summary>
        /// This method is invoked when the "btnSumbitRecordProgram" Button is clicked. This method
        /// will attempt to submit a record of a Program based on the data entered by the user.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void btnNextAndSubmitRecordProgram_Click(object sender, RoutedEventArgs e)
        {
            // Hide the TextBox for errors about recording a Program and set it's Text to the empty string
            this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbCentreScreen.Text = string.Empty;

            // Check the counter and the List of Workouts for the Program being recorded
            if (this.recordProgramCounter < 0 || this.recordProgramWorkouts == null || this.recordProgramWorkouts.Count == 0)
            {
                // Clean up RecordProgram area and reset counter and Workout List
                this.CleanUpRecordProgram();
                this.recordProgramCounter = 0;
                this.recordProgramWorkouts = null;

                // Inform user of failure to record Program
                this.tbCentreScreen.Text = "This Program contains corrupt data.";
                this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }

            // Reset the border thickness of each textBox to zero
            this.ResetBorderForRecordProgram();

            // Get name of Workout being recorded
            string recordWorkoutName = this.recordProgramWorkouts[this.recordProgramCounter].name;

            // Check the TextBoxes for invalid data
            if (this.CheckRecordProgramTextBoxes())
            {
                // JsonReply object for submitting the Program
                JsonReply submitProgramReply = new JsonReply();

                // Check if this is the first Workout in the Program
                // If it is, post the program object and check the reply
                if (this.recordProgramCounter == 0)
                {
                    submitProgramReply = this.SubmitRecordProgram(this.lvPrograms.SelectedItem.ToString());
                    this.recordProgramTimestamp = submitProgramReply.status;

                    if (submitProgramReply.errorCode != APIErrorCodes.Success)
                    {
                        // Errors are handled in the method called in the if statement
                        return;
                    }
                }

                // The submitProgramReply object now holds the timestamp (to be used with the Workouts)
                List<Set> sets = this.CollectSets();

                // Make the call to the API server
                JsonReply jr = ac.PostRecordWorkout(recordWorkoutName, this.tbxRecordProgramNote.Text, sets, this.recordProgramCounter, this.recordProgramTimestamp);

                // Check the response from the server
                if (jr.errorCode == APIErrorCodes.Success)
                {
                    // Increment the counter
                    this.recordProgramCounter++;

                    // Clear the TextBoxes and collapse the extra TextBoxes
                    this.ClearRecordProgramElements();
                    this.CollapseExtraSetTextBoxes();

                    // Check if the counter is at the last workout
                    if (this.recordProgramCounter == this.recordProgramWorkouts.Count - 1)
                    {
                        // Update the text on the submit button
                        this.btnNextAndSubmitRecordProgram.Content = "Submit And Finish";
                    }

                    // Check if the counter is now out of range (finished recording)
                    if (this.recordProgramCounter >= this.recordProgramWorkouts.Count)
                    {
                        // Clean up RecordProgram area and reset counter and Workout List
                        this.CleanUpRecordProgram();
                        this.recordProgramCounter = 0;
                        this.recordProgramWorkouts = null;
                        this.lvPrograms.SelectedItem = null;
                        this.recordProgramTimestamp = null;

                        // Inform user of successful recording of Program and update the header
                        this.tbCentreScreen.Text = "The Program was recorded successfully!";
                        this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        this.tbSelectedProgramHeader.Text = string.Empty;

                        return;
                    }

                    // Inform the user that the Workout was submitted successfully
                    this.tbxErrorRecordingProgram.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    this.tbxErrorRecordingProgram.Text = recordWorkoutName + " recorded.";

                    //  Update the name of the Workout being recorded and display it in the header
                    recordWorkoutName = this.recordProgramWorkouts[this.recordProgramCounter].name;
                    this.tbSelectedProgramHeader.Text = "Recording " + this.lvPrograms.SelectedItem.ToString() + ": " + recordWorkoutName;
                }
                else if (jr.errorCode == APIErrorCodes.ValidationError)
                {
                    this.tbxErrorRecordingProgram.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    this.tbxErrorRecordingProgram.Text = "\nValidation Error. Reply from server:\n" + jr.status;
                }
                else
                {
                    // The JsonReply indicates an unsuccessful API call. Display the response
                    this.tbxErrorRecordingProgram.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    this.tbxErrorRecordingProgram.Text = "\nJsonReply status:\n" + jr.status;
                }
            }
            else
            {
                // At least one of the sets contains invalid information
                this.tbxErrorRecordingProgram.Visibility = Windows.UI.Xaml.Visibility.Visible;
                this.tbxErrorRecordingProgram.Text = "Please enter values into the boxes highlight in red.";
            }
        }

        /// <summary>
        /// This method will attempt to Record a Program object in the database. If the Program
        /// is not recorded, this method will inform the user of the failure.
        /// </summary>
        /// <param name="programName"> The name of the Program to be added. </param>
        /// <returns> A JsonReply object indicating the success status of the POST request. </returns>
        private JsonReply SubmitRecordProgram(string programName)
        {
            JsonReply jr = this.ac. PostRecordProgram(programName, string.Empty, true);

            if (jr.errorCode != APIErrorCodes.Success)
            {
                this.tbxErrorRecordingProgram.Text = "Error while recording Program:\n" + jr.ToString();
            }

            return jr;
        }

        #endregion RecordProgram Button Methods

        #region DeleteWorkout Button Methods

        /// <summary>
        /// This method is invoked when the "btnDeleteProgram" Button is clicked. This method
        /// will show the UI elements/Controls in the "DeleteProgram" area, Clean Up any other
        /// areas, and set some TextBlock values inside the "DeleteProgram" area.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void btnDeleteProgram_Click(object sender, RoutedEventArgs e)
        {
            // Hide GriView Elements and Clean up the "CreateNewProgram, "RecordProgram", and "ProgramInfo" areas
            this.CollapseGridViewElements();
            this.CleanUpNewProgram();
            this.CleanUpProgramInfo();
            this.CleanUpRecordProgram();

            // Check if the user has selected a Workout from the ListView on the right
            if (this.lvPrograms.SelectedItem == null)
            {
                // If no Workout is select, inform the user and exit this method.
                this.tbCentreScreen.Text = "Please select a Program from the List on the left to delete";
                this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;
                return;
            }

            // Add the correct header to this section of the page
            this.tbSelectedProgramHeader.Text = "Delete The Program: " + this.lvPrograms.SelectedItem.ToString();

            // Add the header inside the "DeleteWorkout" area
            this.tbDeleteProgramHeader.Text = "Are you sure you want to delete \"" + this.lvPrograms.SelectedItem.ToString() + "\"?";

            // Show the "DeleteAWorkout" elements/controls
            this.ShowDeleteProgramElements();
        }

        /// <summary>
        /// This method is invoked when the "btnConfirmDeleteProgram" Button is clicked. This method
        /// will attempt to delete the selected Program, from the ListView above, from the database.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void btnConfirmDeleteProgram_Click(object sender, RoutedEventArgs e)
        {
            // The ListView should have a selected item but check just in case
            // Check if the user has selected a Program from the ListView on the right
            if (this.lvPrograms.SelectedItem == null)
            {
                // If no Program is select, inform the user and exit this method.
                this.tbCentreScreen.Text = "Please select a Program from the List on the left to delete";
                this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;
                return;
            }

            // Make the call to the API server
            JsonReply jr = this.ac.DeleteProgramSkeleton(this.lvPrograms.SelectedItem.ToString());

            if (jr.errorCode == APIErrorCodes.Success)
            {
                // Clear the TextBoxes
                this.CleanUpDeleteProgram();

                // Set the selected item in the ListView to null
                this.lvPrograms.SelectedItem = null;

                // Inform the user that the Program was recorded successfully
                this.tbSelectedProgramHeader.Text = string.Empty;
                this.tbCentreScreen.Text = "Program deleted successfully";
                this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;

                // Update the Workout ListView to show the new Program (which shouldn't have any entries)
                this.UpdateProgramListView();

                // Set the selected item in the ListView to null
                this.lvPrograms.SelectedItem = null;
            }
            else if (jr.errorCode == APIErrorCodes.ValidationError)
            {
                this.tbDeleteProgramError.Text = "\nJsonReply status:\n" + jr.status;
            }
            else
            {
                // The JsonReply indicates an unsuccessful API call. Display the response
                this.tbDeleteProgramError.Text = "\nError while deleting Program:\n" + jr.status;
            }

        }

        /// <summary>
        /// This method is invoked when the "btnDeleteProgram" Button is clicked. This method
        /// will hide the "DeleteProgram" area and inform the user to select a Program.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void btnCancelDeleteProgram_Click(object sender, RoutedEventArgs e)
        {
            // User has decided to not delete the Workout. Hide the "DeleteWorkout" area
            this.CleanUpDeleteProgram();

            // Set the ListView selection to null
            this.lvPrograms.SelectedItem = null;

            // Show the standard message
            this.tbCentreScreen.Text = "The Program was not deleted. Select a Program to view it.";
            this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        #endregion DeleteWorkout Button Methods

        /// <summary>
        /// This method will attempt to update the "lvNewProgramWorkoutList" ListView with a List of the
        /// users Workouts. This method makes a call to the API to get a list of distinct Workout names.
        /// </summary>
        /// <returns> True if the ListView was successfully updated. Otherwise, false. </returns>
        private bool UpdateWorkoutListView()
        {
            // Get the list of workouts for this user
            //List<JsonWorkout.WorkoutDetails> workouts = ac.GetWorkout("");
            List<string> woNames = ac.GetDistinctWorkoutNames();

            try
            {
                // Set the List of workouts as the source for the ListView of Workouts
                this.lvNewProgramWorkoutList.ItemsSource = woNames;
                // Set the default select item to null
                this.lvNewProgramWorkoutList.SelectedItem = null;

                // Check if there List fo workout names is empty. If it is, inform the user
                if (woNames.Count == 0)
                {
                    // Hide the "NewProgram" area and display the error message
                    this.CollapseNewProgramElements();
                    this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    this.tbCentreScreen.Text = "You haven't created any Exercises yet.";

                    // Clear the UI elements in the "NewProgram" area
                    this.ClearNewProgramElements();
                }
                return true;
            }
            catch (NullReferenceException ex)
            {
                // Call ToString() to avoid 'variable not used' warning
                ex.ToString();
                // Failed to retrieve the List of workout names
                this.CollapseNewProgramElements();
                this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;
                this.tbCentreScreen.Text = "The List of Exercise names could not be retrieved. The server may be offline.";

                // Clear the UI elements in the "NewProgram" area
                this.ClearNewProgramElements();
                return false;
            }
        }

        /// <summary>
        /// This method will hide the "gvSelectedPrograms" GridView and the TextBlock used for
        /// displaying error messages about the GridView
        /// </summary>
        private void CollapseGridViewElements()
        {
            this.gvSelectedPrograms.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        #region CreateNewProgram Methods

        /// <summary>
        /// This method will set the visibility of each element used in creating a new Program to Collapsed.
        /// </summary>
        private void CollapseNewProgramElements()
        {
            this.gridNewProgram.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        /// <summary>
        /// This method will set the visibility of each element used in creating a new Program to Visible.
        /// </summary>
        private void ShowNewProgramElements()
        {
            this.gridNewProgram.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        /// <summary>
        /// This method checks for null, empty, or whitespace values in the TextBoxes that are
        /// used to create a new Program. This method will increase the border thickness of the
        /// TextBox(es) that are not valid.
        /// </summary>
        /// <returns> True if all the TextBoxes for adding a new Program contain valid text. </returns>
        private bool CheckNewProgramTextBoxes()
        {
            // This bool is set to false if any of the UI elements or controls are not valid
            bool valid = true;

            // Check the new Workout name supplied
            if (string.IsNullOrWhiteSpace(this.tbxNewProgramName.Text))
            {
                this.tbxNewProgramName.BorderThickness = new Thickness(2);
                valid = false;
            }

            // Check that the ListBox contains at least one item (Workout)
            if (this.lbxNewProgramSelectedWorkouts.Items == null || this.lbxNewProgramSelectedWorkouts.Items.Count < 1)
            {
                this.lbxNewProgramSelectedWorkouts.BorderThickness = new Thickness(2);
                valid = false;
            }

            return valid;
        }

        /// <summary>
        /// This method will put an empty string in to each TextBox used for creating a new Program
        /// </summary>
        private void ClearNewProgramElements()
        {
            this.tbxNewProgramName.Text = string.Empty;
            this.tbxNewProgramNote.Text = string.Empty;
            this.lbxNewProgramSelectedWorkouts.ItemsSource = null;
        }

        /// <summary>
        /// This method sets the Border Thickness of each textBox used in creating a new Program to zero.
        /// </summary>
        private void ResetBorderForNewProgram()
        {
            this.tbxNewProgramName.BorderThickness = new Thickness(0);
            this.lvNewProgramWorkoutList.BorderThickness = new Thickness(0);
            this.lbxNewProgramSelectedWorkouts.BorderThickness = new Thickness(0);
        }

        /// <summary>
        /// This method resets the default values for the border thickness, contents, and visibility
        /// for the controls in the "NewProgram" area.
        /// </summary>
        private void ResetDefaults()
        {
            this.ResetBorderForNewProgram();
            this.tbNewProgramError.Text = string.Empty;
            this.tbNewProgramError.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        /// <summary>
        /// This method will tidy up all the UI elements involved in displaying
        /// information about a Program. This method should be called when the user
        /// wants to use another area on this page and stop looking at the "NewWorkout"
        /// area.
        /// </summary>
        private void CleanUpNewProgram()
        {
            // Hide the elements for the "NewProgram" area
            this.CollapseNewProgramElements();

            // Empty the text in the TextBoxes in the "NewProgram" area
            this.ClearNewProgramElements();

            // Reset the border thickness of the Set Textboxes
            this.ResetBorderForNewProgram();
        }
        
        /// <summary>
        /// This method is invoked when the selected item in the "lvNewProgramWorkoutList" ListView changes.
        /// This method will add the selected item from the ListView to the "lbxNewProgramSelectedWorkouts"
        /// ListBox if the ListBox does not already contain the item.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void lvNewProgramWorkoutList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Check if the selected item is null, if it is do nothing
            if (this.lvNewProgramWorkoutList.SelectedItem == null)
            {
                return;
            }

            // If the ListBox doesn't already contain this Workout, add it. Otherwise, do nothing.
            if (!this.lbxNewProgramSelectedWorkouts.Items.Contains(this.lvNewProgramWorkoutList.SelectedItem))
            {
                this.lbxNewProgramSelectedWorkouts.Items.Add(this.lvNewProgramWorkoutList.SelectedItem);
            }
        }

        /// <summary>
        /// This method is invoked when the selected item in the "lbxNewProgramSelectedWorkouts" ListBox changes.
        /// This method will removed the selected item from the ListBox.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void lbxNewProgramSelectedWorkouts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Check if the selected item is null, if it is do nothing
            if (this.lbxNewProgramSelectedWorkouts.SelectedItem == null)
            {
                return;
            }

            this.lbxNewProgramSelectedWorkouts.Items.Remove(this.lbxNewProgramSelectedWorkouts.SelectedItem);
        }

        #endregion CreateNewProgram Methods

        #region RecordProgram Methods

        /// <summary>
        /// This method will set the visibility of each element used in recording a Program to Collapsed.
        /// </summary>
        private void CollapseRecordProgramElements()
        {
            // Hide all weight and rep TextBoxes from 4 - 10
            this.CollapseExtraSetTextBoxes();

            // Hide the workout note header, workout note TextBox, and submit Button
            this.gridRecordProgram.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

            // Set the vertical offset of the ScrollViewer to zero
            this.svRecordProgram.ScrollToVerticalOffset(0);
        }

        /// <summary>
        /// This method will collapse the TextBoxes used for Sets 4 to 10.
        /// </summary>
        private void CollapseExtraSetTextBoxes()
        {
            this.tbxWeight4.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxReps4.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxNote4.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxWeight5.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxReps5.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxNote5.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxWeight6.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxReps6.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxNote6.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxWeight7.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxReps7.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxNote7.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxWeight8.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxReps8.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxNote8.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxWeight9.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxReps9.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxNote9.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxWeight10.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxReps10.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbxNote10.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        /// <summary>
        /// This method will set the visibility of each element used in recording a Program to Visible.
        /// </summary>
        private void ShowRecordProgramElements()
        {
            // Show the grid containing all of the RecordProgram elements
            this.gridRecordProgram.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        /// <summary>
        /// This method checks for null, empty, or whitespace values in the TextBoxes that are
        /// used to record a Program. This method will increase the border thickness of the
        /// TextBox(es) that are not valid.
        /// </summary>
        /// <returns> True if all the TextBoxes for recording a Program contain valid text. </returns>
        private bool CheckRecordProgramTextBoxes()
        {
            // This bool is set to false if any of the TextBoxes are not valid
            bool valid = true;

            // Check the first sets values supplied
            if (string.IsNullOrWhiteSpace(this.tbxWeight1.Text))
            {
                this.tbxWeight1.BorderThickness = new Thickness(2);
                valid = false;
            }

            if (string.IsNullOrWhiteSpace(this.tbxReps1.Text))
            {
                this.tbxReps1.BorderThickness = new Thickness(2);
                valid = false;
            }

            // Check the second set. If any TextBox contains valid data, then the others must too
            if (!string.IsNullOrWhiteSpace(this.tbxWeight2.Text) || !string.IsNullOrWhiteSpace(this.tbxReps2.Text) || !string.IsNullOrWhiteSpace(this.tbxNote2.Text))
            {
                // Check each TextBox
                if (string.IsNullOrWhiteSpace(this.tbxWeight2.Text))
                {
                    this.tbxWeight2.BorderThickness = new Thickness(2);
                    valid = false;
                }

                if (string.IsNullOrWhiteSpace(this.tbxReps2.Text))
                {
                    this.tbxReps2.BorderThickness = new Thickness(2);
                    valid = false;
                }
            }

            // Check the third set. If any TextBox contains valid data, then the others must too
            if (!string.IsNullOrWhiteSpace(this.tbxWeight3.Text) || !string.IsNullOrWhiteSpace(this.tbxReps3.Text) || !string.IsNullOrWhiteSpace(this.tbxNote3.Text))
            {
                // Check each TextBox
                if (string.IsNullOrWhiteSpace(this.tbxWeight3.Text))
                {
                    this.tbxWeight3.BorderThickness = new Thickness(2);
                    valid = false;
                }

                if (string.IsNullOrWhiteSpace(this.tbxReps3.Text))
                {
                    this.tbxReps3.BorderThickness = new Thickness(2);
                    valid = false;
                }
            }

            /*
             * The remaining TextBoxes for Sets 4 to 10 are hidden by default. First check their visibility. If they
             * are collapsed, then return the valid variable because the remaining Set TextBoxes should not contain data
             * because they are set to collapsed.
             */

            // Check visibility first, then check the data if the TextBoxes are visibile
            if (this.tbxWeight4.Visibility.Equals(Windows.UI.Xaml.Visibility.Visible))
            {
                if (!string.IsNullOrWhiteSpace(this.tbxWeight4.Text) || !string.IsNullOrWhiteSpace(this.tbxReps4.Text) || !string.IsNullOrWhiteSpace(this.tbxNote4.Text))
                {
                    // Check each TextBox
                    if (string.IsNullOrWhiteSpace(this.tbxWeight4.Text))
                    {
                        this.tbxWeight4.BorderThickness = new Thickness(2);
                        valid = false;
                    }

                    if (string.IsNullOrWhiteSpace(this.tbxReps4.Text))
                    {
                        this.tbxReps4.BorderThickness = new Thickness(2);
                        valid = false;
                    }
                }
            }
            else
            {
                return valid;
            }

            // Check visibility first, then check the data if the TextBoxes are visibile
            if (this.tbxWeight5.Visibility.Equals(Windows.UI.Xaml.Visibility.Visible))
            {
                if (!string.IsNullOrWhiteSpace(this.tbxWeight5.Text) || !string.IsNullOrWhiteSpace(this.tbxReps5.Text) || !string.IsNullOrWhiteSpace(this.tbxNote5.Text))
                {
                    // Check each TextBox
                    if (string.IsNullOrWhiteSpace(this.tbxWeight5.Text))
                    {
                        this.tbxWeight5.BorderThickness = new Thickness(2);
                        valid = false;
                    }

                    if (string.IsNullOrWhiteSpace(this.tbxReps5.Text))
                    {
                        this.tbxReps5.BorderThickness = new Thickness(2);
                        valid = false;
                    }
                }
            }
            else
            {
                return valid;
            }

            // Check visibility first, then check the data if the TextBoxes are visibile
            if (this.tbxWeight6.Visibility.Equals(Windows.UI.Xaml.Visibility.Visible))
            {
                if (!string.IsNullOrWhiteSpace(this.tbxWeight6.Text) || !string.IsNullOrWhiteSpace(this.tbxReps6.Text) || !string.IsNullOrWhiteSpace(this.tbxNote6.Text))
                {
                    // Check each TextBox
                    if (string.IsNullOrWhiteSpace(this.tbxWeight6.Text))
                    {
                        this.tbxWeight6.BorderThickness = new Thickness(2);
                        valid = false;
                    }

                    if (string.IsNullOrWhiteSpace(this.tbxReps6.Text))
                    {
                        this.tbxReps6.BorderThickness = new Thickness(2);
                        valid = false;
                    }
                }
            }
            else
            {
                return valid;
            }

            // Check visibility first, then check the data if the TextBoxes are visibile
            if (this.tbxWeight7.Visibility.Equals(Windows.UI.Xaml.Visibility.Visible))
            {
                if (!string.IsNullOrWhiteSpace(this.tbxWeight7.Text) || !string.IsNullOrWhiteSpace(this.tbxReps7.Text) || !string.IsNullOrWhiteSpace(this.tbxNote7.Text))
                {
                    // Check each TextBox
                    if (string.IsNullOrWhiteSpace(this.tbxWeight7.Text))
                    {
                        this.tbxWeight7.BorderThickness = new Thickness(2);
                        valid = false;
                    }

                    if (string.IsNullOrWhiteSpace(this.tbxReps7.Text))
                    {
                        this.tbxReps7.BorderThickness = new Thickness(2);
                        valid = false;
                    }
                }
            }
            else
            {
                return valid;
            }

            // Check visibility first, then check the data if the TextBoxes are visibile
            if (this.tbxWeight8.Visibility.Equals(Windows.UI.Xaml.Visibility.Visible))
            {
                if (!string.IsNullOrWhiteSpace(this.tbxWeight8.Text) || !string.IsNullOrWhiteSpace(this.tbxReps7.Text) || !string.IsNullOrWhiteSpace(this.tbxNote7.Text))
                {
                    // Check each TextBox
                    if (string.IsNullOrWhiteSpace(this.tbxWeight8.Text))
                    {
                        this.tbxWeight8.BorderThickness = new Thickness(2);
                        valid = false;
                    }

                    if (string.IsNullOrWhiteSpace(this.tbxReps8.Text))
                    {
                        this.tbxReps8.BorderThickness = new Thickness(2);
                        valid = false;
                    }
                }
            }
            else
            {
                return valid;
            }

            // Check visibility first, then check the data if the TextBoxes are visibile
            if (this.tbxWeight9.Visibility.Equals(Windows.UI.Xaml.Visibility.Visible))
            {
                if (!string.IsNullOrWhiteSpace(this.tbxWeight9.Text) || !string.IsNullOrWhiteSpace(this.tbxReps9.Text) || !string.IsNullOrWhiteSpace(this.tbxNote9.Text))
                {
                    // Check each TextBox
                    if (string.IsNullOrWhiteSpace(this.tbxWeight9.Text))
                    {
                        this.tbxWeight9.BorderThickness = new Thickness(2);
                        valid = false;
                    }

                    if (string.IsNullOrWhiteSpace(this.tbxReps9.Text))
                    {
                        this.tbxReps9.BorderThickness = new Thickness(2);
                        valid = false;
                    }
                }
            }
            else
            {
                return valid;
            }

            // Check visibility first, then check the data if the TextBoxes are visibile
            if (this.tbxWeight10.Visibility.Equals(Windows.UI.Xaml.Visibility.Visible))
            {
                if (!string.IsNullOrWhiteSpace(this.tbxWeight10.Text) || !string.IsNullOrWhiteSpace(this.tbxReps10.Text) || !string.IsNullOrWhiteSpace(this.tbxNote10.Text))
                {
                    // Check each TextBox
                    if (string.IsNullOrWhiteSpace(this.tbxWeight10.Text))
                    {
                        this.tbxWeight10.BorderThickness = new Thickness(2);
                        valid = false;
                    }

                    if (string.IsNullOrWhiteSpace(this.tbxReps10.Text))
                    {
                        this.tbxReps10.BorderThickness = new Thickness(2);
                        valid = false;
                    }
                }
            }
            else
            {
                return valid;
            }

            return valid;
        }

        /// <summary>
        /// This method will put an empty string in to each TextBox used for recording a Program
        /// </summary>
        private void ClearRecordProgramElements()
        {
            this.tbxWeight1.Text = string.Empty;
            this.tbxReps1.Text = string.Empty;
            this.tbxNote1.Text = string.Empty;
            this.tbxWeight2.Text = string.Empty;
            this.tbxReps2.Text = string.Empty;
            this.tbxNote2.Text = string.Empty;
            this.tbxWeight3.Text = string.Empty;
            this.tbxReps3.Text = string.Empty;
            this.tbxNote3.Text = string.Empty;
            this.tbxWeight4.Text = string.Empty;
            this.tbxReps4.Text = string.Empty;
            this.tbxNote4.Text = string.Empty;
            this.tbxWeight5.Text = string.Empty;
            this.tbxReps5.Text = string.Empty;
            this.tbxNote5.Text = string.Empty;
            this.tbxWeight6.Text = string.Empty;
            this.tbxReps6.Text = string.Empty;
            this.tbxNote6.Text = string.Empty;
            this.tbxWeight7.Text = string.Empty;
            this.tbxReps7.Text = string.Empty;
            this.tbxNote7.Text = string.Empty;
            this.tbxWeight8.Text = string.Empty;
            this.tbxReps8.Text = string.Empty;
            this.tbxNote8.Text = string.Empty;
            this.tbxWeight9.Text = string.Empty;
            this.tbxReps9.Text = string.Empty;
            this.tbxNote9.Text = string.Empty;
            this.tbxWeight10.Text = string.Empty;
            this.tbxReps10.Text = string.Empty;
            this.tbxNote10.Text = string.Empty;

            this.tbxRecordProgramNote.Text = string.Empty;
        }

        /// <summary>
        /// This method sets the Border Thickness of each textBox used in recording sets to zero.
        /// </summary>
        private void ResetBorderForRecordProgram()
        {
            this.tbxWeight1.BorderThickness = new Thickness(0);
            this.tbxReps1.BorderThickness = new Thickness(0);
            this.tbxNote1.BorderThickness = new Thickness(0);

            this.tbxWeight2.BorderThickness = new Thickness(0);
            this.tbxReps2.BorderThickness = new Thickness(0);
            this.tbxNote2.BorderThickness = new Thickness(0);

            this.tbxWeight3.BorderThickness = new Thickness(0);
            this.tbxReps3.BorderThickness = new Thickness(0);
            this.tbxNote3.BorderThickness = new Thickness(0);

            this.tbxWeight4.BorderThickness = new Thickness(0);
            this.tbxReps4.BorderThickness = new Thickness(0);
            this.tbxNote4.BorderThickness = new Thickness(0);

            this.tbxWeight5.BorderThickness = new Thickness(0);
            this.tbxReps5.BorderThickness = new Thickness(0);
            this.tbxNote5.BorderThickness = new Thickness(0);

            this.tbxWeight6.BorderThickness = new Thickness(0);
            this.tbxReps6.BorderThickness = new Thickness(0);
            this.tbxNote6.BorderThickness = new Thickness(0);

            this.tbxWeight7.BorderThickness = new Thickness(0);
            this.tbxReps7.BorderThickness = new Thickness(0);
            this.tbxNote7.BorderThickness = new Thickness(0);

            this.tbxWeight8.BorderThickness = new Thickness(0);
            this.tbxReps8.BorderThickness = new Thickness(0);
            this.tbxNote8.BorderThickness = new Thickness(0);

            this.tbxWeight9.BorderThickness = new Thickness(0);
            this.tbxReps9.BorderThickness = new Thickness(0);
            this.tbxNote9.BorderThickness = new Thickness(0);

            this.tbxWeight10.BorderThickness = new Thickness(0);
            this.tbxReps10.BorderThickness = new Thickness(0);
            this.tbxNote10.BorderThickness = new Thickness(0);
        }

        /// <summary>
        /// This method will tidy up all the UI elements involved in displaying
        /// information about a Program. This method should be called when the user
        /// wants to use another area on this page and stop looking at the "RecordProgram"
        /// area.
        /// </summary>
        private void CleanUpRecordProgram()
        {
            // Hide the elements for the "RecordProgram" area
            this.CollapseRecordProgramElements();

            // Empty the text in the TextBoxes in the "RecordProgram" area
            this.ClearRecordProgramElements();

            // Reset the border thickness of the Set Textboxes
            this.ResetBorderForRecordProgram();
        }

        /// <summary>
        /// This method is invoked when a key is presssed down in one of the TextBoxes used for holding data
        /// about either the Weight or Repetitions for a Set. This method will only accept characters that are
        /// digits. It will also accept a single decimal point (.) from the num pad. Any other characters will
        /// not be added to the TextBox that invoked this method.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void SetTextBox_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            // Check for only one decimal point
            if (e.Key == Windows.System.VirtualKey.Decimal && (sender as TextBox).Text.IndexOf('.') == -1)
            {
                return;
            }

            // Check if the value is too small or big to be a number
            if (e.Key < Windows.System.VirtualKey.Number0 || e.Key > Windows.System.VirtualKey.NumberPad9)
            {
                e.Handled = true;
            }

            // Check if the value is in between 0 - 9 and 0 - 9 on the num pad
            if (e.Key > Windows.System.VirtualKey.Number9 && e.Key < Windows.System.VirtualKey.NumberPad0)
            {
                e.Handled = true;
            }
        }

        #endregion RecordProgram Methods

        #region ProgramInfo Methods

        /// <summary>
        /// This method will set the visibility of each element used in displaying information about
        /// a Program to Collapsed.
        /// </summary>
        private void CollapseProgramInfoElements()
        {
            // Hide the GridView containing all of the elements for "WorkoutInfo"
            this.gridProgramInfo.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        /// <summary>
        /// This method will set the visibility of each element used in displaying information about
        /// a Program to Visible.
        /// </summary>
        private void ShowProgramInfoElements()
        {
            // Show the GridView containing all of the elements for "WorkoutInfo"
            this.gridProgramInfo.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        /// <summary>
        /// This method will put an empty string in to each TextBox used for displaying information
        /// about a Program.
        /// </summary>
        private void ClearProgramInfoElements()
        {
            this.tbProgramInfoSubheader.Text = string.Empty;
            this.tbProgramInfoWorkoutsDetails.Text = string.Empty;
        }

        /// <summary>
        /// This method will tidy up all the UI elements involved in displaying
        /// information about a Program. This method should be called when the user
        /// wants to use another area on this page and stop looking at the "ProgramInfo"
        /// area.
        /// </summary>
        private void CleanUpProgramInfo()
        {
            // Hide the elements for the "WorkoutInfo" area
            this.CollapseProgramInfoElements();

            // Empty the text in the TextBoxes in the "WorkoutInfo" area
            this.ClearProgramInfoElements();
        }

        #endregion ProgramInfo Methods

        #region DeleteProgram Methods

        /// <summary>
        /// This method will set the visibility of each element used in deleting a Program to Collapsed.
        /// </summary>
        private void CollapseDeleteWorkoutElements()
        {
            this.gridDeleteProgram.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        /// <summary>
        /// This method will set the visibility of each element used in deleting a Program to Visible.
        /// </summary>
        private void ShowDeleteProgramElements()
        {
            this.gridDeleteProgram.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        /// <summary>
        /// This method will put an empty string in to each TextBox used for deleteing a Program
        /// </summary>
        private void ClearDeleteProgramElements()
        {
            this.tbDeleteProgramHeader.Text = string.Empty;
            this.tbDeleteProgramError.Text = string.Empty;
        }

        /// <summary>
        /// This method will tidy up all the UI elements involved in displaying
        /// information about a Program. This method should be called when the user
        /// wants to use another area on this page and stop looking at the "DeleteProgram"
        /// area.
        /// </summary>
        private void CleanUpDeleteProgram()
        {
            // Hide the elements for the "DeleteProgram" area
            this.CollapseDeleteWorkoutElements();

            // Empty the text in the TextBoxes in the "DeleteProgram" area
            this.ClearDeleteProgramElements();
        }

        #endregion DeleteProgram Methods

        #region Set Collecting Methods

        /// <summary>
        /// This method will get all of the sets entered by the user when they
        /// are recording a workout.
        /// </summary>
        /// <returns> A List of Set objects for each Set the user entered. </returns>
        private List<Set> CollectSets()
        {
            List<Set> sets = new List<Set>();

            // Try get the first Set
            Set first = this.TryGetSet(this.tbxWeight1, this.tbxReps1, this.tbxNote1);

            if (first != null)
            {
                sets.Add(first);
            }
            else
            {
                return null;
            }

            // Check if the user entered data for the second and third Set
            // If the Weight and Rep TextBoxes are empty for the second Set, no more sets were entered
            if (string.IsNullOrWhiteSpace(this.tbxWeight2.Text) && string.IsNullOrWhiteSpace(this.tbxReps2.Text))
            {
                return sets;
            }
            else
            {
                // Collect the data for the second Set. if the Weight or Rep values are invalid,
                // return null and highlight the TextBox with invalid data
                Set second = this.TryGetSet(this.tbxWeight2, this.tbxReps2, this.tbxNote2);
                if (second != null)
                {
                    sets.Add(second);
                }
                else
                {
                    return null;
                }
            }

            // If the Weight and Rep TextBoxes are empty for the third Set, no more sets were entered
            if (string.IsNullOrWhiteSpace(this.tbxWeight3.Text) && string.IsNullOrWhiteSpace(this.tbxReps3.Text))
            {
                return sets;
            }
            else
            {
                // Collect the data for the third Set. if the Weight or Rep values are invalid,
                // return null and highlight the TextBox with invalid data
                Set third = this.TryGetSet(this.tbxWeight3, this.tbxReps3, this.tbxNote3);
                if (third != null)
                {
                    sets.Add(third);
                }
                else
                {
                    return null;
                }
            }

            // Now check if the user entered data for the remaining Sets (4 to 10).
            // Before checking each set, check the visibility of the Set's TextBoxes.
            // If the Visibility is set to Collapsed, stop checking the Sets and return
            // what has been found already.

            // Check if the fourth Set's TextBoxes are visible and they contain valid data
            if (this.SetHasValuesAndIsVisible(this.tbxWeight4, this.tbxReps4))
            {
                Set fourth = this.TryGetSet(this.tbxWeight4, this.tbxReps4, this.tbxNote4);
                if (fourth != null)
                {
                    sets.Add(fourth);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                // User did not enter data for this Set, return the current List of sets
                return sets;
            }

            // Check if the fifth Set's TextBoxes are visible and they contain valid data
            if (this.SetHasValuesAndIsVisible(this.tbxWeight5, this.tbxReps5))
            {
                Set fifth = this.TryGetSet(this.tbxWeight5, this.tbxReps5, this.tbxNote5);
                if (fifth != null)
                {
                    sets.Add(fifth);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                // User did not enter data for this Set, return the current List of sets
                return sets;
            }

            // Check if the sixth Set's TextBoxes are visible and they contain valid data
            if (this.SetHasValuesAndIsVisible(this.tbxWeight6, this.tbxReps6))
            {
                Set sixth = this.TryGetSet(this.tbxWeight6, this.tbxReps6, this.tbxNote6);
                if (sixth != null)
                {
                    sets.Add(sixth);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                // User did not enter data for this Set, return the current List of sets
                return sets;
            }

            // Check if the seventh Set's TextBoxes are visible and they contain valid data
            if (this.SetHasValuesAndIsVisible(this.tbxWeight7, this.tbxReps7))
            {
                Set seventh = this.TryGetSet(this.tbxWeight7, this.tbxReps7, this.tbxNote7);
                if (seventh != null)
                {
                    sets.Add(seventh);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                // User did not enter data for this Set, return the current List of sets
                return sets;
            }

            // Check if the eighth Set's TextBoxes are visible and they contain valid data
            if (this.SetHasValuesAndIsVisible(this.tbxWeight8, this.tbxReps8))
            {
                Set eighth = this.TryGetSet(this.tbxWeight8, this.tbxReps8, this.tbxNote8);
                if (eighth != null)
                {
                    sets.Add(eighth);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                // User did not enter data for this Set, return the current List of sets
                return sets;
            }

            // Check if the ninth Set's TextBoxes are visible and they contain valid data
            if (this.SetHasValuesAndIsVisible(this.tbxWeight9, this.tbxReps9))
            {
                Set ninth = this.TryGetSet(this.tbxWeight9, this.tbxReps9, this.tbxNote9);
                if (ninth != null)
                {
                    sets.Add(ninth);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                // User did not enter data for this Set, return the current List of sets
                return sets;
            }

            // Check if the tenth Set's TextBoxes are visible and they contain valid data
            if (this.SetHasValuesAndIsVisible(this.tbxWeight10, this.tbxReps10))
            {
                Set tenth = this.TryGetSet(this.tbxWeight10, this.tbxReps10, this.tbxNote10);
                if (tenth != null)
                {
                    sets.Add(tenth);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                // User did not enter data for this Set, return the current List of sets
                return sets;
            }

            return sets;
        }

        /// <summary>
        /// This method takes two TextBoxes (that contian data for a Set) and detemines if a user
        /// entered incorrect values or did not enter anything at all.
        /// </summary>
        /// <param name="tbxWeight"> The TextBox containing a value for the Weight in a Set. </param>
        /// <param name="tbxReps"> The TextBox containing a value for the Repetitions in a Set. </param>
        /// <returns> True if a Set object can be created from the data in the TextBoxes. </returns>
        private bool SetHasValuesAndIsVisible(TextBox tbxWeight, TextBox tbxReps)
        {
            // Check visibility of this Set object's TextBoxes
            if (tbxWeight.Visibility.Equals(Windows.UI.Xaml.Visibility.Collapsed))
            {
                return false;
            }

            // If the Weight and Rep TextBoxes are empty for the this Set, no more sets were entered
            if (string.IsNullOrWhiteSpace(tbxWeight.Text) && string.IsNullOrWhiteSpace(tbxReps.Text))
            {

                return false;
            }

            // The Set's TextBoxes are visible and contain valid data
            return true;
        }

        /// <summary>
        /// This method takes three TextBoxes as parameters and tries to parse the values
        /// to craete a Set object. If a "weight" and a "repetitions" value can not be
        /// parsed, then null is returned.
        /// </summary>
        /// <param name="tbxWeight"> A TextBox containing the value for the weight in the Set. </param>
        /// <param name="tbxReps"> A TextBox containing the value for the reps in the Set. </param>
        /// <param name="tbxNote"> A TextBox containing the value for the note in the Set. </param>
        /// <returns> A Set object with the values from the TextBox parameters. Returns null
        /// if a "weight" and a "repetitions" value could not be parsed. </returns>
        private Set TryGetSet(TextBox tbxWeight, TextBox tbxReps, TextBox tbxNote)
        {
            // Try parse the value in the Weight TextBox
            double weight = this.TextBoxContainsDouble(tbxWeight);
            if (weight == -1)
            {
                tbxWeight.BorderThickness = new Thickness(2);
                return null;
            }

            // Try parse the value in the Repetitions textBox
            int reps = this.TextBoxContainsInt(tbxReps);
            if (reps == -1)
            {
                tbxReps.BorderThickness = new Thickness(2);
                return null;
            }

            // If the value supplied for the note is null replace it with the empty string
            if (string.IsNullOrWhiteSpace(tbxNote.Text))
            {
                tbxNote.Text = string.Empty;
            }

            return new Set(weight, reps, tbxNote.Text);
        }

        /// <summary>
        /// Tries to parse the Text value of a TextBox to a Double.
        /// </summary>
        /// <param name="tbx"> The TextBox that contains the data to try parse. </param>
        /// <returns> Returns the Double value of the data in the TextBox. If the value can't
        /// be parsed thent his method returns -1. </returns>
        private double TextBoxContainsDouble(TextBox tbx)
        {
            // Check if the TextBox contains 0
            if (tbx.Text.Equals("0") || tbx.Text.Equals("0.0") || tbx.Text.Equals(".0"))
            {
                return 0;
            }

            double dValue;

            // Try parse the value in the TextBox to a Double
            if (!Double.TryParse(tbx.Text, out dValue))
            {
                // The value can't be parsed to either a Double so return -1
                return -1;
            }

            return dValue;
        }

        /// <summary>
        /// Tries to parse the Text value of a TextBox to an int.
        /// </summary>
        /// <param name="tbx"> The TextBox that contains the data to try parse. </param>
        /// <returns> Returns the int value of the data in the TextBox. If the value can't
        /// be parsed thent his method returns -1. </returns>
        private int TextBoxContainsInt(TextBox tbx)
        {
            int dInt;

            // Try parse the value in the TextBox to an int
            if (!Int32.TryParse(tbx.Text, out dInt))
            {
                // The value can't be parsed to either an int so return -1
                return -1;
            }

            return dInt;
        }

        #endregion Set Collecting Methods

        #region OnFocus Methods for RecordWorkout TextBoxes

        /// <summary>
        /// This method is invoked when the "tbxWeight3" TextBox is focused. This method will
        /// make the TextBoxes "tbxWeight4" and "tbxReps4" visible.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void tbxWeight3_GotFocus(object sender, RoutedEventArgs e)
        {
            this.tbxWeight4.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxReps4.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxNote4.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        /// <summary>
        /// This method is invoked when the "tbxWeight4" TextBox is focused. This method will
        /// make the TextBoxes "tbxWeight5" and "tbxReps5" visible.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void tbxWeight4_GotFocus(object sender, RoutedEventArgs e)
        {
            this.tbxWeight5.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxReps5.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxNote5.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        /// <summary>
        /// This method is invoked when the "tbxWeight5" TextBox is focused. This method will
        /// make the TextBoxes "tbxWeight6" and "tbxReps6" visible.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void tbxWeight5_GotFocus(object sender, RoutedEventArgs e)
        {
            this.tbxWeight6.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxReps6.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxNote6.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        /// <summary>
        /// This method is invoked when the "tbxWeight6" TextBox is focused. This method will
        /// make the TextBoxes "tbxWeight7" and "tbxReps7" visible.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void tbxWeight6_GotFocus(object sender, RoutedEventArgs e)
        {
            this.tbxWeight7.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxReps7.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxNote7.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        /// <summary>
        /// This method is invoked when the "tbxWeight7" TextBox is focused. This method will
        /// make the TextBoxes "tbxWeight8" and "tbxReps8" visible.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void tbxWeight7_GotFocus(object sender, RoutedEventArgs e)
        {
            this.tbxWeight8.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxReps8.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxNote8.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        /// <summary>
        /// This method is invoked when the "tbxWeight8" TextBox is focused. This method will
        /// make the TextBoxes "tbxWeight9" and "tbxReps9" visible.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void tbxWeight8_GotFocus(object sender, RoutedEventArgs e)
        {
            this.tbxWeight9.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxReps9.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxNote9.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        /// <summary>
        /// This method is invoked when the "tbxWeight9" TextBox is focused. This method will
        /// make the TextBoxes "tbxWeight10" and "tbxReps10" visible.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void tbxWeight9_GotFocus(object sender, RoutedEventArgs e)
        {
            this.tbxWeight10.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxReps10.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbxNote10.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        #endregion OnFocus Methods for RecordWorkout TextBoxes
    }
}
