﻿//-----------------------------------------------------------------------
// <copyright file="GraphPage.xaml.cs" company="EvanDolan">
//     Copyright (c) Evan Dolan. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace KeepFit
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Windows.Foundation;
    using Windows.Foundation.Collections;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;
    using Windows.UI.Xaml.Controls.Primitives;
    using Windows.UI.Xaml.Data;
    using Windows.UI.Xaml.Input;
    using Windows.UI.Xaml.Media;
    using Windows.UI.Xaml.Navigation;

    using JsonClasses;
    using KeeptFitAPI;
    using WinRTXamlToolkit.Controls.DataVisualization.Charting;

    /// <summary>
    /// A class containing a String and Double. This class is used to display data on a chart.
    /// </summary>
    public class ChartElement
    {
        public string Name { get; set; }
        public double Amount { get; set; }
    }

    /// <summary>
    /// A page that displays details for a single item within a group while allowing gestures to
    /// flip through other items belonging to the same group.
    /// </summary>
    public sealed partial class GraphPage : KeepFit.Common.LayoutAwarePage
    {
        /// <summary>
        /// An APICaller object is supplied when the App navigates to this page.
        /// </summary>
        APICaller ac;

        /// <summary>
        /// This List of Sets holds the Sets of the currently selected Workout
        /// </summary>
        List<Set> selectWorkoutSets;

        /// <summary>
        /// Initializes a new instance of the <see cref="GraphPage"/> class.
        /// </summary>
        public GraphPage()
        {
            this.InitializeComponent();
            this.selectWorkoutSets = new List<Set>();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // An instance of an APICaller object should be passed when the App
            // navigates to this page.
            this.ac = e.Parameter as APICaller;

            // Display users fullname on top right of screen
            this.tbUsersFullname.Text = this.ac.Fullname;

            // Update the ListView of Workouts.
            this.UpdateWorkoutListView();
        }

        /// <summary>
        /// Invoked when an item within the "lvPrograms" ListView is selected.
        /// </summary>
        /// <param name="sender">The GridView (or ListView when the application is Snapped)
        /// displaying the selected item.</param>
        /// <param name="e">Event data that describes how the selection was changed.</param>
        void lvWorkouts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Hide any Chart that is visible
            this.HideAllCharts();

            // Empty the itemssoucre of each chart
            this.CleanAllCharts();

            // The ListView's default selection item is null. If the selected item is null
            // then return and don't update the "gvSelectedWorkouts" GridView which shows
            // information on the selected Workout
            if (this.lvWorkouts.SelectedItem == null)
            {
                this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;
                this.tbCentreScreen.Text = "No Exercise selected.";
                return;
            }

            // Change the header to the name of the selected Program
            this.tbSelectedWorkoutHeader.Text = this.lvWorkouts.SelectedItem.ToString();

            // Try get the List of Sets for this Workout
            try
            {
                // Get a list of the workouts (and the data contained in them) in the database that have the
                // same name as the workout selected in the ListView and set it to the ItemSource of the GridView.
                List<JsonWorkout.WorkoutDetails> workoutDetails = this.ac.GetWorkouts(this.lvWorkouts.SelectedItem.ToString());
                
                // Populate the List of Sets with the values from the users Workouts
                // Iterate through each Workout and the Workouts Sets
                foreach (JsonWorkout.WorkoutDetails w in workoutDetails)
                {
                    foreach (JsonSet.SetDetails s in w.sets)
                    {
                        Set st = new Set(s.weight, s.repetitions, s.timestamp_string);
                        this.selectWorkoutSets.Add(st);
                    }
                }

                // Hide the error TextBlock
                this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

                // Set the "tbSelectedWorkoutHeader" TextBlock to be the name of the selected Program
                this.tbSelectedWorkoutHeader.Text = string.Empty + this.lvWorkouts.SelectedItem.ToString();

                // Check if the List returned is empty (user has not recorded any of these Programs)
                if (workoutDetails.Count == 0)
                {
                    // Show the error in the info TextBlock
                    this.tbCentreScreen.Text = "You have not recorded anything for this Exercise";
                    this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;
                }
                else
                {
                    // Instruct the user to select a Chart type
                    this.tbCentreScreen.Text = "Now select a chart type from below the list of Exercises";
                    this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;
                }

            }
            catch (Exception ex)
            {
                // This is called to get read of the "Variable unused" warning
                ex.ToString();
                // If the list of workouts is null or empty, the user has not made any entries for this workout
                // An ArgumentOutOfRangeExc eption, ArgumentNullException, or ArgumentException can be thrown here
                // Inform the user that there is no data about the workouts for the selected workout
                this.tbSelectedWorkoutHeader.Text = string.Empty;

                // Inform the user of the error
                this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;
                this.tbCentreScreen.Text = "You have not recorded anything for this Program";
                this.tbInfo.Text = ex.ToString();
            }
        }

        /// <summary>
        /// This method will set the ItemsSource property of each of the charts to null.
        /// </summary>
        private void CleanAllCharts()
        {
            (this.chartColumn.Series[0] as ColumnSeries).ItemsSource = null;
            (this.chartPie.Series[0] as PieSeries).ItemsSource = null; ;

            (this.chartLine.Series[0] as LineSeries).ItemsSource = null;
            (this.chartBar.Series[0] as BarSeries).ItemsSource = null;
            (this.chartBubble.Series[0] as BubbleSeries).ItemsSource = null;
            this.selectWorkoutSets = new List<Set>();
        }

        /// <summary>
        /// This method will attempt to update the "lvWorkouts" ListView with a List of the users
        /// Workouts. This method makes a call to the API to GET a list of distinct Workout names.
        /// </summary>
        /// <returns> True if the ListView was successfully updated. Otherwise, false. </returns>
        private bool UpdateWorkoutListView()
        {
            // Get the list of workouts for this user
            List<string> workoutNames = this.ac.GetDistinctWorkoutNames();

            try
            {
                // Set the List of workouts as the source for the ListView of Workouts
                this.lvWorkouts.ItemsSource = workoutNames;
                // Set the default select item to null
                this.lvWorkouts.SelectedItem = null;

                // Check if there List fo workout names is empty. If it is, inform the user
                if (workoutNames.Count == 0)
                {
                    this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    this.tbCentreScreen.Text = "You haven't created any Exercise yet.";
                }
                return true;
            }
            catch (NullReferenceException ex)
            {
                // Call ToString() to avoid 'variable not used' warning
                ex.ToString();
                // Failed to retrieve the List of workout names
                this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;
                this.tbCentreScreen.Text = "The List of Exercise names could not be retrieved. The server may be offline.";
                return false;
            }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
            // Allow saved page state to override the initial item to display
            if (pageState != null && pageState.ContainsKey("SelectedItem"))
            {
                navigationParameter = pageState["SelectedItem"];
            }

            // TODO: Assign a bindable group to this.DefaultViewModel["Group"]
            // TODO: Assign a collection of bindable items to this.DefaultViewModel["Items"]
            // TODO: Assign the selected item to this.flipView.SelectedItem
        }

        /// <summary>
        /// This method is invoked when the App tries to navigate to the prvious page. This method will
        /// check the "previous_page" variable, which is locally stored in memory, to find which page
        /// to navigate to.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        protected override void GoBack(object sender, RoutedEventArgs e)
        {
            // Store previous page string in a temp variable
            string previousPage = Windows.Storage.ApplicationData.Current.LocalSettings.Values["previous_page"] as string;
            // Set the previous page local variable
            Windows.Storage.ApplicationData.Current.LocalSettings.Values["previous_page"] = "graph";

            // Can get to the Graph page from the Workout, WorkoutProgram, or Home pages.

            if (previousPage.Equals("workout"))
            {
                this.Frame.Navigate(typeof(WorkoutPage), ac);
            }
            else if (previousPage.Equals("workoutprogram"))
            {
                this.Frame.Navigate(typeof(WorkoutProgramPage), ac);
            }

            // Default behaviour
            this.Frame.Navigate(typeof(HomePage), ac);
        }

        /// <summary>
        /// This method will hide all of the Charts.
        /// </summary>
        private void HideAllCharts()
        {
            this.chartBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.chartBubble.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.chartColumn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.chartLine.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.chartPie.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        #region Show Chart Methods

        /// <summary>
        /// This method is invoked when the "btnShowColumnChart" Button is clicked. This method will
        /// show the "chartColumn" Chart and load its contents.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void btnShowColumn_Click(object sender, RoutedEventArgs e)
        {
            // If the Column Chart is currently being dispayed then no need to do anything
            if (this.chartColumn.Visibility == Windows.UI.Xaml.Visibility.Visible)
            {
                return;
            }

            // Check if a Workout is not selected or the List of Sets is null
            if (this.lvWorkouts.SelectedItem == null || this.selectWorkoutSets == null)
            {
                this.tbCentreScreen.Text = "Please select an Exercise";
                this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;
                return;
            }

            // Check if the exercise list is empty
            if (this.selectWorkoutSets.Count == 0)
            {
                // Show the error in the info TextBlock
                this.tbCentreScreen.Text = "You have not recorded anything for this Exercise";
                this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;
                return;
            }

            this.LoadColumnChart();

            // Show the Column Chart
            this.chartColumn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            
            // Hide the other charts and the info TextBlock
            this.chartBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.chartBubble.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.chartLine.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.chartPie.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        /// <summary>
        /// This method is invoked when the "btnShowPieChart" Button is clicked. This method will
        /// show the "chartPie" Chart and load its contents.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void btnShowPieChart_Click(object sender, RoutedEventArgs e)
        {
            // If the Pie Chart is currently being dispayed then no need to do anything
            if(this.chartPie.Visibility == Windows.UI.Xaml.Visibility.Visible)
            {
                return;
            }

            // Check if a Workout is not selected or the List of Sets is null
            if (this.lvWorkouts.SelectedItem == null || this.selectWorkoutSets == null)
            {
                this.tbCentreScreen.Text = "Please select an Exercise";
                this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;
                return;
            }

            // Check if the exercise list is empty
            if (this.selectWorkoutSets.Count == 0)
            {
                // Show the error in the info TextBlock
                this.tbCentreScreen.Text = "You have not recorded anything for this Exercise";
                this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;
                return;
            }

            this.LoadPieChart();

            // Show the Pie Chart
            this.chartPie.Visibility = Windows.UI.Xaml.Visibility.Visible;

            // Hide the other charts and the info TextBlock
            this.chartBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.chartBubble.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.chartColumn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.chartLine.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private void btnShowLineChart_Click(object sender, RoutedEventArgs e)
        {

        }

        #endregion Show Chart Methods

        #region Load Chart Methods

        /// <summary>
        /// This method will set the ItemsSource of the Column Chart to be the Sets for the selected Workout.
        /// </summary>
        private void LoadColumnChart()
        {
            // Iterate through the list of selected exercises and set up the data to display
            for (int i = 0; i < this.selectWorkoutSets.Count; i++)
            {
                this.selectWorkoutSets[i].Repetitions = i + 1;
            }

            this.tbInfo.Visibility = Windows.UI.Xaml.Visibility.Visible;

            List<ChartElement> chartElements = new List<ChartElement>();
            foreach (Set s in this.selectWorkoutSets)
            {
                chartElements.Add(new ChartElement() { Name = s.GetDate(), Amount = s.Weight });
                this.tbInfo.Text += s.Timestamp + "\n";
            }

            (this.chartColumn.Series[0] as ColumnSeries).ItemsSource = this.selectWorkoutSets;
        }

        /// <summary>
        /// This method will set the ItemsSource of the Pie Chart to be the Sets for the selected Workout.
        /// </summary>
        private void LoadPieChart()
        {
            // Check if the exercise list is empty
            if (this.selectWorkoutSets == null || this.selectWorkoutSets.Count == 0)
            {
                // Show the error in the info TextBlock
                this.tbCentreScreen.Text = "You have not recorded anything for this Exercise";
                this.tbCentreScreen.Visibility = Windows.UI.Xaml.Visibility.Visible;
                return;
            }

            try
            {
                // Load the sets in to three ranges for the Pie Chart: Low, Medium, and High
                // Iterate through the Sets and get the minimum and maximum value of the Weights
                double min = this.selectWorkoutSets[0].Weight;
                double max = min;

                foreach (Set s in this.selectWorkoutSets)
                {
                    // Check for new min value
                    if (s.Weight < min)
                    {
                        min = s.Weight;
                    }
                    else if (s.Weight > max)
                    {
                        max = s.Weight;
                    }

                }

                // Split the values in to three ranges (LOW, MEDIUM, HIGH)
                // Calculate the low and medium upper limits on the values
                double lowUpperLimit = min + (max - min) * 0.33;
                double medUpperLimit = min + (max - min) * 0.66;

                int low = 0, med = 0, high = 0;

                // Iterate through the Sets and calculate how many values fall into which range
                foreach (Set s in this.selectWorkoutSets)
                {
                    if (s.Weight <= lowUpperLimit)
                    {
                        low++;
                    }
                    else if (s.Weight <= medUpperLimit)
                    {
                        med++;
                    }
                    else
                    {
                        high++;
                    }
                }

                // Create a ChartElement List to represent the values and their ranges
                List<ChartElement> chartElementSets = new List<ChartElement>();
                chartElementSets.Add(new ChartElement() { Name = "LOW", Amount = low });
                chartElementSets.Add(new ChartElement() { Name = "MEDIUM", Amount = med });
                chartElementSets.Add(new ChartElement() { Name = "HIGH", Amount = high });

                // Set the Pie Chart's ItemsSource to the List of values and ranges
                (this.chartPie.Series[0] as PieSeries).ItemsSource = chartElementSets;
            }
            catch (Exception e)
            {
                this.tbInfo.Visibility = Windows.UI.Xaml.Visibility.Visible;
                this.tbInfo.Text = "No data on the Exercise could be found.\n" + e.ToString();
            }
        }

        #endregion Load Chart Methods
    }
}
