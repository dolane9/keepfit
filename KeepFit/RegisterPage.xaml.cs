﻿//-----------------------------------------------------------------------
// <copyright file="RegisterPage.xaml.cs" company="EvanDolan">
//     Copyright (c) Evan Dolan. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace KeepFit
{
    using JsonClasses;
    using KeeptFitAPI;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Windows.Foundation;
    using Windows.Foundation.Collections;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;
    using Windows.UI.Xaml.Controls.Primitives;
    using Windows.UI.Xaml.Data;
    using Windows.UI.Xaml.Input;
    using Windows.UI.Xaml.Media;
    using Windows.UI.Xaml.Navigation;

    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class RegisterPage : KeepFit.Common.LayoutAwarePage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RegisterPage"/> class.
        /// </summary>
        public RegisterPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // A username may have been passed as a parameter. If the value passed is
            // not null, convert it to a string and put it in the username TextBox
            string username = e.Parameter as string;
            if (!username.Equals(null))
            {
                this.tbxUsername.Text = username;
            }
        }

        /// <summary>
        /// This method is called then the "btnGoToRegister" button is clicked. This method will
        /// attempt to register a new user in the system with the details supplied by the user in
        /// the TextBoxes on this page.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void btnTryRegister_Click(object sender, RoutedEventArgs e)
        {
            // Create JsonReply for the hold the reply from the server, if any
            JsonReply jr = new JsonReply();

            if (this.AreTextBoxesValid())
            {
                // Get the users login credentials, create an APICaller object, and create a JsonReply
                byte[] auth = System.Text.Encoding.UTF8.GetBytes(this.tbxUsername.Text + ":" + this.pbPassword.Password);
                APICaller ac = new APICaller(System.Convert.ToBase64String(auth), this.tbxUsername.Text);

                // Make call to server and try log in
                jr = ac.RegisterUser(this.tbxEmail.Text, this.tbxFirstname.Text, this.tbxLastname.Text);

                if (jr.errorCode == APIErrorCodes.Success)
                {
                    // Set the previous page local variable
                    Windows.Storage.ApplicationData.Current.LocalSettings.Values["previous_page"] = "register";

                    // Successful registration, navigate to next page
                    this.Frame.Navigate(typeof(HomePage), ac);
                }
                else
                {
                    // Register has failed, inform user and stay on this page
                    this.tbFailedRegister.Text = "The registration has failed. The following error was received:\n";
                    this.tbFailedRegister.Text += jr.status;
                }
            }
            else
            {
                this.tbFailedRegister.Text = "Oh no! One of the values entered above is invalid.";
                return;
            }

            // Check if the registration failed because the username already exists. If so, clear the TextBox
            // and increase the border thickness for the username TextBox
            if (jr.errorCode.Equals(APIErrorCodes.UsernameAlreadyExists))
            {
                this.tbxUsername.Text = string.Empty;
                this.tbxUsername.BorderThickness = new Thickness(2);
            }

            // Check if the registration failed because the email already exists. If so, clear the TextBox
            // increase the border thickness for the email TextBox
            if (jr.errorCode.Equals(APIErrorCodes.EmailAlreadyExists))
            {
                this.tbxEmail.Text = string.Empty;
                this.tbxEmail.BorderThickness = new Thickness(2);
            }
        }

        /// <summary>
        /// This method is called when the user clicks on the the "btnBackButton" button. This method
        /// will navigate the App to the MainPage and send the string "register" an type object. This
        /// object is sent so that when the MainPage receives it, it will know which page the App
        /// was last on before it navigated to the MainPage.
        /// </summary>
        /// <param name="sender"> This parameter is never used. </param>
        /// <param name="e"> This parameter is never used. </param>
        private void btnBackButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage));
        }

        /// <summary>
        /// This method validates the values entered in each TextBox and PasswordBox on the Register page.
        /// If any value is null, empty, or just white space the method will return false.
        /// This method will also increase the border thickness of any TextBox that contains an invalid value
        /// </summary>
        /// <returns> True if each value in ever TextBox and PasswordBox is non-empty, non-null and not
        /// just whitespace. Otherwise, false. </returns>
        private bool AreTextBoxesNonEmpty()
        {
            // This bool is set to false if any of the TextBoxes are not valid
            bool valid = true;

            // Check the firstname supplied
            if (string.IsNullOrWhiteSpace(this.tbxFirstname.Text))
            {
                this.tbxFirstname.BorderThickness = new Thickness(2);
                valid = false;
            }

            // Check the lastname supplied
            if (string.IsNullOrWhiteSpace(this.tbxLastname.Text))
            {
                this.tbxLastname.BorderThickness = new Thickness(2);
                valid = false;
            }

            // Check the email suppled
            if (string.IsNullOrWhiteSpace(this.tbxEmail.Text))
            {
                this.tbxEmail.BorderThickness = new Thickness(2);
                valid = false;
            }

            // Check the username supplied
            if (string.IsNullOrWhiteSpace(this.tbxUsername.Text))
            {
                this.tbxUsername.BorderThickness = new Thickness(2);
                valid = false;
            }

            // Check the password supplied
            if (string.IsNullOrWhiteSpace(this.pbPassword.Password))
            {
                this.pbPassword.BorderThickness = new Thickness(2);
                valid = false;
            }
            
            return valid;
        }

        /// <summary>
        /// This method validates the values entered in each TextBox and PasswordBox on the Register page.
        /// If any value is empty or invalid this method will return false.
        /// This method will also increase the border thickness of any TextBox that contains an invalid value
        /// </summary>
        /// <returns> True if each value in ever TextBox and PasswordBox is non-empty, non-null, not
        /// just whitespace, and valid. Otherwise, false. </returns>
        private bool AreTextBoxesValid()
        {
            // First set the border thickness of each TextBox back to zero.
            this.SetTextBoxBorderthicknessToDefault();

            // Check each TextBox for a valid value and then for an empty value
            if (!this.IsValidName(this.tbxFirstname.Text))
            {
                this.tbxFirstname.BorderThickness = new Thickness(2);
                return false;
            }

            if (!this.IsValidName(this.tbxLastname.Text))
            {
                this.tbxLastname.BorderThickness = new Thickness(2);
                return false;
            }

            if (!this.IsValidEmail(this.tbxEmail.Text))
            {
                this.tbxEmail.BorderThickness = new Thickness(2);
                return false;
            }

            if (!this.IsValidUsername(this.tbxUsername.Text))
            {
                this.tbxUsername.BorderThickness = new Thickness(2);
                return false;
            }

            return this.AreTextBoxesNonEmpty();
        }

        /// <summary>
        /// Resets the border thickness of each TextBox on the Register page.
        /// </summary>
        private void SetTextBoxBorderthicknessToDefault()
        {
            this.tbxFirstname.BorderThickness = new Thickness(0);
            this.tbxLastname.BorderThickness = new Thickness(0);
            this.tbxEmail.BorderThickness = new Thickness(0);
            this.tbxUsername.BorderThickness = new Thickness(0);
            this.pbPassword.BorderThickness = new Thickness(0);
        }

        /// <summary>
        /// This method checks if the string supplied represents a valid email.
        /// </summary>
        /// <param name="email"> A String representing an email. </param>
        /// <returns> True is the string represents a valid email. Otherwise false. </returns>
        private bool IsValidEmail(string email)
        {
            Regex rx = new Regex("^[A-Za-z]+[A-Za-z0-9_-]*@[A-Za-z]+[A-Za-z0-9_-]*.[A-Za-z]+[A-Za-z0-9]*$");
            return rx.IsMatch(email);
        }

        /// <summary>
        /// This method checks if the string supplied represents a valid username.
        /// </summary>
        /// <param name="username"> A String representing a username. </param>
        /// <returns> True is the string represents a valid username. Otherwise false. </returns>
        private bool IsValidUsername(string username)
        {
            // The username must begin with a letter and can contain any combination of letters, numbers, and the "_" and "-" symbols
            Regex rx = new Regex("^[A-Za-z]+[A-Za-z0-9_-]*$");
            return rx.IsMatch(username);
        }

        /// <summary>
        /// This method checks if the string supplied represents a valid first or last name.
        /// </summary>
        /// <param name="name"> A String representing a name. </param>
        /// <returns> True is the string represents a valid name. Otherwise false. </returns>
        private bool IsValidName(string name)
        {
            // The name can contain any upper or lower case letters and have a minimum length of 1
            Regex rx = new Regex("^[A-Za-z]+$");
            return rx.IsMatch(name);
        }
    }
}
