KeepFit

Keep Fit is an Application (App) for Tablet and Computer devices running the operating system Windows 8.1. The aim of the App is to promote a healthy lifestyle to the user. The App helps the user record their progress during exercising. A user can view their progress for each exercise and have their records shown on a graph for comparison. This allows a user to easily see if they are meeting targets and how well they are performing overall. The layout of the App was created using XAML.

The App can also allow a user to record what they eat and keep a record of their diet. The App provides statistics on exercising performance and food eaten, such as calorie intake. Each user creates their own account. User information is stored on a MongoDB database on an external server. Node.js is used for communication between the App and the database. 

Another feature of the App is the bar-code scanning function. A user can scan a bar-code on a food product and the App will attempt to identify the item and provide some nutritional values about the product. A user can use this feature to keep track of what they have eaten. By scanning a product, they an specify how much of the food item they have eaten. This information is recorded on the 
database.

Project Area: Software Development
Project Technology: C#, Node.js, REST, NoSQL, XAML
Project Platform: Windows

How do I get set up?

Download this repository to a Windows 8.1 machine.
Open up in VisualStudio (VS 2012 Ultimate is used by the owner).
Press F5 and run on Local Machine.
Application will be installed after this.

Could also create an App Package from Visual Studio. Project -> Store -> Create Package... Select no when asked to upload to Windows Store and then click next for everything else. This will create some files along with a shell files. Right click and run the file and the application will be installed on the local machine.

Coding Guidelines

StyleCop is used in this project.

Who do I talk to?

Repo owner
https://bitbucket.org/dolane9/